function getCart() {
  $.ajax({
    url: '/cart/show',
    type: 'GET',
    success: function(res) {
      if(!res) alert('Ошибка!');
      showCart(res);
    },
    error: function() {
      alert('Ошибка');
    }
  });
}

function clearCart() {
  $.ajax({
    url: '/cart/clear',
    type: 'GET',
    success: function(res) {
      if(!res) alert('Ошибка!');
      showCart(res);
      $('#displayCount').html($('#cartcount').text());
    },
    error: function() {
      alert('Ошибка');
    }
  });
}

function showCart(cart) {
  $('#cart .modal-body').html(cart);
  if (cart.indexOf("Корзина пуста") >= 0) {
      $('#issue-order-button').hide();
      $('#clear-cart-button').hide();
  } else {
      $('#issue-order-button').show();
      $('#clear-cart-button').show();
  }
  $('#cart').modal();
}

$('#cart .modal-body').on('click', '.del-item', function() {
  var id = $(this).data('id');
  $.ajax({
    url: '/cart/del-item',
    data: {id: id},
    type: 'GET',
    success: function(res) {
      if(!res) alert('Ошибка');
      showCart(res);
      $('#displayCount').html(Number($('#cartcount').text().replace(/\D+/g,"")));
    },
    error: function() {
      alert('Ошибка');
    }
  });
});

function addCart(id) {
    var count = $('#count-modal'+id).val();
    $.ajax({
      url: '/cart/add',
      data: {id: id, count: count},
      type: 'GET',
      success: function(res) {
        if (!isNaN(parseFloat(res)) && isFinite(res)) {
            showNotice(res);
        } else {
            alert(res);
            return false;
        }
      },
      error: function() {
        alert('Ошибка');
      }
    });
}

$('.restourants-page-search_button').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var count = $('#count'+id).val();
    $.ajax({
      url: '/cart/add',
      data: {id: id, count: count},
      type: 'GET',
      success: function(res) {
          if (!isNaN(parseFloat(res)) && isFinite(res)) {
              showNotice(res);
          } else {
              alert(res);
              return false;
          }
      },
      error: function() {
        alert('Ошибка');
      }
    });
});

function showPromoForm() {
    jQuery('#promo-code').show();
    jQuery('#promo-btn').text('Отправить');
    var promoval = jQuery('#promo-code').val();
    if (promoval != "") {
        $.ajax({
          url: '/cart/promo?code=' + encodeURIComponent(promoval),
          type: 'GET',
          success: function(res) {
            showCart(res);
          },
          error: function(res) {
              alert('Ошибка');
          }
        });
    }
}

function sendPromo() {
    jQuery('#promo-code').show();
    jQuery('#promo-btn').text('Отправить');
    var promoval = jQuery('#promo-code').val();
    if (promoval != "") {
        $.ajax({
          url: '/cart/promo?code=' + encodeURIComponent(promoval) + '&form=2',
          type: 'GET',
        });
    }
}

function showNotice(e) {
    $('#notice').modal();
    text = $('#displayCount').text(e);
    setTimeout(function() { $('#notice').modal('hide') }, 1000);
}

function checkout() {
    var minprice = $('#cart-minprice').html();
    var sum = $('#cart-sum').html();
    if (parseInt(minprice) > parseInt(sum)) {
        alert('Минимальная сумма заказа не набрана');
    } else {
        window.location.replace("/cart/view");
    }
}
