/*
 * Script opens header on mobile displays
 */
jQuery('#hamburger').click(function() {
  if(jQuery('#header').hasClass('header_open')) {
    jQuery('#header.header_open').removeClass('header_open');
    jQuery('#hamburger').removeClass('header_hamburger__animation');
  } else {
    jQuery('#header').addClass('header_open');
    jQuery('#hamburger').addClass('header_hamburger__animation');
  }
});

/*
 * Script opens list of cities on first char
 */
(function($) {
  $(function() {
    $('.choose-city_list-alphabet').on('click', '.choose-city_char:not(.active)', function() {
      $(this)
        .addClass('active').siblings().removeClass('active')
        .closest('.modal_choose-city').find('.choose-city-tab_list').removeClass('active').eq($(this).index()).addClass('active');
    });
  });
})(jQuery);

/*
 * Script calculates count of food
 */
// jQuery(document).ready(function () {
//    jQuery(document).click(function() {
//      var displayCount = jQuery('#cartcount').html();
//      if (displayCount === undefined) {
//        jQuery('#displayCount').html('0');
//    }
//    });
//  });

function getCityName(e) {
    $.ajax({
      url: '/city?name=' + e,
      type: 'GET',
    });
}

/*
 * Script opens modal window of cities
 */

if ($('#header_city').length != 0) {
  document.getElementById('header_city').onclick = function() {
    if (document.getElementById('modal_choose-city').classList.contains('choose-city_open')) {
      document.getElementById('modal_choose-city').classList.remove('choose-city_open');
    } else {
        if (document.getElementById('dropdown-city') !== "" && document.getElementById('dropdown-city') !== undefined && document.getElementById('dropdown-city') !== null) {
            document.getElementById('dropdown-city').remove(document.getElementById('dropdown-city'));
        }
        document.getElementById('modal_choose-city').classList.add('choose-city_open');
    }
  };
}

if ($('#change_city').length != 0) {
  document.getElementById('change_city').onclick = function() {
    if (document.getElementById('modal_choose-city').classList.contains('choose-city_open')) {
      document.getElementById('modal_choose-city').classList.remove('choose-city_open');
    } else {
    if (document.getElementById('dropdown-city') !== "" && document.getElementById('dropdown-city') !== undefined &&                document.getElementById('dropdown-city') !== null) {
        document.getElementById('dropdown-city').remove(document.getElementById('dropdown-city'));
    }
      document.getElementById('modal_choose-city').classList.add('choose-city_open');
    }
  };
}


/*
 * Script adds incrementation and dicrementation for input[number]
 */
 $(document).ready(function() {
   $('.decr').click(function () {
       var $input = $(this).parent().find('input');
       var count = parseInt($input.val()) - 1;
       count = count < 1 ? 1 : count;
       $input.val(count);
       $input.change();
       return false;
   });
   $('.incr').click(function () {
       var $input = $(this).parent().find('input');
       $input.val(parseInt($input.val()) + 1);
       $input.change();
       return false;
   });
});

/*
 *
 */
// if (jQuery(window).width() > '767') {
//   jQuery('#modal_choose-city').css('width', jQuery('.choose-city_list-alphabet:first-child').width() + 60);
// }
// jQuery('.choose-city-tab_list').css('width', jQuery('.choose-city_list-alphabet:first-child').width() - 25);

/*
 *
 */
jQuery('.go_to').click( function(){
	  console.log('a?');
  var scroll_el = $(this).attr('href');
  if ($(scroll_el).length != 0) {
    jQuery('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);
  } else {
	  console.log('a?');
    window.location.href = "../";
  }
  return false;
});

$('.keyvisual-carousel').owlCarousel({
  loop:true,
  margin:0,
  nav:true,
  dots:true,
  autoplay: true,
  autoplayTimeout: 6000,
  smartSpeed: 1500,
  responsive:{
      0:{
          items:1
      }
  }
});

// $(function() {
//     $(window).resize(function () {
//       $('.owl-item').css('width', $( window ).width());
//       console.log('a');
//     });
//
// });

// document.body.onresize = function() {
//   var screenWidth = document.body.clientWidth;
//   var owlItemCount = document.getElementsByClassName('owl-item');
//
//   for (var i = 0; i < owlItemCount.length; i++) {
//     setTimeout (
//       function(obj, width) {
//         obj.style.width = width;
//       },
//       5000,
//       owlItemCount[i],
//       document.body.clientWidth
//     );
//   }
//   console.log(owlItemCount[0].style.width + ' ' + document.body.clientWidth + ' ' + screenWidth);
// };

var ratioDataIdToText = {
    'list_of_stations' : function () {
        return $('.order-train-way_title__changed-text').text('Выберите станцию на которой хотите осуществить заказ');
    },

    'list_of_trains' : function () {
        return $('.order-train-way_title__changed-text').text('Выберите Ваш поезд');
    },

    'undefined' : function () {
      return false;
    }
};

$(document).ready(function () {
  ratioDataIdToText[$('.stations-train_table').attr('data-id')]();
});

function externalLinks() {
    links = document.getElementsByTagName("a");
    for (i=0; i<links.length; i++) {
        link = links[i];
        if (link.getAttribute("href") && link.getAttribute("rel") == "external")
        link.target = "_blank";
    }
 }
 window.onload = externalLinks;
