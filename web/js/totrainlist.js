function getTrainList() {
    var wherefrom = $('#selecttt option:selected').val();
    var wherein = $('#stationform-wherein option:selected').val();
    var date = $('#stationform-date').val();

    if (wherefrom != '' && wherein != '' && date != '') {
        $.ajax({
            url: '/train/list?wherefrom=' + wherefrom + '&wherein=' + wherein + '&date=' + date,
            type: 'GET',
        });
    return false;
    }
}
