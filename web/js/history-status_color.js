/*
 * Changes color of status
 */
jQuery(document).ready(function () {
  switch (jQuery('.history-block_status').html()) {
    case 'В ожидании ответа':
      jQuery('.history-block_status').addClass('wait');
      break;

    case 'Поступил в обработку':
      jQuery('.history-block_status').addClass('processed');
      break;

    case 'Выполнен':
      jQuery('.history-block_status').addClass('done');
      break;

    case 'Отменен':
      jQuery('.history-block_status').addClass('canceled');
      break;
  }
});
