/*
 * Telephone mask
 */
jQuery(function(){
  jQuery("#phone").mask("+7 (999) 999-9999");
});

/*
 * Form check
 */
jQuery(document).ready(function() {
  jQuery('#email').blur(function() {
    if(jQuery(this).val() != '') {
      var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
      if(pattern.test($(this).val())){
        jQuery(this).css({'border' : '1px solid #569b44'});
        jQuery('#valid').text('Верно');
      } else {
        jQuery(this).css({'border' : '1px solid #ff0000'});
        jQuery('#valid').text('Не верно');
      }
    }
    // else {
    //   jQuery(this).css({'border' : '1px solid #ff0000'});
    //   jQuery('#valid').text('Поле email не должно быть пустым');
    // }
  });
});
