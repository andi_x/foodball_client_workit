$('#order-payment_type').click(function() {
    var inp = $(this).children('option');
    $(inp).each(function() {
            if ($(this).prop('selected')) {
                if ($(this).val() == 3) {
                    $('#pay-button').show();
                    $('#submit-button').hide();
                } else {
                    $('#pay-button').hide();
                    $('#submit-button').show();
                }
            }
    });
});

$(document).ready(function() {
    var pay = $('#order-payment_type').children('option');
    var transaction = $('#order-transaction_id').val();
    if (transaction !== null && transaction !== undefined && transaction.length !== 0) {
        $('#pay-button').hide();
        $('#submit-button').show();
    } else {
        $(pay).each(function() {
                if ($(this).prop('selected')) {
                    if ($(this).val() == 3) {
                        $('#pay-button').show();
                        $('#submit-button').hide();
                    } else {
                        $('#pay-button').hide();
                        $('#submit-button').show();
                    }
                }
        });
    }
});
