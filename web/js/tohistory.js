
function getHistory() {
  $.ajax({
    url: '/history/show',
    type: 'GET',
    success: function(res) {
      if(!res) alert('Ошибка!');
      showHistory(res);
    },
    error: function() {
      alert('Error');
    }
  });
}

function showHistory(history) {
  $('#history_account').html(history);
}
