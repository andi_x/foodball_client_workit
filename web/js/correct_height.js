window.onload = function() {
  if ($(window).width() > '767'){
    var mainContentHeight = document.getElementById('restourants_page_contain_restaurants').offsetHeight;
    var mainAsideHeight = document.getElementById('asideMenu').offsetHeight;
    if (mainContentHeight > mainAsideHeight) {
      document.getElementById('asideMenu').style.height = mainContentHeight + 'px';
    } else {
      document.getElementById('restourants_page_contain_restaurants').style.height = mainAsideHeight + 'px';
    }
  }
};
