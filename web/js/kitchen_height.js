jQuery('.restourants-page-contain_foods-more').click(function() {
  if (jQuery('.restourants-page-contain-foods-checks_container').hasClass('open')) {
    jQuery('.restourants-page-contain-foods-checks_container.open').removeClass('open');
  } else {
    jQuery('.restourants-page-contain-foods-checks_container').addClass('open');
  }
});

var foodsChecks = jQuery('.restourants-page-contain_foods-checks .restourants-page-contain_checkbox-emulator');
if (foodsChecks.length < 8) {
  jQuery('.restourants-page-contain_foods-more').css('display', 'none');
}
