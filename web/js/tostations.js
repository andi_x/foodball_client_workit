function showForm(name, city, date, waiting_time) {
  $('#train .modal-header').html('<h2>' + 'Город: ' + city  + '</h2>' + '<h2>' + 'Cтанция: ' + name + '</h2>');
  $('#train .modal-body .station').val(name);
  $('#train .modal-body .waiting_time').val(waiting_time);
  $('#train .modal-body .date').val(date);
  $('#train').modal();
}

jQuery('.stations-train-table_row').click(function () {
  if (!(jQuery(this).hasClass('inactive'))) {
  var name = $(this).data('name');
  var code = $(this).data('code');
  var date = $(this).data('date');
  var waiting_time = $(this).data('waiting');
    $.ajax({
      url: '/train/info?code=' + code,
      type: 'GET',
      success: function(res) {
        if(res != 0) {
          res = $.parseJSON(res);
          $('#train .modal-body .city_id').val(res.id);
          $('#train .modal-body .city').val(res.name);
          showForm(name, res.name, date, waiting_time);
        }
      },
      error: function() {
        alert('Станция не найдена');
      },
    });
  }
});
