/*
 * Configurations of the flip light box
 */
jQuery('body').flipLightBox({
  lightbox_flip_speed: 500,
  lightbox_border_color: '#666666'
});

var linkWidth = jQuery('.main_gallery a');
var imageLinksCount = linkWidth.length;
var imageLinksFiles = [];

var imageHeight = linkWidth[0].offsetWidth;

var linkWidth = jQuery('.main_gallery a');

/*
 * Cycle does height equal width
 */
for (var i = 0; i < linkWidth.length; i++) {
  linkWidth[i].style.height = imageHeight + 'px';
}

/*
 * Function adds images, if the gallery has less images them need
 */
// for (var i = 0; i < linkWidth.length; i++) {
//   imageLinksFiles[i] = linkWidth[i].getAttribute('href');
// }
//
// window.onload = function() {
//   while (imageLinksCount % 8 !== 0) {
//     var a = document.getElementById('main-gallery_content').innerHTML;
//     imageLinksCount++;
//
//     var randomImage = Math.round(Math.random() * (imageLinksFiles.length-1));
//     document.getElementById('main-gallery_content').innerHTML = a
//       + '<a href="' + imageLinksFiles[randomImage]
//       + '" class="flipLightBox-' + imageLinksCount
//       + '" style="background-image: url(\''
//       + imageLinksFiles[randomImage] + '\'); margin-right: 4px; height: '
//       + imageHeight + 'px;"><span style="display: none;"></span></a>';
//   }
// }
