/*
* Timer
*/
var sec = 4;
var timer = new _classTimer(sec);

// timer.onInit = function () {
//     alert('start')
// };

timer.onEnd = function () {
    send();
};

$('.user-page_link').click(function() {
  var tabID = $(this).attr('href');
  var bg = $(this).css("backgroundColor");
  var hg = $(this).data('height');

  $('.user-page_link').removeClass('open');
  $(this).addClass('open');

  $('.user-page_tab').removeClass('open');
  $(tabID).addClass('open');

  $('.user-page_content').css('border-color', bg);

  $('.user-page_content').css('min-height', hg);

  return false;
});

$('.message').click(function() {
  if ($(this).hasClass('new')) {
    $(this).removeClass('new');
    check($(this).data('id'));
  }

  $('.message-content_title').text($(this).find('.message_title').text());
  $('.message-content_text').text($(this).find('.message_text').text());

  $('.message_content').addClass('open');

  $('.message-content_close').click(function() {
  $('.message_content').removeClass('open');
  });
});

jQuery('#order_from').change(function() {
    timer.start();
});

jQuery('#order_to').change(function() {
    timer.start();
});

function send()
{
    var order_from = jQuery('#order_from').val();
    var order_to = jQuery('#order_to').val();

    $.ajax({
        url: '/account?order_from=' +  encodeURIComponent(order_from)
        + '&order_to=' + encodeURIComponent(order_to),
        type: 'GET',
    });
}

function check(id)
{
    $.ajax({
        url: 'site/check-message?id=' +  encodeURIComponent(id),
        type: 'GET',
    });
}
