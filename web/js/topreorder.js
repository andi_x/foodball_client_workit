/*
* Timer
*/
var sec = 2;
var timer = new _classTimer(sec);

// timer.onInit = function () {
//     alert('start')
// };

timer.onEnd = function () {
    send();
};

/*
 * Script clear search restiraunt name
 */
jQuery('#clear').click(function() {
    $.ajax({
      url: '/filters-clear',
      type: 'GET',
      success: function() {
          location.reload(true);
      }
    });
});

/*
 * Script range slider config
 */
 jQuery(function () {
   jQuery("#range").ionRangeSlider({
     min: 0,
     max: 9000,
    //  from: 0,
     from: jQuery('#range_value').val(),
    //  to: 9000,
     to: jQuery('#range_second_value').val(),

     type: 'double',

     hide_min_max: true,
     hide_from_to: false,
   });
 });

 /*
  * Script adds value from input-range to input-text
  */
 document.getElementById('range').onchange = function() {
  var rangeValue = jQuery('#range_value');
  var rangeArray = jQuery('.irs-from').text();
  var firstValue = '';

  for (key in rangeArray) {
    if (rangeArray[key] !== ' ') {
      firstValue = firstValue + rangeArray[key];
    }
  }

  rangeValue.val(firstValue);
  timer.start();

  var rangeSecondValue = jQuery('#range_second_value');
  var rangeSecondArray = jQuery('.irs-to').text();
  var secondValue = '';

  for (key in rangeSecondArray) {
    if (rangeSecondArray[key] !== ' ') {
      secondValue = secondValue + rangeSecondArray[key];
    }
  }

  rangeSecondValue.val(secondValue);
  timer.start();
};


    // timer.onChange = function (sec, f) {
    //     console.log(sec);
    // };


jQuery('#range_value').change(function() {
    timer.start();
});


function send(e)
{
    var categories = [];
    jQuery(".products_filter").each( function() {
        if(jQuery(this).prop('checked')) {
            categories.push(jQuery(this).val());
        }
    });
    var types = [];
    jQuery('.agent_filter').each( function() {
        if(jQuery(this).prop('checked')) {
            types.push(jQuery(this).val());
        }
    });
    var minprice = jQuery('#range_value').val();
    var maxprice = jQuery('#range_second_value').val();
    var name = jQuery('#restoraunt').val();

  $.ajax({
    url: '?types=' +  encodeURIComponent(types)
    + '&minprice=' + encodeURIComponent(minprice)
    + '&maxprice=' + encodeURIComponent(maxprice)
    + '&name=' + encodeURIComponent(name)
    + '&categories=' + encodeURIComponent(categories),
    type: 'GET',
  });
}

/*
 * Hides checkbox 'Еда за БОНУСЫ'
 */
$(document).ready(function() {
  var arrayCheckbox = jQuery('.restourants-page-contain_checkbox-emulator');

  for (var i = 0; i < arrayCheckbox.length; i++) {
        try {
            if (arrayCheckbox[i].getElementsByTagName('label')[0].innerText === " Еда за БОНУСЫ") {
              arrayCheckbox[i].style.display = 'none';
              break;
            }
        } catch (e) {

        }
  }
});


/*
* Перейти на предзаказ
 */
function preOrdHome(e) {
  $.ajax({
    url: '/preord',
    type: 'GET',
    success: function() {
        window.location=e;
    }
  });
}

/*
* Показать модальное окно предзаказа
 */
function ShowPreOrd(s) {
    $('#preOrdModal').modal();
    $('#preOrdModal .preorder').attr('onclick', "return preOrdHome(\""+s+"\");");
    return false;
}

function cancelPreord() {
    $.ajax({
      url: '/cancel-preord',
      type: 'GET',
    });
}
