/*
 * Configurations for owl carousel on index page
 */
jQuery('.owl-carousel.main-carousel').owlCarousel({
  loop:true,
  margin:0,
  nav:false,
  dots: false,
  autoplay: 6000,
  autoplayTimeout: 6000,
  responsive:{
    0:{
        items:1
    },
    600:{
        items:1
    },
    1000:{
        items:1
    }
  }
})
