/*
 * Script for active checkbox on index page
 */
jQuery('.main-subscribtion-form_input-checkbox-block').click(function() {
  if (jQuery(this).hasClass('active_checkbox')) {
    jQuery(this).removeClass('active_checkbox');
    jQuery(this).children('.restourants-page-contain_checkbox').removeAttr('checked');
  } else {
    jQuery(this).addClass('active_checkbox');
    jQuery(this).children('.restourants-page-contain_checkbox').attr('checked', 'true');
  }
});

/*
 * Script for active checkbox on preorder page
 */
jQuery('.restourants-page-contain_checkbox-emulator').click(function() {
 if (jQuery(this).hasClass('active_checkbox')) {
   jQuery(this).removeClass('active_checkbox');
   jQuery(this).children().children('input[type="checkbox"]').removeAttr('checked');
   jQuery(this).children('input[type="checkbox"]').removeAttr('checked');
 } else {
   jQuery(this).addClass('active_checkbox');
   jQuery(this).children().children('input[type="checkbox"]').attr('checked', 'true');
   jQuery(this).children('input[type="checkbox"]').attr('checked', 'true');
 }
});

jQuery('.restourants-page-contain_checkbox-emulator input[checked]').parents('.restourants-page-contain_checkbox-emulator').addClass('active_checkbox');
