changeOwlLink();

$(window).resize(function() {
    changeOwlLink();
});

function changeOwlLink () {
    if ($(window).width() < 768) {
        $('.keyvisual_link').attr('href', '#train_list');
    } else {
        $('.keyvisual_link').attr('href', '#main-order');
    }
}

/*
 * Script closes modal window for choose cities
 */
jQuery('#choose-city_close').click(function() {
  jQuery('#modal_choose-city').removeClass('choose-city_open');
});
