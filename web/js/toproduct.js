function showProduct(form) {
    $('#product-modal .modal-body').html(form);
    $('#product-modal').modal();
}

function getProduct(id) {
  $.ajax({
    url: '/product/form?id=' + id,
    type: 'GET',
    success: function(res) {
      if(!res) alert('Ошибка!');
      showProduct(res);
    },
    error: function() {
      alert('Ошибка');
    }
  });
}

function Decr() {
    var $input = $('#product-modal .decr').parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
}

function Incr() {
    var $input = $('#product-modal .incr').parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
}
