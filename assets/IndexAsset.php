<?php

namespace app\assets;

use yii\web\AssetBundle;


class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
      '/libs/carousel/js/owl.carousel.min.js',
      '/libs/gallery/fliplightbox.min.js',
      '/libs/maskedinput/jquery.maskedinput.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
