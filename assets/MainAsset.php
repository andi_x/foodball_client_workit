<?php

namespace app\assets;

use yii\web\AssetBundle;


class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      '/css/fonts.css',
      '/libs/bootstrap3/css/bootstrap.min.css',
      '/libs/font-awesome-4.7.0/css/font-awesome.min.css',
      '/css/style.css?ver=2',
      '/css/media.css?ver=2',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
