<?php
use app\components\widgets\TrainsWidget;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use kartik\select2\Select2;
use yii\web\JsExpression;

$this->title = 'Заказ готовой еды прямо к вагону поезда |FOOD BALL';
$this->registerMetaTag(['name' => 'description', 'content' => 'С помощью сервиса FOOD BALL у тебя есть возможность заказать доставку суши, бургеров, пиццы, шашлыков к вагону поезда, в котором ты едешь.']);

$route = Url::to(['/train/suggest'], 'https');
$formatJs = <<< 'JS'
var formatRepoSelection = function (repo) {
    return repo.text;
}
JS;

$this->registerJs($formatJs, yii\web\View::POS_HEAD);

$resultsJs = <<< 'JS'
function (data) {
    return {
        results: data.items,
    };
}
JS;

if(isset($stationform->date)) {
  $this->registerJs("
    $('tbody td').on('click', function (e) {
        var code = $(this).closest('tr').data('key');
        var from = $(this).closest('tr').data('from');
        var where = $(this).closest('tr').data('where');
        if(e.target == this)
location.href = '" . Url::to(['/train'], 'https') . "?train_num=' + code +
'&date=' + '$stationform->date' + '&from=' + from + '&where=' + where;
    });
", yii\web\View::POS_END);
}

?>

<section class="stations_search">
      <div class="container">
        <div class="row">
          <article class="col-lg-12">
            <div class="order-train_way active">
              <h3 class="order-train-way_title order-train-way_title__top">Доставка к поезду</h3>
              <h3 class="order-train-way_title order-train-way_title__changed-text"></h3>
              <?php if(isset($stationform) && !isset($stationform->date)) { ?>
                <?php $this->registerJsFile('/js/totrainlist.js', ['depends' => 'app\assets\IndexAsset']); ?>
                <?php $form = ActiveForm::begin([
                    'options' => ['class' => 'form-inline'],
                ])
                ?>

                <?= $form->field($stationform, 'wherefrom')->widget(Select2::classname(), [
					'language' => 'ru',
                    'value' => '1',
                    'initValueText' => 'Станция отправления', // set the initial display text
                    'options' => ['class' => 'order-train-way_input', 'id' => 'selecttt',
                      'onclick' => 'yaCounter44742985.reachGoal(\'otpravlenie\'); return true;'],
                    'pluginOptions' => [
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Ожидайте результат...'; }"),
                        ],
                        'ajax' => [
                            'url' => $route,
                            'dataType' => 'json',
                            'delay' => 2000,
                            'data' => new JsExpression('function(params) { return {name:params.term}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                          ],
                     ],
                ]);?>

                <div class="form-group">
                    <img src="/image/arrows.png" alt="" class="order-train-way_arrows">
                </div>

                <?= $form->field($stationform, 'wherein')->widget(Select2::classname(), [
					'language' => 'ru',
                    'value' => '1',
                    'initValueText' => 'Станция прибытиия', // set the initial display text
                    'options' => ['class' => 'order-train-way_input'],
                    'pluginOptions' => [
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Ожидайте результат...'; }"),
                        ],
                        'ajax' => [
                            'url' => $route,
                            'dataType' => 'json',
                            'delay' => 2000,
                            'data' => new JsExpression('function(params) { return {name:params.term}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                          ],
                     ],
                ]);?>

                <?= $form->field($stationform,'date')->widget(DatePicker::className(),['options' => ['class' => 'order-train-way_input', 'placeholder' => 'Дата отправления', 'type' => 'text'], 'dateFormat' => 'dd.MM.yyyy'])->label(false) ?>

                <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"></i>Найти', ['class' => 'order-content_btn click', 'onclick' => 'yaCounter44742985.reachGoal(\'serchtrain\'); return getTrainList();']) ?>

                <?php ActiveForm::end() ?>
              <? } ?>
            </div>
          </article>
        </div>
      </div>
    </section>

    <main class="stations_train">
      <section class="container">
        <article class="row">
          <div class="col-lg-12">
            <?php if(isset($trainform->train_num)) { ?>
              <?php $this->title = 'Список станций'; ?>
              <?php $this->registerJsFile('/js/tostations.js', ['depends' => 'app\assets\IndexAsset']); ?>
            <div class="stations-train_info" data-local=<?= $_SESSION['trtime']; ?>>
              <div class="stations-train_borders">
                <table class="stations-train_table" data-id="list_of_stations">
                  <thead class="stations-train-table_header">
                    <tr class="stations-train-table-header_row">
                      <th class="stations-train-table-header_cell"></th>
                      <th class="stations-train-table-header_cell">Станция</th>
                      <th class="stations-train-table-header_cell">Прибытие</th>
                      <th class="stations-train-table-header_cell">Стоянка</th>
                      <th class="stations-train-table-header_cell">Отправление</th>
                    </tr>
                  </thead>
                  <tbody class="stations-train-table_body">
                   <?= TrainsWidget::widget(['tpl' => 'stations', 'trainform' => $trainform]); ?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php } elseif (isset($stationform->wherefrom)) { ?>
              <?php $this->title = 'Список поездов'; ?>
              <div class="stations-train_info">
                <div class="stations-train_borders">
                  <table class="stations-train_table" data-id="list_of_trains">
                    <thead class="stations-train-table_header">
                      <tr class="stations-train-table-header_row">
                        <th class="stations-train-table-header_cell">Номер поезда</th>
                        <th class="stations-train-table-header_cell">Станция отправления</th>
                        <th class="stations-train-table-header_cell">Станция прибытия</th>
                      </tr>
                    </thead>
                    <tbody class="stations-train-table_body">
                     <?= TrainsWidget::widget(['tpl' => 'trains_list', 'stationform' => $stationform]); ?>
                    </tbody>
                  </table>
                </div>
              </div>
            <?php  } ?>
          </div>
        </article>
      </section>
    </main>
