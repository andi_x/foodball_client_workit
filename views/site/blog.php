<?php
use yii\helpers\Html;
?>

    <div id="modal_window" class="modal_window">
      <!-- <div class="modal-window_background"></div> -->
      <div class="modal-window_content">
        <p class="modal-window_text">
          Этот контент на данный момент создается.<br>
          Торжественно клянемся, <br>
          что он будет доступен в ближайшее время!
        </p>

        <a href="javascript://0" class="modal-window_close">X</a>
      </div>
    </div>

    <main id="restourants_page_block" class="restourants-page_block">

      <div class="restourants-page_contain">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 no-padding">
              <section id="restourants_page_contain_restaurants" class="restourants-page-contain_restaurants">
                <h2 class="restourants-page-restaurants_title">Блог</h2>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

                <article class="restourants-page-restaurants_restaurant">
                  <div class="blog_image-block">
                    <img src="/image/restaurant-1.jpg" alt="" class="blog_image">
                  </div>
                  <div class="food-content_order-content">
                    <h3 class="restourants-page-restaurant-type_name">Новый-старый блог</h3>

                    <p class="food-content_text blog-content_text">
                      Описание товара описание описание описание описание описание описание описание
                      описание описание описание описание описание описание описание описание описание описание описание
                    </p>

                    <a href="" class="restourants-page-search_button food-content_bye blog-content_button">
                      <span class="food-content-bye_cost">Подробнее</span>
                    </a>
                  </div>
                </article>

              </section>

            </div>
          </div>
        </div>
      </div>

    </main>
