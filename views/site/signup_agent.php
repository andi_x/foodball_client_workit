<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile('/js/phone_mask.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/checkboxes_style.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/signup_agent.js', ['depends' => 'app\assets\IndexAsset']);

?>

<?php
$this->title = 'Начни зарабатывать с FOOD BALL уже сегодня';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="auth_block">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2 col-xs-offset-0">
        <div class="auth_block__background clearfix">
          <div class="site-login">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Пожалуйста, заполните следующие поля для регистрации:</p>

            <?php $form = ActiveForm::begin([
              'id' => 'login-form',
              'layout' => 'horizontal',
              'fieldConfig' => [
                'template' => "{label}\n<div class=\"auth-block_input\">{input}</div>{hint}\n<div class=\"auth-block_help\">\n{error}</div>",
                'labelOptions' => ['class' => 'auth-block_label control-label'],
              ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'phone')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'email')->input('email') ?>
            <label for="rzd_relation">Отношение к РЖД:</label>
            <?= Html::dropDownList('rzd_select', 'rzd_select', [
                                                                    '1' => 'Я проводник',
                                                                    '2' => 'Я продаю билеты',
                                                                    '3' => 'Свой вариант'
                                                                ], ['id' => 'rzd_select', 'class' => 'form-control', 'style' => 'font-size: 14px; width: 150px; margin-bottom: 10px ']); ?>
            <?= $form->field($model, 'rzd_relation')->hiddenInput(['value' => 'Я проводник'])->label(false) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="auth-block_form-group">
              <div class="col-lg-11">
                <?php if( Yii::$app->session->hasFlash('success') ): ?>
                  <?php echo Yii::$app->session->getFlash('success'); ?>
                  <br>
                  <br>
                <?php endif; ?>
                <?php if( Yii::$app->session->hasFlash('error') ): ?>
                  <?php echo Yii::$app->session->getFlash('error'); ?>
                  <br>
                  <br>
                <?php endif; ?>
                <div class="restourants-page-contain_checkbox-emulator">
                  <input type="checkbox" class="restourants-page-contain_checkbox" required>
                  <label class="restourants-page-contain-checkbox_name">Согласен на обработку моих персональных данных </label>
                  <a href="/politic" class="poltic">Подробнее о политике конфиденциальности</a>
                </div>

                <div class="restourants-page-contain_checkbox-emulator">
                  <input type="checkbox" class="restourants-page-contain_checkbox" required>
                  <label class="restourants-page-contain-checkbox_name">Согласен</label>
                  <a href="/partnership-agent" class="poltic">Подробнее о соглашение для агентов</a>
                </div>

              </div>
              <div class="col-lg-12">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
              </div>
            </div>

            <?php ActiveForm::end(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
