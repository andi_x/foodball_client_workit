<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php
$this->title = 'Подтверждение';
$this->params['breadcrumbs'][] = $this->title;

?>

<section class="auth_block">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2 col-xs-offset-0">
        <div class="auth_block__background clearfix">
          <div class="site-login">
              <h1><?= Html::encode($this->title) ?></h1>

              <?php $form = ActiveForm::begin([
                  'id' => 'login-form',
                  'layout' => 'horizontal',
                  'fieldConfig' => [
                      'template' => "{label}\n<div class=\"auth-block_input\">{input}</div>{hint}\n<div class=\"auth-block_help\">\n{error}</div>",
                      'labelOptions' => ['class' => 'auth-block_label control-label'],
                  ],
              ]); ?>

              <?= $form->field($model, 'code')->textInput(['autofocus' => true]) ?>
              <?= $form->field($model, 'phone')->hiddenInput(['value' => $_SESSION['confirm']])->label(false) ?>

              <div class="form-group">
                  <div class="col-lg-offset-1 col-lg-11">
                      <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary', 'name' => 'confirm-button']) ?>
                  </div>
              </div>

              <?php ActiveForm::end(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
