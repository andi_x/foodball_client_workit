<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/phone_mask.js', ['depends' => 'app\assets\IndexAsset']);
?>

<section class="auth_block">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2 col-xs-offset-0">
        <div class="auth_block__background clearfix">
          <div class="site-login">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Пожалуйста, заполните следующие поля для авторизации:</p>

            <?php $form = ActiveForm::begin([
              'id' => 'login-form',
              'layout' => 'horizontal',
              'fieldConfig' => [
                'template' => "{label}\n<div class=\"auth-block_input\">{input}</div>\n<div class=\"auth-block_help\">{error}</div>",
                'labelOptions' => ['class' => 'auth-block_label control-label'],
              ],
            ]); ?>

            <?= $form->field($model, 'phone')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
              <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
              </div>

              <div class="col-lg-offset-1 col-lg-11">
                <a href="/signup" class="btn btn-primary">Регистрация</a>
              </div>

              <div class="col-lg-offset-3 col-lg-9">
                  <?php if( Yii::$app->session->hasFlash('error') ): ?>
                      <?php echo Html::a('Восстановить пароль',  Url::to(['/reset'], 'https')); ?>
                  <?php endif; ?>
              </div>
            </div>

            <?php ActiveForm::end(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
