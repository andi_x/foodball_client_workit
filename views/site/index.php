<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\widgets\Pjax;

use kartik\select2\Select2;
use yii\web\JsExpression;

$this->registerCssFile('libs/carousel/js/assets/owl.carousel.min.css', ['depends' => 'app\assets\MainAsset']);
$this->registerCssFile('libs/carousel/js/assets/owl.theme.default.min.css', ['depends' => 'app\assets\MainAsset']);

$this->registerJsFile('/js/carousel_index_configurations.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/checkboxes_style.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/gallery.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/index_form.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/toindex.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/totrainlist.js', ['depends' => 'app\assets\IndexAsset']);

$this->title = 'FOOD BALL – доставка еды к поездам и домой по всей России';
$this->registerMetaTag(['name' => 'description', 'content' => 'Уникальный сервис доставки еды в поезда и домой. Заказ еды домой и в поезд из любимых ресторанов.']);

$route = Url::to(['/train/suggest'], 'https');
$formatJs = <<< 'JS'
var formatRepoSelection = function (repo) {
    return repo.text;
}
JS;

$this->registerJs($formatJs, yii\web\View::POS_HEAD);

$resultsJs = <<< 'JS'
function (data) {
    return {
        results: data.items,
    };
}
JS;

$cookies = Yii::$app->request->cookies;
?>

<?php if (!isset($city_id)) { ?>
<?php
    $s = <<< JS
  $(function() {
    jQuery(window).ready(function() {
      jQuery('.home').attr('href','#');
    });
    jQuery(window).load(function() {
      jQuery('a[href="#"]').click(function() {
        jQuery('#modal_window').css('display', 'block');
      });
      jQuery('.modal-window_close').click(function() {
        jQuery('#modal_window').css('display', 'none');
      });
    });
  });
JS;
    $this->registerJs($s, yii\web\View::POS_END);
?>
<div id="modal_window" class="modal_window">
    <!-- <div class="modal-window_background"></div> -->
    <div class="modal-window_content">
      <p class="modal-window_text">
          Упс! Сервис FOOD BALL, к сожалению, еще не работает в этом городе.
        Совсем скоро мы исправим эту ситуацию, а пока скачать наше приложение,
        чтобы иметь возможность заказать в доступных городах.<br>
        <a href="https://itunes.apple.com/ru/app/foodball/id1257321093?mt=8">Ссылка на App Strore</a><br>
        <a href="https://play.google.com/store/apps/details?id=ru.foodballrf.foodballapp&hl=ru">Ссылка на Google play</a><br>
        Хочешь стать нашим партнером?<br>
        <a href="https://foodballrf.ru/partnership">Заполняй форму для партнеров</a>
      </p>

      <a href="javascript://0" class="modal-window_close">X</a>
    </div>
  </div>
<?php } ?>

    <!--Keyvisual Begin-->
    <section class="keyvisual">
      <div class="keyvisual-carousel owl-carousel owl-theme">
        <article class="keyvisual_item item">
          <div class="keyvisual-item_background" style="background-image:url('/image/carousel-item.jpg');"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <h3 class="keyvisual_title">КУПИЛ БИЛЕТ - <br>КУПИ ОБЕД!</h3>
                <p class="keyvisual_text">Проверь какое меню ждет тебя в дороге!</p>

                <form action="" class="keyvisual_form">
                  <a href="#main-order" class="keyvisual_link" type="button" onclick="yaCounter44742985.reachGoal('click'); return true;"><img src="/image/check_icon.png" alt="">ЗАКАЗАТЬ СЕЙЧАС</a>
                </form>
              </div>
            </div>
          </div>
        </article>
        <article class="keyvisual_item item">
          <div class="keyvisual-item_background" style="background-image:url('/image/carousel-item1.jpg');"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <h3 class="keyvisual_title">СКОРО <br>В ДОРОГУ?</h3>
                <p class="keyvisual_text">Закажи питание на всю поездку <br>прямо сейчас!</p>

                <form action="" class="keyvisual_form">
                  <a href="#main-order" class="keyvisual_link keyvisual_link__second" type="button" onclick="yaCounter44742985.reachGoal('click'); return true;"><img src="/image/check_icon.png" alt="">ЗАКАЗАТЬ СЕЙЧАС</a>
                </form>
              </div>
            </div>
          </div>
        </article>
        <article class="keyvisual_item item">
          <div class="keyvisual-item_background" style="background-image:url('/image/carousel-item2.jpg');"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <h3 class="keyvisual_title">НЕОБЫЧНЫЙ <br>СЕРВИС</h3>
                <p class="keyvisual_text">С ОБЫЧНЫМИ ЦЕНАМИ!</p>

                <form action="" class="keyvisual_form">
                  <a href="#main-order" class="keyvisual_link keyvisual_link__third" type="button" onclick="yaCounter44742985.reachGoal('click'); return true;"><img src="/image/check_icon.png" alt="">ЗАКАЗАТЬ СЕЙЧАС</a>
                </form>
              </div>
            </div>
          </div>
        </article>
        <article class="keyvisual_item item">
          <div class="keyvisual-item_background" style="background-image:url('/image/carousel-item3.jpg');"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <h3 class="keyvisual_title">АППЕТИТНАЯ <br>ЗАБОТА</h3>
                <p class="keyvisual_text">Позаботься о близких, <br>закажи для них!</p>

                <form action="" class="keyvisual_form">
                  <a href="#main-order" class="keyvisual_link keyvisual_link__fourth" type="button" onclick="yaCounter44742985.reachGoal('click'); return true;"><img src="/image/check_icon.png" alt="">ЗАКАЗАТЬ СЕЙЧАС</a>
                </form>
              </div>
            </div>
          </div>
        </article>
        <article class="keyvisual_item item">
          <div class="keyvisual-item_background" style="background-image:url('/image/carousel-item4.jpg');"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <h3 class="keyvisual_title">НИКУДА НЕ ЕДЕШЬ???</h3>
                <p class="keyvisual_text">Закажи еду домой! <br>Побалуй себя, удиви друзей!</p>

                <form action="" class="keyvisual_form">
                  <a href="#main-order" class="keyvisual_link keyvisual_link__fiveth" type="button" onclick="yaCounter44742985.reachGoal('click'); return true;"><img src="/image/check_icon.png" alt="">ЗАКАЗАТЬ СЕЙЧАС</a>
                </form>
              </div>
            </div>
          </div>
        </article>
        <article class="keyvisual_item item">
          <div class="keyvisual-item_background" style="background-image:url('/image/carousel-item5.jpg');"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                <h3 class="keyvisual_title">СКАЧАЙ FOODBALL!</h3>
                <p class="keyvisual_text">ЗАБЕЙ #ГОЛОДУГОЛ !</p>

                <a href="https://itunes.apple.com/ru/app/foodball/id1257321093?mt=8" onclick="yaCounter44742985.reachGoal('AppStore'); return true;" class="download_link download_link__yellow"><img src="/image/download_app_store-yellow.png" alt=""></a>
                <a href="https://play.google.com/store/apps/details?id=ru.foodballrf.foodballapp&hl=ru" onclick="yaCounter44742985.reachGoal('Googleplay'); return true;" class="download_link download_link__yellow"><img src="/image/google-play-yellow.png" alt=""></a>

              </div>
            </div>
          </div>
        </article>
      </div>
    </section>
    <!--Keyvisual End-->
    <section id="main-order" class="main-order">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="main-order_title">ЗАКАЖИ ПРЯМО СЕЙЧАС!</h3>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a href="/home" class="main-order_link home">
              <div class="main-order_link-image">
                <img src="/image/main-order_image.png" alt="">
                <p>ЗАКАЗАТЬ ДОМОЙ</p>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <a href="#train_list" class="main-order_link train">
              <div class="main-order_link-image">
                <img src="/image/main-order_image(1).png" alt="">
                <p>ЗАКАЗАТЬ В ПОЕЗД</p>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <form action="" class="keyvisual_form">
              <?php
                Modal::begin([
                    'header' => '<h2 style="text-align: center; font-size: 25px;">Выбрать куда доставить</h2>',
                    'toggleButton' => ['tag'=> 'a', 'class' => 'main-order_link',
                      'onclick' => 'yaCounter44742985.reachGoal("pre-order"); return true;',
                      'label' =>
                      '<div class="main-order_link-image">
                          <img src="/image/main-order_image(2).png" alt="">
                          <p>СДЕЛАТЬ ПРЕДЗАКАЗ</p>
                      </div>
                      '],
                ]);
              ?>
                <?= Html::a('<div class="main-order_link-image"><img src="/image/main-order_image.png" alt="" class="where-popup_image"><p>Заказать домой</p></div>', ['/preord'], ['class' => 'main-order_link home', 'onclick' => 'yaCounter44742985.reachGoal(\'pre-home\');']) ?>
                <?= Html::a('<div class="main-order_link-image"><img src="/image/main-order_image(1).png" alt="" class="where-popup_image"><p>Заказать в поезд</p></div>', ['/train'], ['class' => 'main-order_link', 'onclick' => 'yaCounter44742985.reachGoal(\'pre-train\');']) ?>
              <?php Modal::end();  ?>
            </form>

        </div>
      </div>
    </section>

    <section class="train_list" id="train_list">
        <section class="stations_search">
              <div class="container">
                <div class="row">
                  <article class="col-lg-12">

                    <div class="order-train_way active">
                      <h3 class="order-train-way_title">Для доставки еды в поезд введите станцию отправления, прибытия и дату отправления</h3>
                      <?php if(isset($stationform) && !isset($stationform->date)) { ?>
                        <?php $form = ActiveForm::begin([
                            'options' => ['class' => 'form-inline', 'data' => ['pjax' => true]],
                        ])
                        ?>

                        <?= $form->field($stationform, 'wherefrom')->widget(Select2::classname(), [
    					              'language' => 'ru',
                            'value' => '1',
                            'initValueText' => 'Станция отправления', // set the initial display text
                            'options' => ['class' => 'order-train-way_input', 'id' => 'selecttt',
                              'onclick' => 'yaCounter44742985.reachGoal(\'otpravlenie\'); return true;'],
                            'pluginOptions' => [
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Ожидайте результат...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $route,
                                    'dataType' => 'json',
                                    'delay' => 2000,
                                    'data' => new JsExpression('function(params) { return {name:params.term}; }'),
                                    'processResults' => new JsExpression($resultsJs),
                                    'cache' => true
                                  ],
                             ],
                        ]);?>

                        <div class="form-group">
                            <img src="/image/arrows.png" alt="" class="order-train-way_arrows">
                        </div>

                        <?= $form->field($stationform, 'wherein')->widget(Select2::classname(), [
        					'language' => 'ru',
                            'value' => '1',
                            'initValueText' => 'Станция прибытия', // set the initial display text
                            'options' => ['class' => 'order-train-way_input'],
                            'pluginOptions' => [
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Ожидайте результат...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $route,
                                    'dataType' => 'json',
                                    'delay' => 2000,
                                    'data' => new JsExpression('function(params) { return {name:params.term}; }'),
                                    'processResults' => new JsExpression($resultsJs),
                                    'cache' => true
                                  ],
                             ],
                        ]);?>

                        <?= $form->field($stationform,'date')->widget(DatePicker::className(),['options' => ['class' => 'order-train-way_input', 'placeholder' => 'Дата отправления', 'type' => 'text'], 'dateFormat' => 'dd.MM.yyyy'])->label(false) ?>

                        <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"></i>Найти', ['class' => 'order-content_btn click', 'onclick' => 'yaCounter44742985.reachGoal(\'serchtrain\'); return getTrainList();']) ?>

                        <?php ActiveForm::end() ?>
                      <? } ?>
                    </div>
                  </article>
                </div>
              </div>
            </section>
    </section>

    <section id="mobileApp" class="download">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-7 col-sm-7">
            <h2 class="download_title">СКАЧАЙ ПРИЛОЖЕНИЕ</h2>

            <p class="download_text">И БЕРИ ПРИВЫЧКИ С СОБОЙ!</p>

            <img src="/image/download_img.png" alt="" class="download_img">

            <a href="https://itunes.apple.com/ru/app/foodball/id1257321093?mt=8" onclick="yaCounter44742985.reachGoal('AppStore'); return true;" class="download_link"><img src="/image/app_store.png" alt=""></a>
            <a href="https://play.google.com/store/apps/details?id=ru.foodballrf.foodballapp&hl=ru" onclick="yaCounter44742985.reachGoal('Googleplay'); return true;" class="download_link"><img src="/image/play_market.png" alt=""></a>

            <p class="download_info">
              С этим приложением Ты станешь самым крутым в любом поезде! Твои поездки уже никогда не станут прежними! Сотни ресторанов
              и кафе готовы доставить Тебе свои блюда прямо к вагону!
              Удивляйся Сам! Удивляй Друзей!
            </p>
          </div>
          <div class="col-lg-6 col-md-5 col-sm-5">
            <div class="download_hand">
              <img src="/image/hand-with-tel.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id='main_gallery' class="main_gallery">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <h5 class="main-gallery_title">#голодугол</h5>
            <div id="main-gallery_content">
              <a href="image/drive-eat-1.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-1.jpg');"><span></span></a>
              <a href="image/drive-eat-9.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-9.jpg');"><span></span></a>
              <a href="image/drive-eat-3.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-3.jpg');"><span></span></a>
              <a href="image/drive-eat-4.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-4.jpg');"><span></span></a>
              <a href="image/drive-eat-5.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-5.jpg');"><span></span></a>
              <a href="image/drive-eat-6.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-6.jpg');"><span></span></a>
              <a href="image/drive-eat-7.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-7.jpg');"><span></span></a>
              <a href="image/drive-eat-8.jpg" class="flipLightBox" style="background-image: url('image/drive-eat-8.jpg');"><span></span></a>
            </div>
          </div>
        </div>
      </div>
    </section>
