<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Сброс пароля';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/phone_mask.js', ['depends' => 'app\assets\IndexAsset']);
?>

<section class="auth_block">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-2 col-xs-offset-0">
        <div class="auth_block__background clearfix">
          <div class="site-reset">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>Пожалуйста, введите номер телефона:</p>

            <?php $form = ActiveForm::begin([
              'id' => 'reset-form',
              'layout' => 'horizontal',
              'fieldConfig' => [
                'template' => "{label}\n<div class=\"auth-block_input\">{input}</div>\n<div class=\"auth-block_help\">{error}</div>",
                'labelOptions' => ['class' => 'auth-block_label control-label'],
              ],
            ]); ?>

            <?= $form->field($model, 'phone')->textInput(['autofocus' => true]) ?>

            <div class="form-group">
              <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Сбросить пароль', ['class' => 'btn btn-primary', 'name' => 'reset-button']) ?>
              </div>
            </div>

            <?php ActiveForm::end(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
