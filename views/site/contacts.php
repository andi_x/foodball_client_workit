<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас';
$this->params['breadcrumbs'][] = $this->title;
?>

    <section class="main-subscribtion about_page">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <h3 class="main-subscribtion-form_title">Контакты FOOD BALL</h3>

            <p class="main-subscribtion-form_text">
			  Для коммерческих предложений:
			  </br>
			  <a href="mailto:biz@foodballrf.ru" class="poltic">biz@foodballrf.ru</a>
			</p>
			
			<p class="main-subscribtion-form_text">
			  Для резюме:
			  </br>
			  <a href="mailto:biz@foodballrf.ru" class="poltic">rabota@foodballrf.ru</a>
			</p>
			
			<p class="main-subscribtion-form_text">
			  Для прессы:
			  </br>
			  <a href="mailto:biz@foodballrf.ru" class="poltic">Pressa@foodballrf.ru</a>
			</p>
			
			<p class="main-subscribtion-form_text">
			  Для партнёров:
			  </br>
			  <a href="mailto:biz@foodballrf.ru" class="poltic">partner@foodballrf.ru</a>
			</p>
			
			<p class="main-subscribtion-form_text">
			  Вопросы по работе сервиса:
			  </br>
			  <a href="mailto:biz@foodballrf.ru" class="poltic">client@foodballrf.ru</a>
            </p>
			
          </div>
        </div>
      </div>
    </section>
    
    <!--
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
-->
