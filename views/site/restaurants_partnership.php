<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Ты можешь стать партнером FOOD BALL';
$this->registerMetaTag(['name' => 'description', 'content' => 'Заключи партнерское соглашение с сервисом FOOD BALL и открой для себя новый рынок потенциальных клиентов']);
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="main-subscribtion about_page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h3 class="main-subscribtion-form_title">Партнёрство для ресторанов</h3>

                <p class="main-subscribtion-form_text">
                    Приглашаем рестораны, кафе и супермаркеты для сотрудничества с нашим агрегатором FOOD BALL !
                </p>

                <p class="main-subscribtion-form_text">
                    Ежегодно на поездах дальнего следования проезжает более 100 000 000 человек.
                    И это все потенциальные клиенты. В поезде у человека очень ограничен выбор в сфере питания.
                    И выбирая между вагоном-рестораном, привокзальной палаткой и вашим заведением, с большей долей вероятности, пассажир выберет именно вас.
                    Так как вы более привлекательны для него как по цене, так и по качеству.
                </p>

                <p class="main-subscribtion-form_text">
                    Наш сервис является связующим звеном между вашим заведением и покупателями со всей России, проезжающими через ваш город.
                    При этом вам не нужно арендовать торговые и производственные площади на перронах и вокзалах.
                </p>

                <h3 class="main-subscribtion-form_title">
                    Преимущества доставки к поездам:
                </h3>

                <p class="main-subscribtion-form_text">
                    <ul>
                        <li class="main-subscribtion-form_text">простая логистика (доставка в одно и то же место)</li>
                        <li class="main-subscribtion-form_text">экономия на доставке (возможны несколько заказов в один поезд)</li>
                        <li class="main-subscribtion-form_text">огромные возможности в области франчайзинга (у вас заказывают жители других городов и ваш бренд становится узнаваемым по всей стране)</li>
                        <li class="main-subscribtion-form_text">новый рынок (Развиваясь в рамках своего города, любое заведение рано или поздно достигает своего «потолка».
                            С помощью FOOD BALL вы сможете, к своему привычному обороту, добавить абсолютно новый трафик клиентов)</li>
                    </ul>
                </p>

                <h3 class="main-subscribtion-form_title">Сотрудничать просто!</h3>

                <p class="main-subscribtion-form_text">
                    <ul>
                        <li class="main-subscribtion-form_text">заполните форму заявки (и с вами свяжутся)</li>
                        <li class="main-subscribtion-form_text">заключите договор</li>
                        <li class="main-subscribtion-form_text">получите доступ к личному кабинету</li>
                        <li class="main-subscribtion-form_text">работайте и получайте прибыль</li>
                    </ul>
                </p>

                <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'options' => ['class' => 'main-subscribtion_form clearfix']]); ?>
                <div class="main-subscribtion-form_input-block">
                    <?= $form->field($model, 'city', ['enableLabel' => false])->textInput(array('placeholder' => 'Ваш город', 'class'=>'main-subscribtion-form_input'));  ?>
                    <?= $form->field($model, 'restaurant', ['enableLabel' => false])->textInput(array('placeholder' => 'Ваше название', 'class'=>'main-subscribtion-form_input'));  ?>
                    <?= $form->field($model, 'name', ['enableLabel' => false])->textInput(array('placeholder' => 'Контактное лицо', 'class'=>'main-subscribtion-form_input'));  ?>
                    <?= $form->field($model, 'email', ['enableLabel' => false])->textInput(array('placeholder' => 'Ваша почта', 'class'=>'main-subscribtion-form_input'));  ?>
                    <?= $form->field($model, 'phone', ['enableLabel' => false])->textInput(array('placeholder' => 'Телефон', 'class'=>'main-subscribtion-form_input'));  ?>
                    <?= $form->field($model, 'site', ['enableLabel' => false])->textInput(array('placeholder' => 'Сайт', 'class'=>'main-subscribtion-form_input'));  ?>

                    <?= Html::submitButton('Отправить', ['class' => 'main-subscribtion-form_subscribe', 'name' => 'button']) ?>

                </div>
                <?php ActiveForm::end(); ?>

                <p class="main-subscribtion-form_text">
                    Вопросы по работе сервиса: <a href="mailto:client@foodballrf.ru">client@foodballrf.ru</a>
                </p>

                <h3 class="main-subscribtion-form_title">Успехов вам!</h3>

            </div>
        </div>
    </div>
</section>
