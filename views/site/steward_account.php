<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
use kartik\date\DatePicker;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/js/classTimer.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/user_page.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/tohistory.js', ['depends' => 'app\assets\IndexAsset']);
?>
    <section class="main-subscribtion about_page">
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-md-2 col-lg-offset-1 col-md-offset-1 user-page_list clearfix">
            <a href="#important" class="user-page_link user-page_link__red open">Важно</a>
            <a href="#2" class="user-page_link user-page_link__orange">Агент Foodball</a>
            <a href="#5" class="user-page_link user-page_link__blue" onclick="">Профиль</a>
            <a href="#history_account" class="user-page_link user-page_link__green" onclick="return getHistory()">История</a>
            <a href="#3" class="user-page_link user-page_link__yellow">Обратная связь</a>
          </div>
          <div class="col-lg-6 col-md-7 user-page_content">
            <div id="important" class="user-page_tab open">

                <?php if (!empty($messages)) {
                    foreach ($messages as $message): ?>
                    <?php if ($message->status == 0): ?>
                        <a href="#<?= $message->id ?>" class="message new" data-id="<?= $message->id ?>">
                    <?php else: ?>
                        <a href="#<?= $message->id ?>" class="message">
                    <?php endif; ?>
                    <h4 class="message_title"><?= $message->title ?></h4>
                    <p class="message_text">
                        <?= $message->content ?>
                    </p>
                    </a>
                <?php endforeach;
                }?>
            </div>

            <div id="2" class="user-page_tab">
              <div class="conductor_time">
                <input type="date" id="order_from" name="" value="" class="user-page_input">

                <input type="date" id="order_to" name="" value="" class="user-page_input">
              </div>

              <?= Html::tag('p', 'Ваш промокод: ' . '<b>' . Html::encode($promo) . '</b>', ['class' => 'conductor_text']); ?>
              <?= Html::tag('p', 'Всего заказов: ' . '<b>' . Html::encode($statistic->total_orders) . '</b>', ['class' => 'conductor_text']); ?>
              <?= Html::tag('p', 'Выполненных заказов: ' . '<b>' . Html::encode($statistic->complete_orders) . '</b>', ['class' => 'conductor_text']); ?>
              <?= Html::tag('p', 'Прибыль: ' . '<b>' . Html::encode($statistic->balance) . '</b>', ['class' => 'conductor_text']); ?>
              <?= Html::tag('span', 'Минимальная сумма для вывода средств <b>2000</b> рублей', ['class' => 'conductor_text']); ?>


              <button type="button" name="button" class="conductor_btn">Вывести</button>
            </div>

            <div id="3" class="user-page_tab">
                <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
                    <div class="alert alert-success">
                        Спасибо за сообщение. Мы постараемся связаться с вами как можно скорее.
                    </div>
                <?php endif; ?>

                <?php if (Yii::$app->session->hasFlash('contactFormError')): ?>
                    <div class="alert alert-error">
                        Форма заполнена не верно.
                    </div>
                <?php endif; ?>

                <?php Pjax::begin() ?>
                <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => ['data-pjax' => true]]); ?>

                    <?= $form->field($model, 'subject')->textInput(['class' => 'user-page_input'])->label('Введите тему сообщения') ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 3, 'class' => 'user-page_input']) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="">{image}</div></br><div class="user-page_input">{input}</div>',
                    ])->label('Введите капчу') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary user-page_btn', 'name' => 'contact-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
                <?php Pjax::end(); ?>
            </div>

            <div id="history_account" class="user-page_tab">
            </div>

            <div id="5" class="user-page_tab">
                <?php if (Yii::$app->session->hasFlash('profileFormSubmitted')): ?>
                    <div class="alert alert-success">
                        Данные успешно обновлены
                    </div>
                <?php endif; ?>

                <?php if (Yii::$app->session->hasFlash('profileFormError')): ?>
                    <div class="alert alert-error">
                        Форма заполнена не верно.
                    </div>
                <?php endif; ?>

                <?php Pjax::begin() ?>
                <?php $form = ActiveForm::begin(['id' => 'profile-form', 'options' => ['data-pjax' => true]]); ?>

                    <?= $form->field($profile, 'name')->textInput(['class' => 'user-page_input']) ?>

                    <?= $form->field($profile, 'birthday')->widget(DatePicker::className(),
                     ['type'=>1 , 'options' => ['class' => 'user-page_input'], 'pluginOptions' => ['autoclose'=>true, 'format'=>'yyyy-mm-dd']]) ?>

                    <?= $form->field($profile, 'gender')->dropDownList(
                        [
                            '1' => 'Мужской',
                            '0' => 'Женский',
                        ],
                        [
                            'prompt' => '',
                            'class' => 'user-page_input',
                        ]
                    ) ?>

                    <?= $form->field($profile, 'email')->textInput(['class' => 'user-page_input']) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary user-page_btn', 'name' => 'profile-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
                <?php Pjax::end(); ?>
            </div>

          </div>
        </div>
      </div>
    </section>

    <div class="message_content">
      <div class="message-content_block">
        <a href="javascript://0" class="message-content_close"></a>
        <p class="message-content_title"></p>
        <p class="message-content_text"></p>
      </div>
    </div>
