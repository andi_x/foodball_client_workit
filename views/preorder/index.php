<?php
use app\components\widgets\RestorauntWidget;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

use kartik\select2\Select2;
use yii\web\JsExpression;

$this->registerCssFile('/libs/rangeslider/css/ion.rangeSlider.css', ['depends' => 'app\assets\MainAsset']);
$this->registerCssFile('/libs/rangeslider/css/ion.rangeSlider.skinFlat.css', ['depends' => 'app\assets\MainAsset']);
$this->registerJsFile('/js/classTimer.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/topreorder.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('libs/rangeslider/js/ion.rangeSlider.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/checkboxes_style.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/asside_hamburger.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/correct_height.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/kitchen_height.js', ['depends' => 'app\assets\IndexAsset']);

$this->title = 'Заказать бургеры, пиццу, суши, шашлык с помощью FOOD BALL';
$this->registerMetaTag(['name' => 'description', 'content' => 'Быстрая доставка бургеров, пиццы, суши, роллов и шашлыков с помощью сервиса FOOD BALL.']);
$route = Url::to(['/suggest'], 'https');

$formatJs = <<< 'JS'
var formatRepoSelection = function (repo) {
    return repo.text;
}
JS;
$this->registerJs($formatJs, yii\web\View::POS_HEAD);

$resultsJs = <<< 'JS'
function (data) {
    return {
        results: data.items,
    };
}
JS;

if (!empty($_SESSION['preorder_modal'])) {

$preordJs = "
    $(\"a[name='".$_SESSION['preorder_modal']."']\").click();
";

$this->registerJs($preordJs, yii\web\View::POS_READY);

}
?>

<main id="restourants_page_block" class="restourants-page_block">
<div class="restourants-page_contain">
  <div class="container">
    <div class="row">

      <aside class="col-lg-3 col-md-3 col-sm-0 no-padding">
        <div id="asideMenu" class="restourants-page-contain_sidebar">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => ''],
            ])
            ?>
          <div class="restourants-page-contain_search">
            <?= $form->field($filters, 'name')->widget(Select2::classname(), [
                'language' => 'ru', // set the initial display text
                'options' => ['class' => 'restourants-page-contain-search_input', 'id' => 'restoraunt', 'placeholder' => 'название ресторана'],
                'pluginOptions' => [
                    'size' => 'lg',
                    'hideSearch' => 'true',
                    'minimumInputLength' => 2,
                    'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Ничего не найдено'; }"),
                    ],
                    'ajax' => [
                        'url' => $route,
                        'dataType' => 'json',
                        'delay' => 2000,
                        'data' => new JsExpression('function(params) { return {name:params.term}; }'),
                        'processResults' => new JsExpression($resultsJs),
                        'cache' => true
                      ],
                 ],
                 'pluginEvents' => [
                    "select2:select" => "function() { send(); }",
                 ],
            ])->label(false);?>

            <h3 class="restourants-page-contain-search_title">кухня</h3>
            <a href="javascript://0" id='clear' class="restourants-page-contain-search_clear">сброс</a>
          </div>

          <div class="restourants-page-contain_min-cost">
            <h3 class="restourants-page-contain-min_title">МИН. СУММА</h3>
            <div class="input-number">
              <!-- <span class="input-number_button decr">-</span> -->
              <?= $form->field($filters, 'min')->input('number', [
                                                      'id' => 'range_value',
                                                      'class' => 'restourants-page-contain-min_input',
                                                    ])->label(false) ?>

              <?= $form->field($filters, 'max')->input('number', [
                                                      'id' => 'range_second_value',
                                                      'class' => 'restourants-page-contain-min_input',
                                                    ])->label(false) ?>
              <!-- <span class="input-number_button incr">+</span> -->
              <input type="text" id="range" value="" name="range" />
            </div>
          </div>


          <div class="restourants-page-contain_foods-checks">
            <div class="restourants-page-contain-foods-checks_container">
            <?= $form->field($filters,'categories', ['options' => ['class' => 'restourants-page-contain_checkbox', 'onClick' => 'send()']])
                    ->checkboxList($product_filter, [
                                    'class' => 'restourants-page-contain_checkbox-emulator',
                                    'separator' => '</div><div class="restourants-page-contain_checkbox-emulator">',
                                    'itemOptions' => ['class' => 'products_filter'],
                    ])->label(false) ?>
            </div>
            <a href="javascript://0" class=
            "restourants-page-contain_foods-more">Еще...</a>

          </div>

          <div class="restourants-page-contain_foods-checks">
            <div class="restourants-page-contain-foods-checks_container">
            <?= $form->field($filters,'types', ['options' => ['class' => 'restourants-page-contain_checkbox', 'onClick' => "send()"]])
                    ->checkboxList($agent_filter, [
                                    'class' => 'restourants-page-contain_checkbox-emulator',
                                    'separator' => '</div><div class="restourants-page-contain_checkbox-emulator">',
                                    'itemOptions' => ['class' => 'agent_filter restourants-page-contain_checkbox-emulator'],
                    ])->label(false) ?>
            </div>
          </div>
          <?php ActiveForm::end() ?>
          <!-- <div class="restourants-page-contain_criterions-checks">
            <h3 class="restourants-page-contain-criterions_title">Критерии</h3>

            <div class="restourants-page-contain_checkbox-emulator">
              <input type="checkbox" name="shares" value="" class="restourants-page-contain_checkbox">
              <label for="shares" class="restourants-page-contain-checkbox_name">Акции</label>
            </div>

            <div class="restourants-page-contain_checkbox-emulator">
              <input type="checkbox" name="rating" value="" class="restourants-page-contain_checkbox">
              <label for="rating" class="restourants-page-contain-checkbox_name">По рейтингу</label>
            </div>

            <div class="restourants-page-contain_checkbox-emulator">
              <input type="checkbox" name="express" value="" class="restourants-page-contain_checkbox">
              <label for="express" class="restourants-page-contain-checkbox_name">ЭКСПРЕСС-доставка</label>
            </div>
          </div> -->

          <div class="restourants-page-contain_info">
            <p class="restourants-page-contain-info_text">
              <b>FOODBALL</b> - уникальный независимый сервис по доставке
              еды продуктов и товаров первой необходимости к
              поездам и дверям ваших домов.
              FOODBALL - это хорошая привычка которую можно брать с собой.
            </p>

          </div>
        </div>
      </aside>

      <div class="col-lg-9 col-md-9 col-sm-12 no-padding">
        <section id="restourants_page_contain_restaurants" class="restourants-page-contain_restaurants preorder_page">
          <h2 class="restourants-page-restaurants_title">Список ресторанов</h2>
            <?php
                echo RestorauntWidget::widget(['tpl' => 'restoraunts', 'timezone' => $timezone]);
            ?>
        </section>
      </div>
    </div>
  </div>
</div>

<a href="javascript://0" id="asideHamburger" class="aside_hamburger">
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
  <span class="aside-hamburger_layer"></span>
</a>
</main>

<?php
Modal::begin([
'id'     => 'preOrdModal',
'size'   => 'modal-sm',
'header' => '<h4>В данный момент ресторан <br /> не работает</h4>',
'closeButton' => false,
]);

echo '<h4>Хотите совершить предзаказ?</h4>';
echo '<div class="row">
        <button type="button" class="btn btn-default preorder">Предзаказ</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="return cancelPreord()">Отмена</button>
    </div>';

Modal::end();
?>
