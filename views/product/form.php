<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\helpers\AgentHelper;

$this->registerJsFile('/js/tocart.js', ['depends' => 'app\assets\IndexAsset']);
?>


<article class="restourants-page-restaurants_restaurant">
  <h3 class="restourants-page-restaurant-type_name"><?= Html::encode($product->name) ?></h3>
  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
    <div class="restourants-page-restaurants_restaurant-logo"
      style="background-image: url(<?= $product->image ? 'https://new.foodballrf.ru/images/products/' . $product->image : 'https://new.foodballrf.ru/images/products/none.jpg' ?>);" onError="this.src='https://new.foodballrf.ru/images/products/none.jpg'">
      <!-- <img class="restourants-resize" src="../image/restourants-size.png" alt=""> -->
    </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-8">
    <div class="food-content_order-content">
      <p class="food-content_text food-content-text_description">
          <?= Html::encode($product->description) ?>
      </p>
    </div>
  </div>
  <div class="col-lg-2 col-md-3 col-sm-6 text-center">
    <div class="food-content_order-content">
     <span class="food-content_text food-content-text_count">Количество:</span>
     <div class="input-number">
       <?php if (!$product->for_points) { ?>
           <span class="input-number_button decr" onclick="return Decr();">-</span>
           <input name="count" min="1" max="50" type="number" id="<?= 'count-modal' . $product->id ?>" value="1" class="food-content-order-content_input" />
           <span class="input-number_button incr" onclick="return Incr();">+</span>
       <?php } else  { ?>
           <input name="count" min="1" max="50" type="number" id="<?= 'count-modal' . $product->id ?>" value="1" class="food-content-order-content_input" readonly/>
       <?php } ?>
     </div>
   </div>
  </div>
  <div class="col-lg-3 col-md-2 col-sm-6 text-center">
    <span class="food-content_text food-content-text_cost">
      <?= AgentHelper::Price($product->price) ?> <span class="food-cost_currency"><?= $product->for_points ? 'Баллов' : 'Руб.'?></span>
    </span>
    <a href="javascript://0" class="restourants-page-search_button food-content_bye" onclick="return addCart(<?= $product->id ?>)">
      <span class="food-content-bye_text">в корзину</span>
    </a>
  </div>
</article>
