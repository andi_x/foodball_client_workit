<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\components\helpers\AgentHelper;
use yii\helpers\StringHelper;
use yii\helpers\Html;

$this->registerCssFile('libs/carousel/js/assets/owl.carousel.min.css', ['depends' => 'app\assets\MainAsset']);
$this->registerCssFile('libs/carousel/js/assets/owl.theme.default.min.css', ['depends' => 'app\assets\MainAsset']);
$this->registerJsFile('/js/carousel_tohome.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/toproduct.js', ['depends' => 'app\assets\IndexAsset']);

$this->title = 'Список продуктов';
?>

    <main id="restourants_page_block" class="restourants-page_block products_block">
      <section class="order-home_food">
        <div class="container">
          <div class="row">
            <div class="owl-carousel food-carousel">
              <?php if (!empty($products_categories)): ?>
              <?php foreach ($products_categories as $id => $category) { ?>
                <div class="">
                  <a href=<?= Url::current(['category' => $id, 'cities' => null, 'page' => 1]) ?> class="order-home-food_link"><img src="/image/order-home-food-1.png" alt=""><span><?= Html::encode($category['name']) ?></span></a>
                </div>
              <?php } ?>
            <?php endif; ?>
            <div class="">
              <a href=<?= Url::current(['cities' => null, 'category' => null, 'page' => 1]) ?> class="order-home-food_link"><img src="/image/order-home-food-1.png" alt="">
              <span>Общая</span></a>
            </div>
            </div>
          </div>
        </div>
      </section>
  </br>
      <div class="restourants-page_contain">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 col-md-12 col-sm-12 col-lg-offset-1 col-md-offset-0 no-padding">
              <div class="text-center">
                  <div class="restourants-page-restaurants_restaurant-logo history-block_restaurant-logo" style="background-image: url(<?= $agent_info->image ? 'https://new.foodballrf.ru/images/agents/' . $agent_info->image : 'https://new.foodballrf.ru/images/agents/none.jpg' ?>); border: aliceblue;">
                    <!-- <img class="restourants-resize" src="../image/restourants-size.png" alt=""> -->
                  </div>
                <?= LinkPager::widget(['pagination' => $pagination]) ?>
              </div>

              <section id="restourants_page_contain_restaurants" class="restourants-page-contain_restaurants">
                <h2 class="restourants-page-restaurants_title">Выбор товаров</h2>
                <?php if (!empty($products_list)) { ?>
                  <?php foreach ($products_list as $product) { ?>
                    <article class="restourants-page-restaurants_restaurant">
                      <h3 class="restourants-page-restaurant-type_name mobile-restourant_title"><?= Html::encode($product->name) ?></h3>
                      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
                        <div class="restourants-page-restaurants_restaurant-logo"
                          style="background-image: url(<?= $product->image ? 'https://new.foodballrf.ru/images/products/' . $product->image : 'https://new.foodballrf.ru/images/products/none.jpg' ?>);" onError="this.src='https://new.foodballrf.ru/images/products/none.jpg'">
                          <!-- <img class="restourants-resize" src="../image/restourants-size.png" alt=""> -->
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-8">
                        <div class="food-content_order-content">
                          <h3 class="restourants-page-restaurant-type_name food-content_title"><?= $product->name ?></h3>
                          <p class="food-content_text food-content-text_description food-desktop">
                            <?= StringHelper::truncate(Html::encode($product->description), 130) ?>
                            <button type="button" name="button" role="button" class="btn-link food-content-text_btn" onclick="return getProduct(<?= $product->id ?>);">Подробнее</button>
                          </p>
                          <p class="food-content_text food-content-text_description food-mobile">
                              <?= Html::encode($product->description) ?>
                          </p>
                        </div>
                      </div>
                      <div class="col-lg-2 col-md-3 col-sm-6 text-center">
                        <div class="food-content_order-content product_quantity">
                         <span class="food-content_text food-content-text_count">Количество:</span>
                         <div class="input-number">
                        <?php if (!$product->for_points) { ?>
                           <span class="input-number_button decr">-</span>
                           <input name="count" min="1" max="50" type="number" id="<?= 'count' . $product->id ?>" value="1" class="food-content-order-content_input" />
                           <span class="input-number_button incr">+</span>
                        <?php } else  { ?>
                            <input name="count" min="1" max="1" type="number" id="<?= 'count' . $product->id ?>" value="1" class="food-content-order-content_input" readonly/>
                        <?php } ?>
                         </div>
                       </div>
                      </div>
                      <div class="col-lg-3 col-md-2 col-sm-6 text-center">
                        <span class="food-content_text food-content-text_cost">
                          <?= AgentHelper::Price($product->price) ?> <span class="food-cost_currency"><?= $product->for_points ? ' Баллов' : ' Руб.'?></span>
                        </span>
                        <a href=<?= Url::to(['cart/add', 'id' => $product->id], 'https') ?> class="restourants-page-search_button food-content_bye" data-id="<?= $product->id ?>">
                          <span class="food-content-bye_text">в корзину</span>
                        </a>
                      </div>
                    </article>
                  <?php } ?>
                <?php } ?>
              </section>

              <div class="text-center">
                <?= LinkPager::widget(['pagination' => $pagination]) ?>
              </div>
            </div>
          </div>
        </div>
      </div>

    </main>


    <?php
    Modal::begin([
    'id'     => 'cart',
    'size'   => 'modal-lg',
    'footer' => '<button type="button" class="btn btn-default"
    data-dismiss="modal">Продолжить покупки</button>'.
    '<button type="button" class="btn btn-success" id="issue-order-button" onclick="checkout()">Оформить заказ</a>
    <button type="button" class="btn btn-danger" id="clear-cart-button" onclick="clearCart()">Очистить корзину</button>',
    ]);

    Modal::end();
    ?>

    <?php
    Modal::begin([
    'id'     => 'product-modal',
    'size'   => 'modal-lg',
    ]);

    Modal::end();
    ?>

    <?php
    Modal::begin([
        'id'     => 'notice',
        'size'   => 'modal-lg',
    ]);
    echo "Товар добавлен в корзину";

    Modal::end();
    ?>
