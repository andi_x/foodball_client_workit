<?php
use yii\helpers\Html;
?>

<?php if (!Yii::$app->user->isGuest) { ?>
    <div class="restourants-page__bonusses-block">
        <h3 class="restourants-page-restaurants_title restourants-page-restaurants_title__bonusses"><?= $points . ' баллов' ?></h3>
        <?php if (isset($res)) { ?>
            <h3 "restourants-page-restaurants_title restourants-page-restaurants_title__bonusses"><?= $res ?></h3>
        <?php } ?>
        <input type="text" name="promo-code" id="promo-code" value="" style="display: none" class="promo-code">
        <button type="button" name="promobtn" id="promo-btn" onclick="showPromoForm()" class="btn promo-btn">Ввести промокод</button>
    </div>
<?php } ?>

<?php if(!empty($session['cart'])): ?>
  <h2 class="restourants-page-restaurants_title">Корзина</h2>
    <table class="basket-content_table">
      <thead class="stations-train-table_header">
        <tr class="stations-train-table-header_row">
          <th class="basket-content-header_cell">Фото</th>
          <th class="basket-content-header_cell">Название</th>
          <th class="basket-content-header_cell">Количество</th>
          <th class="basket-content-header_cell">Стоимость</th>
          <th class="basket-content-header_cell">Удалить</th>
        </tr>
      </thead>
      <tbody class="stations-train-table_body">

        <?php foreach ($session['cart'] as $id => $product): ?>
          <tr class="stations-train-table_row selected_row">
            <td class="basket-content_cell">
              <img src=<?= $product['image'] ? 'http://new.foodballrf.ru/images/products/' . $product['image'] : 'http://new.foodballrf.ru/images/products/none.jpg' ?> alt="" onError="this.src='http://new.foodballrf.ru/images/products/none.jpg'" class="basket-page_food-image">
            </td>
            <td class="basket-content_cell"><?= $product['name'] ?></td>
            <td class="basket-content_cell"><?= $product['count'] ?></td>
            <td class="basket-content_cell"><?= $product['for_points'] == 1 ? $product['price'] . ' баллов' : $product['price'] . ' Руб.'?></td>
            <td class="basket-content_cell"><span data-id="<?= $id ?>" class="glyphicon glyphicon-remove del-item text-danger" aria-hidden="true"></span></td>
          </tr>
        <?php endforeach; ?>

        <tr class="stations-train-table_row selected_row">
          <td class="basket-content_cell" colspan="4">Итого: </td>
          <td id="cartcount" class="basket-content_cell"><?= $session['cart.count'] . ' шт.' ?></td>
        </tr>

        <tr class="stations-train-table_row selected_row">
          <td class="basket-content_cell" colspan="4">На сумму: </td>
          <td  id="cart-sum" class="basket-content_cell"><?= $session['cart.sum'] ? $session['cart.sum'] . ' Руб.' : '0 Руб.' ?></td>
        </tr>

        <tr class="stations-train-table_row selected_row">
          <td class="basket-content_cell" colspan="4">Минимальная сумма заказа</td>
          <td id="cart-minprice" class="basket-content_cell"><?= $minprice . ' Руб.'?></td>
        </tr>

        <tr class="stations-train-table_row selected_row">
            <td class="basket-content_cell" colspan="4"><span>Стоимость доставки</span></td>
            <td class="basket-content_cell"><?= $delivery_price . ' Руб.' ?></td>
        </tr>

      </tbody>
    </table>
  <?php else: ?>
    <h3 class="restourants-page-restaurants_title">Корзина пуста</h3>
  <?php endif; ?>
