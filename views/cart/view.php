<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use app\components\widgets\CloudPayWidget;

$this->registerJsFile('/js/phone_mask.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/tocart.js', ['depends' => 'app\assets\IndexAsset']);
$this->registerJsFile('/js/tocartview.js', ['depends' => 'app\assets\IndexAsset']);


$this->title = 'Заказ';
?>

   <main id="restourants_page_block" class="restourants-page_block basket_page">
     <div class="restourants-page_contain">
		<div class="container">
         <div class="row">
           <div class="col-lg-8 col-md-10 col-sm-12 col-lg-offset-2 col-md-offset-1 col-sm-offset-0 no-padding">
             <section id="restourants_page_contain_restaurants" class="restourants-page-contain_restaurants">

            <?php if (!Yii::$app->user->isGuest) { ?>
                <div class="restourants-page__bonusses-block">
                    <h3 class="restourants-page-restaurants_title restourants-page-restaurants_title__bonusses"><?= $points . ' баллов' ?></h3>
                    <input type="text" name="promo-code" id="promo-code" value="" style="display: none" class="promo-code">
                    <button type="button" name="promobtn" id="promo-btn" onclick="sendPromo()" class="btn promo-btn">Ввести промокод</button>
                </div>
            <?php } ?>

                 <h2 class="restourants-page-restaurants_title">Корзина</h2>

                 <div class="basket_table-block">
                   <table class="basket-content_table">
                     <thead class="stations-train-table_header">
                       <tr class="stations-train-table-header_row">
                         <th class="basket-content-header_cell">Фото</th>
                         <th class="basket-content-header_cell">Название</th>
                         <th class="basket-content-header_cell">Количество</th>
                         <th class="basket-content-header_cell">Стоимость</th>
                         <th class="basket-content-header_cell">Сумма</th>
                       </tr>
                     </thead>
                     <tbody class="stations-train-table_body">

                       <?php foreach ($session['cart'] as $id => $product): ?>
                         <tr class="stations-train-table_row selected_row">
                           <td class="basket-content_cell">
                             <img src=<?= $product['image'] ? 'http://new.foodballrf.ru/images/products/' . $product['image'] : 'http://new.foodballrf.ru/images/products/none.jpg' ?> alt="" onError="this.src='http://new.foodballrf.ru/images/products/none.jpg'" class="basket-page_food-image">
                           </td>
                           <td class="basket-content_cell"><?= $product['name'] ?></td>
                           <td class="basket-content_cell"><?= $product['count'] ?></td>
                           <td class="basket-content_cell"><?= $product['price'] ?></td>
                           <td class="basket-content_cell"><?= $product['price'] * $product['count'] ?></td>
                         </tr>
                       <?php endforeach; ?>

                       <tr class="stations-train-table_row selected_row">
                         <td class="basket-content_cell" colspan="4">Итого: </td>
                         <td class="basket-content_cell"><?= $session['cart.count'] ?></td>
                       </tr>

                       <tr class="stations-train-table_row selected_row">
                         <td class="basket-content_cell" colspan="4">На сумму: </td>
                         <td class="basket-content_cell"><?= $amount ?> руб.</td>
                       </tr>

                       <tr class="stations-train-table_row selected_row">
                           <td class="basket-content_cell" colspan="5"><span>Стоимость доставки включена</span></td>
                       </tr>

                     </tbody>
                   </table>
                 </div>

               <?php $form = ActiveForm::begin(); ?>

               <?php if(isset($_SESSION['station'])) { ?>
                 <?= $form->field($order, 'delivery_at')->textInput(['autofocus' => true, 'readonly' => true, 'value' => $_SESSION['data']])->label('Время доставки по местному времени')?>
             <?php } elseif($_SESSION['preOrd'] !== 1) { ?>
                 <?= $form->field($order,'delivery_at')->hiddenInput(['value' => $_SESSION['data']])->label(false); ?>
             <?php } elseif($_SESSION['preOrd'] === 1) { ?>
                 <?= $form->field($order,'delivery_at')->widget(DateTimePicker::className(),
                  ['convertFormat' => true, 'options' => ['class' => 'stations-search_input', 'placeholder' => 'Время доставки по местному времени'],
                   'pluginOptions' => ['format' => 'yyyy-MM-dd h:i', 'todayHighlight' => true, 'startDate' => $_SESSION['data'],
                   'endDate' => $_SESSION['data_end']]])->label('Время доставки по местному времени') ?>
               <?php } ?>

                <?php if (isset($_SESSION['station'])) { ?>
                    <?php  $city = isset($_SESSION['order_city']) ? $_SESSION['order_city'] : Yii::$app->request->cookies['city'];?>
      				<?= $form->field($order, 'delivery_address')->textArea(['autofocus' => true,
                     'value' =>
                     'Город: ' . $city . " \n"
                     . ' Станция: ' . $_SESSION['station'] . " \n"
                     . ' Номер поезда: ' . $_SESSION['train_num'] . " \n"
                     . ' Номер вагона: ' . $_SESSION['wagon_num'] . " \n"
                     . ' Время стоянки: ' . $_SESSION['stop'] . " \n"
                     , 'readonly' => true, 'rows' => 4]) ?>
      		    <?php } else { ?>
      				<?= $form->field($order, 'delivery_address')->textInput(['autofocus' => true]) ?>
                <?php } ?>
               <?= $form->field($order, 'name')->textInput(['autofocus' => true, 'value' => $name]) ?>
               <?= $form->field($order, 'phone')->textInput(['autofocus' => true, 'value' => $phone]) ?>
               <?= $form->field($order, 'promo')->textInput(['autofocus' => true]) ?>
               <?= $form->field($order, 'note')->textArea(['autofocus' => true, 'rows' => 3]) ?>
               <?= $form->field($order, 'payment_type')->dropDownList($pay_types) ?>
               <?= $form->field($order, 'transaction_id')->hiddenInput()->label(false) ?>

                <?php if (($_SESSION['CloudPay']) === false) { ?>
                     <?= CloudPayWidget::widget(['selector' => '#pay-button', 'amount' => $amount, 'accountId' => $order->phone, 'invoiceId' => $_SESSION['trans_id']]);?>
                <?php } ?>

                <div class="form-group">
                    <div>
                        <?= Html::submitbutton('Заказать и оплатить', ['id' => 'pay-button', 'class' => 'btn btn-primary', 'style' => 'display:none']); ?>
                        <?= Html::submitButton('Заказать', ['id' => 'submit-button', 'class' => 'btn btn-primary']) ?>
                    </div>
                </div>

               <?php ActiveForm::end(); ?>
             </section>
           </div>
         </div>
       </div>
     </div>
   </main>
