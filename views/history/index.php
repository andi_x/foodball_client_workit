<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\helpers\StringHelper;
?>

<?php if(!empty($items)): ?>


      <table class="basket-content_table">
        <thead class="stations-train-table_header">
          <tr class="stations-train-table-header_row">
            <th class="basket-content-header_cell">ФИО</th>
            <th class="basket-content-header_cell">Дата заказа</th>
            <th class="basket-content-header_cell">Контактный телефон</th>
            <th class="basket-content-header_cell">Статус</th>
          </tr>
        </thead>
        <tbody class="stations-train-table_body">
              <?php foreach ($items as $item): ?>
                <?php
                switch ($item['status']) {
                  case '1':
                    $item['status'] = 'В ожидании ответа';
                    break;
                  case '2':
                    $item['status'] = 'Поступил в обработку';
                    break;
                  case '3':
                    $item['status'] = 'Выполнен';
                    break;
                  case '4':
                    $item['status'] = 'Отменен';
                    break;
                } ?>
                <tr onclick="window.location.href='<?= Url::to(['/history/view', 'id' => $item['id']], 'https') ?>'" class="stations-train-table_row">
                  <td class="basket-content_cell"><?= StringHelper::InColumn($item['name'], 40) ?></td>
                  <td class="basket-content_cell"><?= $item['delivery_at'] ?></td>
                  <td class="basket-content_cell"><?= $item['phone'] ?></td>
                  <td class="basket-content_cell"><?= $item['status'] ?></td>
                </tr>
              <?php endforeach; ?>
        </tbody>
      </table>
  <?php else: ?>
    <h3 class="restourants-page-restaurants_title">История заказов пуста</h3>
  <?php endif; ?>
