<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\helpers\StringHelper;

$this->registerJsFile('/js/history-status_color.js', ['depends' => 'app\assets\IndexAsset']);
$this->title = 'История заказов';
?>

<section class="history_block">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="history-block_content clearfix">

          <?php if (isset($order_info->id)) { ?>
            <?php
              switch ($order_info->order_agents[0]->status) {
                case '1':
                  $order_info->status = 'В ожидании ответа';
                  break;
                case '2':
                  $order_info->status = 'Поступил в обработку';
                  break;
                case '3':
                  $order_info->status = 'Выполнен';
                  break;
                case '4':
                  $order_info->status = 'Отменен';
                  break;
              } ?>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="restourants-page-restaurants_restaurant-logo history-block_restaurant-logo" style="background-image: url(<?= $agent_info->image ? 'http://new.foodballrf.ru/images/agents/' . $agent_info->image : 'http://new.foodballrf.ru/images/agents/none.jpg' ?>);">
            </div>
          </div>

          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <h3 class="history-block_agent-name"><?= $agent_info->name ?></h3>
            <span class="history-block_costumer">Оформил: </br><?= StringHelper::InColumn($order_info->name, 40) ?></span>
            <span class="history-block_costumer">По адресу: <?= StringHelper::InColumn($order_info->delivery_address, 60) ?></span>
            <span class="history-block_date">Оформлен: <?= $order_info->delivery_at ?></span>
            <span class="history-block_status"><?= $order_info->status ?></span>
          </div>

          <!-- <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

            <div class="history-block_table-block">
              <div class="history-block_table-content">
                <table class="history-block_table">
                  <thead class="history-block-table_header">
                    <tr class="history-block-table-header_row">
                      <th class="history-block-table-header_cell">Фото</th>
                      <th class="history-block-table-header_cell">Название</th>
                      <th class="history-block-table-header_cell">Количество</th>
                      <th class="history-block-table-header_cell">Цена</th>
                    </tr>
                  </thead>
                  <tbody class="history-block-table_body">
                    <tr class="history-block-table_row">
                      <th class="history-block-table_cell"><img src="http://new.foodballrf.ru/images/agents/none.jpg" alt=""></th>
                      <th class="history-block-table_cell"></th>
                      <th class="history-block-table_cell"></th>
                      <th class="history-block-table_cell"></th>
                      <th class="history-block-table_cell"></th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div> -->


          <?php } else { ?>
            <h2>Заказ не найдeн</h2>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>
