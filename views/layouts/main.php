<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;

use app\components\widgets\CitiesWidget;
use app\components\widgets\AlertWidget;

use yii\widgets\Pjax;
use app\assets\MainAsset;
use yii\bootstrap\Modal;


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="9b80433df273eb3d" />
    <meta name="yandex-verification" content="400ed55e76cf08ed" />
    <meta name="google-site-verification" content="qAmRKz0boVBzhTrRB25aL--q4XgjeNwCj_XHGlyYJeU" />
    <link rel="shortcut icon" href="/image/favicon.png" type="image/png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
<?php MainAsset::register($this) ?>
<?php $cookies = Yii::$app->request->cookies; ?>
<?php if ($_SESSION['city_modal'] === false) {

  if (!isset($cookies['city'])) {
    $script = <<< JS
      $(function() {
        jQuery(window).load(function() {
          document.getElementById('hamburger').click();
            document.getElementById('modal_choose-city').classList.add('choose-city_open').click();
        });
      });
JS;
    $this->registerJs($script, yii\web\View::POS_READY);
  }
  $_SESSION['city_modal'] = true;
}
?>

<?php MainAsset::register($this) ?>
<?php $this->registerJsFile('/js/all_pages.js', ['depends' => 'app\assets\IndexAsset']) ?>
<?php $this->registerJsFile('/js/tocart.js', ['depends' => 'app\assets\IndexAsset']) ?>
</head>
<body>
<?php $this->beginBody() ?>

<!--Google metrics-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-102419940-1', 'auto');
ga('send', 'pageview');

</script>
<!--End of Google metrics-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112951362-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112951362-1');
</script>

<!--Yandex metrics-->
<script type="text/javascript" >
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
      try {
        w.yaCounter45314085 = new Ya.Metrika({
          id:45314085,
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true
        });
      } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
      s = d.createElement("script"),
      f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
      d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45314085" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!--End of Yandex metrics-->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44742985 = new Ya.Metrika({
                    id:44742985,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44742985" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<header id="header" class="header">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-7 col-sm-12">
        <div class="header_flex-container">
          <a href='/' class="header_logo"></a>
          <p class="header_slogan">FOODBALL - доставка еды в поезда и домой</p>
        </div>
      </div>
      <div class="col-sm-4 text-center hidden-lg hidden-md">
        <a href="tel:+78002500091" class="header_tel"><i class="fa fa-mobile" aria-hidden="true"></i> 8-800-250-00-91</a>
      </div>

      <?php Pjax::begin(); ?>
      <div class="col-lg-3 col-md-2 col-sm-3 no-padding text-center">
          <a href="javascript://0" id="header_city" class="header_city"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $cookies['city'] ? $cookies['city']->value : Yii::$app->geoData->data['city']['name_ru'] ?></a>
          <br>
          <a href="tel:+78002500091" class="header_tel hidden-sm hidden-xs"><i class="fa fa-mobile" aria-hidden="true"></i> 8-800-250-00-91</a>
      </div>
      <?php Pjax::end(); ?>
        <?php if (Yii::$app->user->isGuest) { ?>

        <div class="col-lg-3 col-md-3 col-sm-5 text-center">
          <div class="header_status header_status-margin">
            <a href='/login' class="header_auth"><i class="fa fa-user" aria-hidden="true"></i> Вход</a>
            <a href='/signup' class="header_auth">Регистрация</a>
          </div>
        </div>

        <?php } else { ?>

          <div class="col-lg-3 col-md-3 col-sm-3 no-padding text-center">
            <a href="/account" class="history_icon">
              <i class="fa fa-user-secret" aria-hidden="true"></i> Личный кабинет <!--<span id="displayCount" class="basket_display-count"></span>-->
            </a>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-2 text-center">
            <div class="header_status">
              <?php echo Html::beginForm(['/logout'], 'post'); ?>
              <i class="fa fa-user" aria-hidden="true"></i>
              <?php echo Html::submitButton(
                  'Выход', ['class' => 'header_auth']
              ); ?>
              <?php echo Html::endForm(); ?>
            </div>
          </div>
      <?php }?>


</div>
</div>
<a href="javascript://0" id="hamburger" class="header_hamburger">
  <span class="header-hamburger_layer"></span>
  <span class="header-hamburger_layer"></span>
  <span class="header-hamburger_layer"></span>
</a>
<?php if (Yii::$app->controller->route == 'product/index') { ?>
  <a href="javascript://0" onclick="return getCart()" class="baskets_icon">
    <i class="fa fa-shopping-cart"></i> <span id="displayCount" class="basket_display-count"><?= $_SESSION['cart.count'] > 0 ? $_SESSION['cart.count'] : '' ?></span>
  </a>
<?php } ?>

</header>

<?= CitiesWidget::widget(['tpl' => 'cities']) ?>

<?php Pjax::begin(); ?>
    <?php if (!$cookies['city']) { ?>
        <div class="dropdown-city" id="dropdown-city">
            <p>Ваш город <b><i class="icon"></i><?= Yii::$app->geoData->data['city']['name_ru'] ?></b>?</p>
            <a class="btn btn-additional" href="javascript:" onclick="getCityName('<?= Yii::$app->geoData->data['city']['name_ru'] ?>');">да</a>
            <a href="javascript://0" id="change_city" class="header_city">нет</a>
        </div>
    <?php } ?>
<?php Pjax::end(); ?>

  <?= AlertWidget::widget() ?>
  <?= $content ?>

<section class="main-about">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-0">
        <a href="/about" class="main-about_link">
          <img src="/image/main-about(1).png" alt="" class="main-about_image">
          <h5 class="main-about_title">О нас</h5>
          <p class="main-about_text">
            Подробная информация
            о нашем проекте
          </p>
        </a>
      </div>

      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <a href="/press" class="main-about_link">
          <img src="/image/main-about(2).png" alt="" class="main-about_image">
          <h5 class="main-about_title">Для прессы</h5>
          <p class="main-about_text">
            Подробная информация
            о нашем проекте
          </p>
        </a>
      </div>

      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <a href="/signup-agent" class="main-about_link">
          <img src="/image/main-about(2).png" alt="" class="main-about_image">
          <h5 class="main-about_title">Для агентов</h5>
          <p class="main-about_text">
            Подробная информация
            о нашем проекте
          </p>
        </a>
      </div>

      <!-- <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <a href="#" class="main-about_link">
          <img src="/image/main-about(3).png" alt="" class="main-about_image">
          <h5 class="main-about_title">Призы</h5>
          <p class="main-about_text">
            Подробная информация
            о нашем проекте
          </p>
        </a>
      </div> -->

      <!-- <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
        <a href="#" class="main-about_link">
          <img src="/image/main-about(4).png" alt="" class="main-about_image">
          <h5 class="main-about_title">Блог</h5>
          <p class="main-about_text">
            Подробная информация
            о нашем проекте
          </p>
        </a>
      </div> -->

      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <p class="main-about_text main-about_text__last">
          <b>FOODBALL</b> – уникальный независимый сервис
          по доставке еды продуктов и товаров первой
          необходимости к поездам и дверям ваших домов.<br>
          FOODBALL - это хорошая привычка которую можно
          брать с собой.
        </p>
      </div>
    </div>
  </div>
</section>

<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-4">
        <h4 class="footer_title">Компания</h4>
        <ul class="footer_list">
          <li class="footer-list_element">
            <a href="/partnership" class="footer-list_link">Партнерство для ресторанов</a>
          </li>
          <li class="footer-list_element">
            <a href="/convention" class="footer-list_link">Пользовательское соглашение</a>
          </li>
          <li class="footer-list_element">
            <a href="/shareterms" class="footer-list_link">Условия проведения акций</a>
          </li>
          <li class="footer-list_element">
            <a href="/contacts" class="footer-list_link">Контакты</a>
          </li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-3 col-sm-4">
        <h4 class="footer_title">Полезные ссылки</h4>
        <ul class="footer_list">
          <li class="footer-list_element">
            <a href="#mobileApp" class="footer-list_link go_to">Мобильные приложения</a>
          </li>
          <li class="footer-list_element">
            <a href="#main-order" class="footer-list_link go_to">Доставка пиццы</a>
          </li>
          <li class="footer-list_element">
            <a href="#main-order" class="footer-list_link go_to">Доставка суши</a>
          </li>
          <li class="footer-list_element">
            <a href="#main-order" class="footer-list_link go_to">Доставка еды</a>
          </li>
        </ul>
      </div>

      <div class="col-lg-5 col-md-4 col-sm-4 col-lg-offset-1 col-md-offset-2 col-sm-offset-0">
        <h4 class="footer_title">Полезные ссылки</h4>

        <ul class="footer_soc-list">
          <li class="footer-soc-list_element">
            <a href="http://vk.com/foodballrus" class="footer-soc-list_link footer-soc-list_element__vk" rel="external"><i class="fa fa-vk" aria-hidden="true"></i></a>
          </li>
          <li class="footer-soc-list_element">
            <a href="https://www.facebook.com/foodballrus" class="footer-soc-list_link footer-soc-list_element__facebook" rel="external"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          </li>
          <li class="footer-soc-list_element">
            <a href="https://twitter.com/FoodBallrus" class="footer-soc-list_link footer-soc-list_element__twitter" rel="external"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          </li>
          <li class="footer-soc-list_element">
            <a href="http://instagram.com/foodballrus" class="footer-soc-list_link footer-soc-list_element__instagramm" rel="external"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          </li>
          <li class="footer-soc-list_element">
            <a href="https://www.ok.ru/group/58498719285311" class="footer-soc-list_link footer-soc-list_element__google" rel="external"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
          </li>
          <li class="footer-soc-list_element">
            <a href="https://t.me/foodballrus" class="footer-soc-list_link footer-soc-list_element__twitter" rel="external"><i class="fa fa-telegram" aria-hidden="true"></i></a>
          </li>
          <!-- <li class="footer-soc-list_element">
            <a href="#" class="footer-soc-list_link footer-soc-list_element__google"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
          </li> -->
        </ul>

        <p class="footer_text">
          © 2017 FOOD BALL <br>
          Все права защищены
        </p>
		<a href="/politic" class="poltic footer_politic">Подробнее о политике конфиденциальности</a>
      </div>
    </div>
  </div>
    <script type="text/javascript">
    (window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-141902-buOBV';
  </script>
</footer>

<?php if (!Yii::$app->user->isGuest) {

Modal::begin([
  'id'     => 'history',
  'size'   => 'modal-lg',
]);

Modal::end();

} ?>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
