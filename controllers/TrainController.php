<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

use yii\filters\VerbFilter;

use app\models\User;
use app\models\Api;
use app\models\ExpressApi;
use app\models\TrainForm;
use app\models\StationForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class TrainController extends BaseController
{

    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'rules' => [
    //                 [
    //                     'actions' => ['index', 'suggest', 'form', 'info'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //     ];
    // }


    /**
    * Displays to home page.
    *
    * @return string
    */
    public function actionIndex()
    {
        $train_num = Yii::$app->request->get('train_num');
        $date = Yii::$app->request->get('date');
        $from = Yii::$app->request->get('from');
        $where = Yii::$app->request->get('where');
        $session = Yii::$app->session;
        $session->open();

        $stationform = new StationForm;

        if (isset($_SESSION['wherefrom']) && isset($_SESSION['wherein']) && isset($_SESSION['datetrain'])) {
            $stationform->wherefrom = $_SESSION['wherefrom'];
            $stationform->wherein = $_SESSION['wherein'];
            $stationform->date = $_SESSION['datetrain'];
        }

        if(isset($train_num) && isset($date) && isset($from) && isset($where)) {
            $trainform = new TrainForm;
            $trainform->from = $from;
            $trainform->where = $where;
            $trainform->train_num = $train_num;
            $trainform->date = $date;

            return $this->render('index', compact('trainform'));
        }

        return $this->render('index', compact('stationform'));
    }

    /**
    * Display list stations
    *
    * @return JSON array
    */
    public function actionSuggest($name=null)
    {
        if (!Yii::$app->request->isAjax || !isset($name)) {
            return $this->goHome();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = ExpressApi::Suggest($name);

        $items = [];

        foreach ($response->Station as $item) {
            $items['items'][] = ['id' => $item->attributes()->Code, 'text' => $item->NameFull];
        }

        return $items;
    }

    /**
    * Makes form info about train, stations
    *
    * @return redirect
    */
    public function actionForm()
    {
        if(!Yii::$app->request->post()) {
            return $this->goBack();
        }

        $post = Yii::$app->request->post();
        $session = Yii::$app->session;
        $session->open();
        $_SESSION['station'] = $post['station'];
        $_SESSION['train_num'] = $post['train_num'];
        $_SESSION['wagon_num'] = $post['wagon_num'];
        $_SESSION['stop'] = $post['waiting_time'];
        $_SESSION['data_train']  = Yii::$app->formatter->format($post['date'], 'timestamp') - 3 * 3600;
        $_SESSION['time'] = Yii::$app->formatter->asDate($_SESSION['data_train'], 'HH:mm');
        $_SESSION['order_city'] = $post['city'];
        $_SESSION['order_city_id'] = $post['city_id'];

        return $this->redirect(Url::toRoute(['/go-by-train'], 'https'));
    }

    /**
    * Info about stations
    *
    * @return array
    */
    public function actionInfo($code=null)
    {
        if (Yii::$app->request->isAjax) {
            if(isset($code)) {
                $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
                $items = Api::stations_list($token, $code , true, null, 1)->items;
                if (!empty($items[0]->city->available)) {
                    return json_encode($items[0]->city);
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }
        return $this->goHome();
    }

    /**
     *
     */
    public function actionList()
    {
        $wherefrom = Yii::$app->request->get('wherefrom');
        $wherein = Yii::$app->request->get('wherein');
        $date = Yii::$app->request->get('date');

        if(isset($wherefrom) && isset($wherein) && isset($date)) {
            $_SESSION['wherefrom'] = $wherefrom;
            $_SESSION['wherein'] = $wherein;
            $_SESSION['datetrain'] = $date;
            return $this->redirect(Url::to(['/train'], 'https'));
        }
    }

}
