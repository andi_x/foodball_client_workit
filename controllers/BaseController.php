<?php

namespace  app\controllers;
use Yii;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Api;
use app\models\UrlFilter;
use yii\helpers\Url;
use app\components\helpers\StringHelper;

/**
 *
 */
class BaseController extends Controller
{
    // /**
    //  * @inheritdoc
    //  */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => [
    //
    //             ],
    //             'rules' => [
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //             ],
    //         ],
    //     ];
    // }

    public function beforeAction($action)
    {
        Yii::$app->session->open();

        if (Yii::$app->user->isGuest) {
            if (!$_SESSION['token']) {
                $_SESSION['token'] = Api::get_token();
            }
        }

        $controllers_actions = [
            'site/index' => true,
            'preorder/index' => true,
            'product/index' => true,
        ];

        $cookies = Yii::$app->request->cookies;
        $urlFilter = new UrlFilter();

        if (isset($cookies['city']->value)) {
            $urlFilter->str = StringHelper::translit($cookies['city']->value);
        } else {
            $urlFilter->str = StringHelper::translit(Yii::$app->geoData->data['city']['name_ru']);
        }

        // return var_dump(Yii::$app->controller->action->id);

        if (!Yii::$app->request->isAjax) {

            if (isset($controllers_actions[Yii::$app->controller->id . '/' . Yii::$app->controller->action->id])) {
                $url = $urlFilter->edit();

                if ($url !== false) {
                    return $this->redirect($url);
                }
            }
        }

        return parent::beforeAction($action);
    }

    public function afterAction($action, $result)
    {
        Yii::$app->getUser()->setReturnUrl(Yii::$app->request->url);
        return parent::afterAction($action, $result);
    } 
}
