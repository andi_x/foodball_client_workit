<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use app\models\Api;

class HistoryController extends BaseController
{
    public function behaviors()
    {
        return [
          'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                      'actions' => ['show', 'view'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
              ],
          ],
      ];
    }

    public function actionShow()
    {
        $this->layout = false;
        $page =  Yii::$app->request->get('page');
        $token = Yii::$app->user->identity->accessToken;
        $pageCount = Api::orders_list($token)->_meta->pageCount;
        $items = [];

        for ($i = 0; $i <= $pageCount; $i++) {
            $orders_list = Api::orders_list($token, $i);
            $orders_array = ArrayHelper::toArray($orders_list->items);

            foreach ($orders_array as $item) {
                $items[$item['id']] = ['id' => $item['id'], 'name' => $item['name'], 'delivery_at' => $item['delivery_at']
                , 'delivery_address' => $item['delivery_address'], 'phone' => $item['phone'], 'status' => $item['order_agents'][0]['status']];
            }
        }

        return $this->render('index', compact('items'));
    }

    public function actionView()
    {
        $id  = Yii::$app->request->get('id');
        $token = Yii::$app->user->identity->accessToken;
        $order_info = Api::order_info($token, $id);
        $agent_info = Api::agent_info($token, $order_info->order_agents[0]->agent_id);
        return $this->render('view', compact('order_info', 'agent_info'));
    }
}
