<?php

namespace app\controllers;

use app\models\LogoutForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use app\models\ConfirmForm;
use app\models\LoginForm;
use app\models\MailForm;
use app\models\PasswordForm;
use app\models\ProfileForm;
use app\models\PartnershipForm;
use app\models\ResetForm;
use app\models\SignupForm;
use app\models\StewardStatisticForm;
use app\models\Api;
use app\models\User;
use app\models\Session;
use app\models\StationForm;
use app\models\City;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'account', 'check-message'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Session::clear();
        $stationform = new StationForm;
        $city_id = City::getId(Session::getToken());

        return $this->render('index', compact('stationform', 'city_id'));
    }

    /**
     * Signup action.
     *
     * @return string
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm(['scenario' => SignupForm::SCENARIO_USER]);
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->signup();
            if ($res === 'Подтвердите регистрацию') {
                $_SESSION['confirm'] = $model->phone;
                Yii::$app->session->setFlash('success', $res);
                return $this->redirect(Url::to(['confirm'], 'https'));
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->goBack();
            }
        }
        return $this->render('signup', compact('model'));
    }

    /**
     * Signup agent action.
     *
     * @return string
     */
    public function actionSignupAgent()
    {
        if (User::is_steward(Yii::$app->user->identity->accessToken)) {
            return $this->goBack();
        }

        $model = new SignupForm(['scenario' => SignupForm::SCENARIO_STEWARD]);
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->signup();

            if ($res === 'Подтвердите регистрацию') {
                $_SESSION['confirm'] = $model->phone;
                Yii::$app->session->setFlash('success', $res);
                return $this->redirect(Url::to(['confirm'], 'https'));
            } elseif ($res == 'Поздравляем!') {
                Yii::$app->session->setFlash('success', $res);
                return $this->goBack();
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->goBack();
            }
        }
        return $this->render('signup_agent', compact('model'));
    }

    /**
    * Confirm action
    *
    * @return string
    */
    public function actionConfirm()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ConfirmForm();
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->confirm();
            if ($res == 'Вы зарегистрированы') {
                unset($_SESSION['confirm']);
                Yii::$app->session->setFlash('success', $res);
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->goBack();
            }
        }
        return $this->render('confirm', compact('model'));
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->login();
            if ($res == 1) {
                $_SESSION['check'] = true;
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->refresh();
            }
        }
        return $this->render('login', ['model' => $model]);
    }

    /**
     * Reset action.
     *
     * @return string
     */
    public function actionReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ResetForm();
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->reset();
            if ($res == 'Подтвердите сброс пароля') {
                $_SESSION['password'] = $model->phone;
                Yii::$app->session->setFlash('success', $res);
                return $this->redirect(['password']);
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->refresh();
            }
        }
        return $this->render('reset', ['model' => $model]);
    }

    /**
     * Password action
     *
     * @return string
     */
    public function actionPassword()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            $res = $model->confirm();
            if ($res == 'Новый пароль сохранен') {
                unset($_SESSION['confirm']);
                Yii::$app->session->setFlash('success', $res);
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->refresh();
            }
        }
        return $this->render('password', compact('model'));
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $model = new LogoutForm();
        if (Yii::$app->request->post() != null) {
            $request = Yii::$app->request->post();
            if ($model->logout(Yii::$app->user->identity->accessToken)) {
                Yii::$app->user->logout();
            }
        }

        return $this->goHome();
    }

    /**
     * Change user's city
     */
    public function actionCity()
    {
        $id = Yii::$app->request->get('id');
        $name = mb_strtoupper(Yii::$app->request->get('name'));
        $token = Yii::$app->user->identity->accessToken ? Yii::$app->user->identity->accessToken : $_SESSION['token'];

        if (!isset($id)) {
            if (!isset($name)) {
                return $this->goHome();
            }
            $city = Api::cities_list($token,null,$name)->items[0];
        } else {
            $city = Api::cities_info($token, $id);
        }

        if (!empty($city->name)) {
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'city',
                'value' => $city->name,
            ]));
            $cookies->add(new \yii\web\Cookie([
                'name' => 'city_id',
                'value' => $city->id,
            ]));
        }

        $data = [];
        $data['city_id'] = $city->id;
        Session::clear();
        $check = Api::agents_list($token, $data);
        if (empty($check->items)) {
            $_SESSION['check'] = false;
        } else {
            $_SESSION['check'] = true;
        }
        return $this->goBack();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays for press page.
     *
     * @return string
     */
    public function actionPress()
    {
        return $this->render('for_press');
    }
    /**
     * Displays politic page.
     *
     * @return string
     */
    public function actionPolitic()
    {
        return $this->render('politic_konf');
    }

    /**
     * Displays restaurants partnership page.
     *
     * @return string
     */
    public function actionPartnership()
    {
        $model = new PartnershipForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['postClientEmail'])) {
            Yii::$app->session->setFlash('success', 'Письмо отправлено');
            return $this->refresh();
        }
        return $this->render('restaurants_partnership', compact('model'));
    }

    /**
     * Displays contacts page.
     *
     * @return string
     */
    public function actionContacts()
    {
        return $this->render('contacts');
    }

    /**
     * Displays convention page.
     *
     * @return string
     */
    public function actionConvention()
    {
        return $this->render('convention');
    }

    /**
     * Displays share terms page.
     *
     * @return string
     */
    public function actionShareterms()
    {
        return $this->render('shareterms');
    }

    /**
     * Displays agent partnership page.
     *
     * @return string
     */
    public function actionPartnershipAgent()
    {
        return $this->render('partnership_agent');
    }

    /**
     * Displays user page.
     *
     * @return string
     */
    public function actionAccount()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ContactForm();
        $token = Yii::$app->user->identity->accessToken;
        $user = User::findIdentityByAccessToken($token);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact($user->email)) {
                Yii::$app->session->setFlash('contactFormSubmitted');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('contactFormError');
                return $this->refresh();
            }
        }

        $profile = new ProfileForm(['scenario' => isset($user->promo) ? ProfileForm::SCENARIO_STEWARD : ProfileForm::SCENARIO_USER]);
        $profile->email = $user->email;
        $profile->name = $user->username;
        $profile->birthday = $user->birthday;
        $profile->gender = $user->gender;

        if ($profile->load(Yii::$app->request->post())) {
            $response = $profile->update($token);

            if ($response) {
                $user->username = $response->name;
                $user->email = $response->email;
                $user->birthday = $response->birthday;
                $user->gender = $response->gender;
                $user->save();
                Yii::$app->session->setFlash('profileFormSubmitted');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('profileFormError');
                return $this->refresh();
            }
        }

        $promo = User::is_steward($token);

        if ($promo) {
            $order_from = Yii::$app->request->get('order_from');
            $order_to = Yii::$app->request->get('order_to');
            $statistic = new StewardStatisticForm();
            $messages = Api::steward_message($token)->items;
            $res = Api::steward_statistic($token, $order_from, $order_to)->items[0];
            $statistic->balance = isset($res->balance) ? $res->balance : 0;
            $statistic->total_orders = $res->total_orders;
            $statistic->complete_orders = $res->complete_orders;
            return $this->render('steward_account', ['model' => $model,
                'statistic' => $statistic, 'promo' => $promo, 'messages' => $messages, 'profile' => $profile]);
        }

        return $this->render('account', ['model' => $model, 'profile' => $profile]);
    }

    public function actionCheckMessage()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $id = Yii::$app->request->get('id');
        $token = Yii::$app->user->identity->accessToken;
        return Api::steward_check_message($token, $id)->status;
    }

}
