<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use yii\web\Response;

use app\models\User;
use app\models\Api;
use app\models\AgentsFilter;
use app\models\Cart;
use app\models\City;
use app\models\Session;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\components\helpers\StringHelper;

class PreorderController extends BaseController
{

  // public function behaviors()
  // {
  //     return [
  //         'access' => [
  //             'class' => AccessControl::className(),
  //             'rules' => [
  //                 [
  //                     'actions' => ['home', 'suggest', 'filters-clear'],
  //                     'allow' => true,
  //                     'roles' => ['@'],
  //                 ],
  //             ],
  //         ],
  //     ];
  // }


    /**
    * Displays preoder page.
    *
    * @return string
    */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $session->open();
        $cookies = Yii::$app->request->cookies;
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $gettypes = Yii::$app->request->get('types');
        $getcategories = Yii::$app->request->get('categories');
        $getminprice = Yii::$app->request->get('minprice');
        $getmaxprice = Yii::$app->request->get('maxprice');
        $getname = Yii::$app->request->get('name');
        $where = Yii::$app->request->get('where');
        $filters = new AgentsFilter();
        $filters->min = 0;
        $filters->max = 9999;
        $_SESSION['fiat'] = true;

        if (Yii::$app->request->isAjax) {
            $gettypes ? $_SESSION['filter_types'] = $gettypes : $_SESSION['filter_types'] = '';
            $getcategories ? $_SESSION['filter_categories'] = $getcategories : $_SESSION['filter_categories'] = '';
            $getminprice ? $_SESSION['filter_minprice'] = $getminprice : $_SESSION['filter_minprice'] = '';
            $getmaxprice ? $_SESSION['filter_maxprice'] = $getmaxprice : $_SESSION['filter_maxprice'] = '';
            $getname ? $_SESSION['filter_name'] = $getname : $_SESSION['filter_name'] = '';
            return $this->refresh();
        }

        if (isset($_SESSION['filter_types'])) {
            $ftype = explode(',', $_SESSION['filter_types']);
            foreach ($ftype as $type) {
                $filters->types[] = $type;
            }
        }

        if (isset($_SESSION['filter_categories'])) {
            $fcat = explode(',', $_SESSION['filter_categories']);
            foreach ($fcat as $cat) {
                $filters->categories[] = $cat;
            }
        }

        $_SESSION['filter_minprice'] ? $filters->min = $_SESSION['filter_minprice'] : '';
        $_SESSION['filter_maxprice'] ? $filters->max = $_SESSION['filter_maxprice'] : '';
        $_SESSION['filter_name'] ? $filters->name = $_SESSION['filter_name'] : '';

        $city = City::get($token);

        if (!isset($city)) {
            Yii::$app->session->setFlash('error', 'Упс! Сервис FOOD BALL, к сожалению, еще не работает в этом городе');
           $_SESSION['city_modal'] = false;
           return $this->goHome();
        }

        //Список категорий продуктов
        $products_categories = Api::product_categories_list($token);
        $products_categories = $products_categories->items;

        if (is_array($products_categories)) {
            foreach ($products_categories as $product) {
                $product_filter[$product->id] = $product->name;
            }
        }

        //Список категорий агентов
        $agent_types_list = Api::agent_types_list($token);
        $agent_types_list = $agent_types_list->items;

        if (is_array($agent_types_list)) {
            foreach ($agent_types_list as $category) {
                if ($category->id == 6) continue;
                $agent_filter[$category->id] = $category->name;
            }
        }

        //Смещение по часовому поясу от Москвы
        $timezone = $city->timezone;
        // Получение времени заказа
        if (!Session::OrderTime($where, $timezone)) {
            return $this->goHome();
        }
        Cart::clear();

        return $this->render('index', compact('agent_types_list', 'product_filter', 'timezone', 'agent_filter', 'filters'));
    }

    /**
    * Preorder home
    * @return redirect
    * isAjax
    * @return boolean
    */
    public function actionPreord()
    {
        $session = Yii::$app->session;
        $session->open();
        $_SESSION['preOrd'] = 1;
        unset($_SESSION['preorder_modal']);

        if (Yii::$app->request->isAjax) {
            return true;
        }

        return $this->redirect(Url::to(['/home'], 'https'));
    }

    public function actionCancelPreord()
    {
        $session = Yii::$app->session;
        $session->open();
        unset($_SESSION['preorder_modal']);

        return true;
    }

    /**
    * Display list restoraunts
    *
    * @return JSON array
    */
    public function actionSuggest($name=null)
    {
        if (!Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        if(!isset($name)) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $cookies = Yii::$app->request->cookies;
        $name = mb_strtolower($name, 'UTF-8');
        $name_tr = StringHelper::translit($name);

        if ($name === $name_tr) {
            $name = StringHelper::untranslit($name);
        }

        $items = [];

        $data['name'] = str_replace('-', ' ', $name)
            . ',' . $name
            . ',' . $name_tr;
        $data['city_id'] = isset($_SESSION['order_city_id']) ? $_SESSION['order_city_id'] : $cookies['city_id']->value;
        $res = Api::agents_list($token, $data);

        if (!empty($res->items)) {
            foreach ($res->items as $item) {
                $items['items'][] = ['id' => $item->name, 'text' => $item->name];
            }
        }

        if (empty($items)) {
            return false;
        }

        return $items;
    }

    public function actionFiltersClear()
    {
        $session = Yii::$app->session;
        $session->open();
        $_SESSION['filter_name'] ? $_SESSION['filter_name'] = '' : '';

        return true;
    }



}
