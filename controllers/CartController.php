<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Cart;
use app\models\Order;
use app\models\Api;
use app\models\User;
use app\models\Session;
use app\models\ConfirmForm;
use app\components\helpers\AgentHelper;
use yii\helpers\Url;

class CartController extends BaseController
{

    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'rules' => [
    //                 [
    //                     'actions' => ['promo'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //     ];
    // }

    public function actionShow()
    {
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $session = Yii::$app->session;
        $session->open();
        $points = 0;
        $agent_info = Api::agent_info($token, $session->get('agent_id'));
        $minprice = AgentHelper::Price($agent_info->min_order_price);
        $delivery_price = $agent_info->delivery_price;

        if (!Yii::$app->user->isGuest) {
            $points = User::getPoints(Yii::$app->user->identity->accessToken);
        }

        $this->layout = false;
        return $this->render('index', compact('session', 'points', 'minprice', 'delivery_price'));
    }

    public function actionView()
    {
        $session = Yii::$app->session;
        $session->open();
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $cookies = Yii::$app->request->cookies;
        $points = 0;
        $is_call = 0;
        $pay_types = ['1' => 'Наличный расчет'];

        if ($_SESSION['preOrd'] === 1) {
            $order = new Order(['scenario' => Order::SCENARIO_PREORDER]);
        } else {
            $order = new Order(['scenario' => Order::SCENARIO_ORDER]);
        }

        if (!empty($session['agent_id']) && !empty($session['cart'])) {
            true;
        } else {
            Yii::$app->session->setFlash('error', 'Корзина пуста');
            return $this->goHome();
        }

        /*
        * Автозаполнение полей для авторизированных пользователей
         */
        if (!Yii::$app->user->isGuest) {
            $user = User::findIdentityByAccessToken($token);
            $points = $user->points;
            $is_call = $user->is_call;
            $name = $user->username;
            $phone = $user->phone;
        }


        $agent_info = Api::agent_info($token, $session['agent_id']);

        if (!empty($agent_info->agent_pay_types)) {
            foreach ($agent_info->agent_pay_types as $pay_type) {
                if ($pay_type->id > 3)
                    break;
                $pay_types[$pay_type->id] = $pay_type->name;
            }
        }

        $minprice = AgentHelper::Price($agent_info->min_order_price);
        $agent_info->delivery_price != 0 ? $delivery_fee = $agent_info->delivery_price : 0;

        if ($session['cart.sum'] < $minprice) {
            Yii::$app->session->setFlash('error', 'Минимальная сумма заказа ' . $minprice . ' Руб.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        if ($order->load(Yii::$app->request->post())) {

            if ($order->payment_type !== '3') {
                $order->transaction_id = null;
            }

            $order_items =[];
            foreach ($session['cart'] as $id => $product) {
                $order_items[] = ['product_id' => $id, 'quantity' => (int)$product['count']];
            }
            $order->order_items = $order_items;

            if ($order->validate()) {
                $result = Api::orders_create($token, $order);
                if (isset($result->id)) {
                    if (!Yii::$app->user->isGuest) {
                        $user->points -= $result->points;
                        $user->update();
                    };
                    Session::clear();
                    $_SESSION['trans_id'] = null;

                    if (!Yii::$app->user->isGuest) {
                        Yii::$app->session->setFlash('success', 'Ваш заказ принят');
                        return $this->goHome();
                    } else {
                        $_SESSION['confirm_order'] = $order->phone;
                        return $this->redirect(Url::to(['cart/confirm-order'], 'https'));
                    }
                } else {
                    foreach ($result as $mess) {
                        $error .= $mess->message;
                    }
                }
            }

            $errors = $order->getErrors();

            foreach ($errors as $title => $messages) {
                foreach ($messages as $message) {
                    $error .= isset($message) ? $message . '</br>' : '';
                }
            }

            if ( count($errors) === 1 && isset($errors["Pay error"]) ) {
                $_SESSION['CloudPay'] = false;
            } else {
                Yii::$app->session->setFlash('error', $error);
            }

        }

        $amount = $is_call ? $session['cart.sum'] + $delivery_fee + 30 : $session['cart.sum'] + $delivery_fee;
        return $this->render('view', compact('session', 'order', 'name', 'phone', 'points', 'amount', 'pay_types'));
    }


    public function actionAdd()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        $this->layout = false;

        if (!Yii::$app->user->isGuest) {
            $points = User::getPoints(Yii::$app->user->identity->accessToken);
        }

        $id = Yii::$app->request->get('id');
        $count = (int)Yii::$app->request->get('count');
        $count = !$count ? 1 : (int)$count;
        $count = (int)('+'.abs($count));

        if ($count > 50) {
            $count = 50;
        }

        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $to_train = $_SESSION['train'] ? true : false;

        $product = Api::product_info($token, $id);

        if (Yii::$app->user->isGuest && $product->for_points) return "Ошибка авторизации";
        if (empty($product)) return "Товар не найден";
        if ($product->for_points && ($product->price > $points)) return "Не хватает баллов";
        $session = Yii::$app->session;
        $session->open();

        $cart = new Cart();
        $response = $cart->addToCart($product, $count);

        if (($product->to_train !== $to_train && $product->to_house !== true) || $product->agent_id != $_SESSION['agent_id']) {
            Cart::clear();
        }

        if (!empty($response['errors'])) {
            return $response['errors'];
        }

        return isset($_SESSION['cart.count']) ? $_SESSION['cart.count'] : 0;
    }


    public function actionClear()
    {
        if (!Yii::$app->user->isGuest) {
            $points = User::getPoints(Yii::$app->user->identity->accessToken);
        }

        Cart::clear();

        $this->layout = false;
        return $this->render('index', compact('session', 'points'));
    }


    public function actionDelItem()
    {
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $id = Yii::$app->request->get('id');

        if (!Yii::$app->user->isGuest) {
            $points = User::getPoints(Yii::$app->user->identity->accessToken);
        }

        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        $agent_info = Api::agent_info($token, $session['agent_id']);
        $minprice = AgentHelper::Price($agent_info->min_order_price);
        $delivery_price = $agent_info->delivery_price;
        $this->layout = false;
        return $this->render('index', compact('session', 'minprice', 'delivery_price', 'points'));
    }


    public function actionPromo()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goBack();
        }

        $code = Yii::$app->request->get('code');
        $form = Yii::$app->request->get('form');
        $session = Yii::$app->session;
        $session->open();
        $token = Yii::$app->user->identity->accessToken;
        $res = Api::user_promo($token, $code);
        $user = User::findIdentityByAccessToken($token);
        $user->points += $res;
        $agent_info = Api::agent_info($token, $session['agent_id']);
        $minprice = AgentHelper::Price($agent_info->min_order_price);
        $delivery_price = $agent_info->delivery_price;

        if(is_numeric($res)) {
            $user->save();
            $points = $user->points;
            $res = 'Добавлено ' . $res . ' баллов';
            if ($form == 2) {
                Yii::$app->session->setFlash('success', $res);
            }
        } else {
            switch ($res) {
                case 'Promotional code is not active':
                    $res = 'Промокод не активен';
                    break;
                case 'Promotional code already activated':
                    $res = 'Промокод уже активирован';
                    break;
                default:
                    break;
            }
            if ($form == 2) {
                Yii::$app->session->setFlash('error', $res);
            }
            $points = $user->points;
        }

        if ($form == 2) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        $this->layout = false;
        return $this->render('index', compact('session', 'points', 'res', 'minprice', 'delivery_price'));
    }


    public function actionConfirmOrder()
    {
        $model = new ConfirmForm();
        $session = Yii::$app->session;
        $session->open();

        if ($model->load(Yii::$app->request->post())) {
            $res = $model->confirmOrder();
            if ($res == 'Ваш заказ подтвержден') {
                $session->remove('confirm_modal');
                $session->remove('confirm_order');
                Yii::$app->session->setFlash('success', 'Ваш заказ принят');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', $res);
                return $this->refresh();
            }
        }
        return $this->render('confirm', compact('model', 'session'));
    }

    public function actionClearOrder()
    {
        $session = Yii::$app->session;
        $session->open();
        Session::clear();
        $session->remove('confirm_modal');
        $session->remove('confirm_order');
        return $this->goHome();
    }

}
