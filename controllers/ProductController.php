<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\components\data\Pagination;
use app\components\helpers\TimeHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use app\models\User;
use app\models\Api;
use app\models\Cart;
use app\models\City;
use app\models\Session;
use app\models\AgentsFilter;

use \yii\web\HttpException;

class ProductController extends BaseController
{

    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'rules' => [
    //                 [
    //                     'actions' => ['index'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //     ];
    // }


    /**
    * Displays products list page.
    *
    * @return string
    */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $session->open();
        $cookies = Yii::$app->request->cookies;
        $token = Session::getToken();
        $url = Yii::$app->request->get('name');
        $page = Yii::$app->request->get('page');
        $category = Yii::$app->request->get('category');
        $where = Yii::$app->request->get('where');
        $city = City::get($token);
        $for_points = null;

        if (!isset($city)) {
            Yii::$app->session->setFlash('error', 'Упс! Сервис FOOD BALL, к сожалению, еще не работает в этом городе');
            $session['city_modal'] = false;
            return $this->goHome();
        }

        //Смещение по часовому поясу от Москвы
        $timezone = $city->timezone;
        $time = Yii::$app->formatter->format('now', 'timestamp') + ($timezone * 3600);
        $now = Yii::$app->formatter->asDate($time, 'H:m:s');
        // Получение времени заказа
        if (!Session::OrderTime($where, $timezone)) {
            return $this->goHome();
        }

        $agent = AgentsFilter::findByUrl($token, $url, $city->id);

        if (!isset($agent) || empty($agent)) {
            return $this->redirect($session['train'] ? '/train' : '/home');
        }

        if ($session['agent_id'] !== $agent->id) {
            Cart::clear();
            $session->set('agent_id', $agent->id);
        }

        switch ($session['train']) {
            case false:
                $way = '&to_house=1';
                break;
            case true:
                $way = '&to_train=1';
                break;
            default:
                $way = '&to_house=1';
        }

        $agent_info = Api::agent_info($token, $session['agent_id']);

        if (!TimeHelper::validateAgent($agent_info, $timezone, $now)) {
            $session->set('preorder_modal', $url);
            return $this->redirect($session['train'] ? Url::to(['/train'], 'https') : Url::to(['/home'], 'https'));
        }

        if (Yii::$app->user->isGuest) {
            $for_points = false;
        }

        //Список продуктов
        $products_json = Api::products_list($token, $session['agent_id'], $page, $way, $category, $for_points);
        $products_list = $products_json->items;

        //Список категорий продуктов
        $products_categories_json = Api::product_categories_list($token, $session['agent_id']);

        foreach ($products_categories_json as $id => $item) {
            $products_categories[$item->id] = ['name' => $item->name];
        }

        $pagination = new Pagination([
            'defaultPageSize' => 20,
            'totalCount' => $products_json->_meta->totalCount,
            'ignoreParams' => ['cities'],
        ]);

        return $this->render('index', compact('products_list', 'pagination', 'products_categories', 'products_json', 'agent_info'));
    }

    public function actionForm($id)
    {
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $product = Api::product_info($token, $id);

        $this->layout = false;
        return $this->render('form', compact('product'));
    }


}
