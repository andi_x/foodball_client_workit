<?php
namespace  app\components\data;

use Yii;
use yii\web\Request;
use yii\data\Pagination as MainPagination;

/**
 *
 */
class Pagination extends  MainPagination
{

    /**
     * @var array ignore params for create url
     */
    public $ignoreParams = [];

    public function createUrl($page, $pageSize = null, $absolute = false)
    {
        $page = (int) $page;
        $pageSize = (int) $pageSize;
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        if ($page > 0 || $page == 0 && $this->forcePageParam) {
            $params[$this->pageParam] = $page + 1;
        } else {
            unset($params[$this->pageParam]);
        }
        if ($pageSize <= 0) {
            $pageSize = $this->getPageSize();
        }
        if ($pageSize != $this->defaultPageSize) {
            $params[$this->pageSizeParam] = $pageSize;
        } else {
            unset($params[$this->pageSizeParam]);
        }
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        }

        if (!empty($this->ignoreParams)) {
            foreach ($this->ignoreParams as $param) {
                unset($params[$param]);
            }
        }
        
        return $urlManager->createUrl($params);
    }

}
