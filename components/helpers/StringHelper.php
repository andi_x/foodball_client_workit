<?php

namespace app\components\helpers;
use yii\helpers\Html;

class StringHelper extends \yii\helpers\StringHelper
{

    public static function InColumn($string, $length)
    {
        $result = wordwrap($string, $length, "<br />\n");

        return $result;
    }

    public static function untranslit($str)
    {
        // ГОСТ 7.79B
        $transliteration = array(
            'A' => 'А', 'a' => 'а',
            'B' => 'Б', 'b' => 'б',
            'V' => 'В', 'v' => 'в',
            'G' => 'Г', 'g' => 'г',
            'D' => 'Д', 'd' => 'д',
            'E' => 'Е', 'e' => 'е',
            'Yo' => 'Ё', 'yo' => 'ё',
            'Zh' => 'Ж', 'zh' => 'ж',
            'Z' => 'З', 'z' => 'з',
            'I' => 'И', 'i' => 'и',
            'J' => 'Й', 'j' => 'й',
            'K' => 'К', 'k' => 'к',
            'L' => 'Л', 'l' => 'л',
            'M' => 'М', 'm' => 'м',
            'N' => 'Н', 'n' => 'н',
            'O' => 'О', 'o' => 'о',
            'P' => 'П', 'p' => 'п',
            'R' => 'Р', 'r' => 'р',
            'S' => 'С', 's' => 'с',
            'T' => 'Т', 't' => 'т',
            'U' => 'У', 'u' => 'у',
            'F' => 'Ф', 'f' => 'ф',
            'H' => 'Х', 'h' => 'х',
            'Cz' => 'Ц', 'cz' => 'ц',
            'Ch' => 'Ч', 'ch' => 'ч',
            'Sh' => 'Ш', 'sh' => 'ш',
            'Shh' => 'Щ','shh' => 'щ',
            'Y' => 'Ы', 'y' => 'ы',
            'E`' => 'Э', 'e`' => 'э',
            'Yu' => 'Ю', 'yu' => 'ю',
            'Ya' => 'Я', 'ya' => 'я',
        );

        $str = strtr($str, $transliteration);
        $str = preg_replace('|([-]+)|s', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

    public static function translit($str)
    {
        $str = str_replace(' ', '-', $str);
        // ГОСТ 7.79B
        $transliteration = array(
            'А' => 'A',   'а'  =>  'a' ,
            'Б' => 'B',   'б'  =>  'b' ,
            'В' => 'V',   'в'  =>  'v' ,
            'Г' => 'G',   'г'  =>  'g' ,
            'Д' => 'D',   'д'  =>  'd' ,
            'Е' => 'E',   'е'  =>  'e' ,
            'Ё' => 'Yo',  'ё'  =>  'yo',
            'Ж' => 'Zh',  'ж'  =>  'zh',
            'З' => 'Z',   'з'  =>  'z' ,
            'И' => 'I',   'и'  =>  'i' ,
            'Й' => 'J',   'й'  =>  'j' ,
            'К' => 'K',   'к'  =>  'k' ,
            'Л' => 'L',   'л'  =>  'l' ,
            'М' => 'M',   'м'  =>  'm' ,
            'Н' => 'N',   'н'  =>  'n' ,
            'О' => 'O',   'о'  =>  'o' ,
            'П' => 'P',   'п'  =>  'p' ,
            'Р' => 'R',   'р'  =>  'r' ,
            'С' => 'S',   'с'  =>  's' ,
            'Т' => 'T',   'т'  =>  't' ,
            'У' => 'U',   'у'  =>  'u' ,
            'Ф' => 'F',   'ф'  =>  'f' ,
            'Х' => 'H',   'х'  =>  'h' ,
            'Ц' => 'Cz',  'ц'  =>  'cz',
            'Ч' => 'Ch',  'ч'  =>  'ch',
            'Ш' => 'Sh',  'ш'  =>  'sh',
            'Щ' => 'Shh', 'щ'  => 'shh',
            'Ы' => 'Y',  'ы' =>  'y' ,
            'Э' => 'E',  'э'  =>  'e',
            'Ю' => 'Yu',  'ю'  =>  'yu',
            'Я' => 'Ya',  'я'  =>  'ya',
            'Ь' => '',    'ь'  => '',
            'Ъ' => '',    'ъ'  => '',
        );

        $str = strtr($str, $transliteration);
        $str = preg_replace('|([-]+)|s', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

}
