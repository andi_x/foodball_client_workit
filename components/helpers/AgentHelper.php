<?php

namespace app\components\helpers;
use yii\helpers\StringHelper;
use Yii;
use yii\helpers\Html;

class AgentHelper
{

    /*
    * Вывод основной формы ресторана
     */
    public static function BasicForm($agent)
    {
        $image = $agent->image ? "http://new.foodballrf.ru/images/agents/" . $agent->image : "http://new.foodballrf.ru/images/agents/none.jpg";
        $delivery_price =  $agent->delivery_price ? $agent->delivery_price : "бесплатно";

        $form = '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <h3 class="restourants-page-restaurant-type_name mobile-restourant_title">'. Html::encode($agent->name) . '</h3>
          <div class="restourants-page-restaurants_restaurant-logo" style="background-image: url('. $image.');">
          </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
          <h3 class="restourants-page-restaurant-type_name">' . Html::encode($agent->name) . '</h3>
          <span class="restourants-page-restaurant-type_kitchen">' . Html::encode($agent->description) . '</span>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <span class="restourants-page-restaurant-info_amount">' . self::Price($agent->min_order_price)  . ' руб.</span>
          <span  class="restourants-page-restaurant-info_text">мин. сумма заказа</span>

          <span class="restourants-page-restaurant-info_delivery">' . $delivery_price . '</span>
          <span class="restourants-page-restaurant-info_text">стоимость доставки</span>

          <span class="restourants-page-restaurant-info_delivery">' . StringHelper::truncate($agent->open_time, 5, '') . ' - ' . StringHelper::truncate($agent->close_time, 5, '') . '</span>
          <span class="restourants-page-restaurant-info_text">время работы</span>
        </div>';

        return $form;
    }

    /**
     * Вывод типа оплаты
     */
    public static function PayType($agents_pay_types)
    {
        $pay_types = '';

        foreach ($agents_pay_types as $pay_type) {
            if ($pay_type->id > 3) {
                break;
            }

            $pay_types .= '<p class="restourants-page-restaurant-additional_pay" style="text-align: left; padding: 8px 3px">';

            if ($pay_type->id == 1) {
                $pay_types .= '<i class="fa fa-rub" style="height: 23px; float: left; margin-right: 3px;" aria-hidden="true"></i>';
            }

            if ($pay_type->id == 2 || $pay_type->id == 3) {
                $pay_types .= '<i class="fa fa-credit-card" style="height: 23px; float: left; margin-right: 3px;" aria-hidden="true"></i>';
            }

            $pay_types .= Html::encode($pay_type->name) . '</p>';
        }

        return $pay_types;
    }

    public static function Price($price)
    {
        $price = $_SESSION['train'] === true ? $price*1.1 : $price;
        return NumberHelper::round_up($price, 0);
    }
}
