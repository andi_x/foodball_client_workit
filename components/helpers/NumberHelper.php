<?php

namespace app\components\helpers;
use yii\helpers\Html;

class NumberHelper
{

    public static function round_up($value, $places)
    {
        $value = trim($value);
        $mult = pow(10, abs($places));
         return $places < 0 ?
        ceil($value / $mult) * $mult :
            ceil($value * $mult) / $mult;
    }

}
