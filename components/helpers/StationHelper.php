<?php

namespace app\components\helpers;
use yii\helpers\Html;

class StationHelper
{

    public static function formatTime($date, $time)
    {
        if (\DateTime::createFromFormat('d.m.Y H:i', $date . " $time") !== false) {
            return "$date $time по мск.";
        }

        return $time;
    }

    public function formatStop($WaitingTime)
    {
        $WaitingTime = ltrim($WaitingTime, '0');
        $hours = floor($WaitingTime/60); // Получаем количество полных часов
        $minutes = $WaitingTime - ($hours*60); // Получаем оставшиеся минуты
        return $hours ? $hours . ' ч ' . $minutes . ' мин' : $minutes . ' мин';
    }

}
