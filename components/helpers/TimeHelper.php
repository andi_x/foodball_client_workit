<?php

namespace app\components\helpers;
use yii\helpers\Html;

class TimeHelper
{

    /**
    * Проверка времени работы ресторана
    */
    public static function validateAgent($agent, $timezone=null, $order_time)
    {

        if ($_SESSION['train'] === true || $_SESSION['preOrd'] === 1) {
            $agent->close_time = date('H:i:s', strtotime($agent->close_time) + 1*3600);
            if (abs(strtotime($agent->close_time) - strtotime($agent->open_time)) <= 1*3600 ) {
                $agent->close_time = date('H:i:s', strtotime($agent->close_time) - 1*3600);
            }
        }

        if (isset($_SESSION['preOrd'])) {
            return true;
        }

        $closetime = explode(":", $agent->close_time);
        $closeh = $closetime[0];
        $closem = $closetime[1] + $closeh*60;
        $opentime = explode(":", $agent->open_time);
        $openh = $opentime[0];
        $openm = $opentime[1] + $openh*60;
        $ordertime = explode(":", $order_time);
        $orderh = $ordertime[0];

        if ($_SESSION['train'] === true) {
            $orderh += $timezone;
            if ($orderh >= 24) {
                $orderh -= 24;
            }
        }

        $orderm = $ordertime[1] + $orderh*60;
        $checkmin = $openm - $closem;

        if ($checkmin < 0) {
            $op = false;
            if ($openm <= $orderm && $orderm < $closem) {
                $op = true;
            }
        } elseif ($checkmin > 0) {
            $op = true;
            if ($openm >= $orderm && $orderm >= $closem) {
                $op = false;
            }
        } else {
            $op = true;
        }

        return $op;
    }

}
