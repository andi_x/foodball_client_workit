<?php
namespace app\components\widgets;
use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
/**
 * This is just an example.
 */
class CloudPayWidget extends \yii\base\Widget
{
    const TYPE_CHARGE = 'charge';
    const TYPE_AUTH = 'auth';
    public $selector;
    public $amount;
    public $invoiceId;
    public $accountId;
    public $email;
    public $data;
    public $description = 'Заказ с сайта';
    public $publicId = 'pk_7ace4e87433c8785078eb49032d39';
    public $currency = 'RUB';
    public $type = self::TYPE_AUTH;

    public function run()
    {
        $this->view->registerJSFile('https://widget.cloudpayments.ru/bundles/cloudpayments', ['depends' => 'app\assets\IndexAsset']);

        $tmp = array_filter([
            'publicId' => $this->publicId,
            'description' => $this->description,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'invoiceId' => $this->invoiceId,
            'accountId' => $this->accountId,
            'email' => $this->email,
            'data' => $this->data,
        ]) + [
            'publicId' => $this->publicId,
            'currency' => $this->currency,
        ];
        $type = $this->type;
        $this->view->registerJs('$("'.$this->selector.'").on("click", function(e) {
            var obj = $(this);
            var widget = new cp.CloudPayments();
            widget.'.$type.'('.Json::encode($tmp).', function(options) {
                $("#order-transaction_id").val(options.invoiceId);
                $("#pay-button").hide();
                $("#submit-button").show();
                $("#submit-button").click();
                obj.trigger("cloudpayments.success", [options]);
            }, function(reason, options) {
                obj.trigger("cloudpayments.fail", [reason, options]);
            });
            e.preventDefault();
        });');
        $this->view->registerJs('$("'.$this->selector.'").click()');
    }
}
