<?php

namespace app\components\widgets;

use app\models\Api;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use Yii;

class CitiesWidget extends Widget
{
    public $tpl;
    public $CitiesHtml;

    public function init()
    {
        parent::init();
        if ($this->tpl === null) {
            $this->tpl = 'cities';
        }
        $this->tpl .= '.php';
    }

    public function run()
    {
        $cities = Yii::$app->cache->get('cities');
        $token = Yii::$app->user->identity->accessToken ? Yii::$app->user->identity->accessToken : $_SESSION['token'];

        if ($cities) {
            return $cities;
        }

        $pageCount = Api::cities_list($token);

        $pageCount = $pageCount->_meta->pageCount;
        $items = [];

        for ($i = 0; $i <= $pageCount; $i++) {
            $cities_list = Api::cities_list($token, $i);
            $cities_array = ArrayHelper::toArray($cities_list->items);

            foreach ($cities_array as $item) {
                $items[$item['id']] = ['id' => $item['id'], 'name' => $item['name'], 'code' => $item['code']];
            }
        }

        ArrayHelper::multisort($items, 'name', SORT_ASC);
        $this->CitiesHtml = $this->catToTemplate($items, $pageCount);

        if (is_numeric($pageCount) && $pageCount > 0) {
            Yii::$app->cache->set('cities', $this->CitiesHtml, 60*60);
        }

        return $this->CitiesHtml;
    }

    protected function catToTemplate($items)
    {
        ob_start();
        include __DIR__ . '/templates/cities_tpl/' . $this->tpl;
        return ob_get_clean();
    }
}
