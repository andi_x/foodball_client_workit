<?php

namespace app\components\widgets;

define('AC_DIR', dirname(__FILE__) );

use yii\base\Widget;
use yii\web\HttpException;
use yii\web\TooManyRequestsHttpException;
use yii\helpers\ArrayHelper;
use app\models\Api;
use app\models\ExpressApi;
use Yii;


class TrainsWidget extends Widget
{

    public $tpl;
    public $TrainsHtml;
    public $TrainsRespone;
    public $trainform;
    public $stationform;


    public function init()
    {
        parent::init();
        if ($this->tpl === null) {
            $this->tpl = 'stations';
        }
        $this->tpl .= '.php';
    }

    public function run()
    {
        if($this->tpl == 'stations.php') {
            $result = [];
            $codes = [];
            $stations = '';
            $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
            $response = ExpressApi::StationsList($this->trainform->train_num, $this->trainform->date, $this->trainform->from);
            $result['Number'] = $response->Train->attributes()->Number;
            $ts = Yii::$app->formatter->format($_SESSION['datetrain'], 'timestamp') - (3 * 3600);
            $now =  Yii::$app->formatter->format('now', 'timestamp');
            $start = 0;

            /**
             * filter stations
             */
            foreach ($response->Routes->Stop as $station) {
                if ($station->attributes()->Code != $this->trainform->from && $start === 0) {
                    continue;
                } elseif ($station->attributes()->Code != $this->trainform->where && $start === 0) {
                    $start = 1;
                    $days = $station->Days;
                } elseif ($station->attributes()->Code == $this->trainform->where) {
                    $start = 0;
                }
                $result['List'][] = [
                    'Station' => $station->attributes()->Station,
                    'Code' => $station->attributes()->Code,
                    'ArvTime' =>  $station->ArvTime,
                    'DepTime' => $station->DepTime,
                    'Days' => $station->Days - $days,
                    'WaitingTime' => $station->WaitingTime,
                    'Sign' => $station->Sign
                ];
            }

            /**
             * format and validate stations
             */
            foreach ($result['List'] as $id => $station) {



                $waitingTime = ltrim($station['WaitingTime']);
                $datadep = explode(':', $station['DepTime']);
                $datadep[0] = $datadep[0] * 3600;
                $datadep[1] = $datadep[1] * 60;
                $datadep = $datadep[0] + $datadep[1];
                $days = $station['Days'] * 24 * 3600;
                $arvdate = ($ts + $days + $datadep - $station['WaitingTime']*60);
                $depdate = ($ts + $days + $datadep + $station['WaitingTime']*60);
                $check = ($arvdate - $now) / 60;
                $result['List'][$id]['arvdate'] = Yii::$app->formatter->asDate($arvdate, 'dd.MM.yyyy');
                $result['List'][$id]['depdate'] = Yii::$app->formatter->asDate($depdate, 'dd.MM.yyyy');

                if (strcasecmp($station['Sign'], 'ТЕХСТОЯНКА') == 0) {
                    $result['List'][$id]['ArvTime'] = 'ТЕХСТОЯНКА';
                    continue;
                }

                if ($check < 90) {
                    continue;
                }

                if ($waitingTime < 15) {
                    if ($station['Station'] !== "ИШИМ") {
                        continue;
                    } else {
                        if ($waitingTime < 12) {
                            continue;
                        }
                    }
                }

                $stations .=  $station['Code'] . ',';
            }


            if (!empty($stations)) {
                $validStations = Api::stations_list($token, $stations , true, null, 1);
                $pageCount = $validStations->_meta->pageCount;

                for ($i = 0; $i <= $pageCount; $i++) {
                    $stations_list =  Api::stations_list($token, $stations , true, null, $i);
                    $stations_array = ArrayHelper::toArray($stations_list->items);

                    foreach ($stations_array as $item) {
                        if ($item['city']['available'] == 1) {
                            $codes[$item['id']] = ['code' => $item['code']];
                        }
                    }
                }
            }

            foreach ($result['List'] as $id => $station) {
                $result['List'][$id]['active'] = 0;
                foreach ($codes as $i => $code) {
                    if ($station['Code'] == trim($code['code'])) {
                        $result['List'][$id]['active'] = 1;
                        break;
                    }
                }
            }

        } elseif ($this->tpl == 'trains_list.php') {
          // $train_list = Yii::$app->cache->get('trains_list.'  . $this->stationform->wherefrom . '.' .  $this->stationform->wherein . '.' . $this->trainform->where . '.' . $this->stationform->date);
          // if($train_list) return $train_list;
            $result = ExpressApi::TrainsList($this->stationform->wherefrom, $this->stationform->wherein, $this->stationform->date);
        }

        $this->TrainsHtml = $this->catToTemplate($result, $this->stationform->wherefrom, $this->stationform->wherein);

    // $today = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd'); //
    // $cachetime =  Yii::$app->formatter->format($today . ' 23:59:59', 'timestamp') - Yii::$app->formatter->format('now', 'timestamp');

    // if($this->tpl = 'trains_list.php') {
    //   if(!empty($result->tp[0]))
    //     {
    //       Yii::$app->cache->set('trains_list.' . $this->stationform->wherefrom . '.' .  $this->stationform->wherein . '.' . $this->trainform->where . '.' . $this->stationform->date, $this->TrainsHtml, $cachetime);
    //     }
    // }

        return $this->TrainsHtml;
    }

    protected function catToTemplate($result, $from, $to) {
        ob_start();
        include __DIR__ . '/templates/trains_tpl/' . $this->tpl;
        return ob_get_clean();
    }

}
