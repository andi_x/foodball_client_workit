<?php

namespace app\components\widgets;

use app\models\Api;
use app\models\City;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use Yii;

class RestorauntWidget extends Widget
{
    public $tpl;
    public $RestorauntsHtml;
    public $where;
    public $timezone;

    public function init()
    {
        parent::init();
        if ($this->tpl === null) {
          $this->tpl = 'restoraunts';
        }
        $this->tpl .= '.php';
    }

    public function run()
    {
        $session = Yii::$app->session;
        $session->open();
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;

        $data['city_id'] = City::getId($token);
        //Смещение по часовому поясу от Москвы
        $timezone = $this->timezone;
        $time = Yii::$app->formatter->format('now', 'timestamp') + ($timezone * 3600);
        $now = Yii::$app->formatter->asDate($time, 'H:m:s');

        $_SESSION['train'] ? $where = 'train' : $where = 'home';
        $where == 'train' ? $data['to_train'] = 1 : $data['to_house'] = 1;

        $_SESSION['filter_types'] ? $data['agent_type_id'] = $_SESSION['filter_types'] : '';
        $_SESSION['filter_categories'] ? $data['product_category_id'] = $_SESSION['filter_categories'] : '';
        $_SESSION['filter_minprice'] ? $data['min_order_price'] = $_SESSION['filter_minprice'] : '';
        $_SESSION['filter_maxprice'] ? $data['max_order_price'] = $_SESSION['filter_maxprice'] : '';
        $_SESSION['filter_name'] ? $data['name'] = $_SESSION['filter_name'] : '';

        //Список агентов
        $agents_list = [];
        $pageCount =  Api::agents_list($token, $data);
        $pageCount = $pageCount->_meta->pageCount;

        for ($i = 1; $i <= $pageCount; $i++) {
            $data['page'] = $i;
            $items = Api::agents_list($token, $data);
            foreach ($items->items as $item) {
                $agents_list[] = $item;
            }
        }

        if (empty($agents_list)) {
            return false;
        }

        $this->RestorauntsHtml = $this->catToTemplate($where, $agents_list, $now, $timezone);
        return $this->RestorauntsHtml;
    }

    protected function catToTemplate($where, $agents_list, $now, $timezone) {
          ob_start();
          include __DIR__ . '/templates/restoraunt_tpl/' . $this->tpl;
          return ob_get_clean();
    }
}
