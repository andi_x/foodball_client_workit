<?php
use yii\helpers\Url;
?>
<!--Choose City Modal Window
============================
==========================-->

<div id="modal_choose-city" class="modal_choose-city">
  <a href="javascript://0" id="choose-city_close" class="choose-city_close-button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>

  <div class="choose-city_content">
    <ul class="choose-city_list">

    <?php for($i = 0; $i <= count($items); $i++) { ?>
        <?php if (mb_substr($items[$i]['name'], 0, 1) != mb_substr($items[$i-1]['name'], 0, 1)) { ?>
            <li class="choose-city-list_el">
              <p class="choose-city-list_letter"><?= mb_substr($items[$i]['name'], 0, 1) ?></p>
            </li>
            <li class="choose-city-list_el">
              <a href="<?= Url::home(true) . 'city?id=' . $items[$i]['id']?>" class="choose-city-list_link"><?= $items[$i]['name'] ?></a>
            </li>
        <?php } else { ?>
            <li class="choose-city-list_el">
              <a href="<?= Url::home(true) . 'city?id=' . $items[$i]['id']?>" class="choose-city-list_link"><?= $items[$i]['name'] ?></a>
            </li>
        <?php } ?>
    <?php } ?>

    </ul>
  </div>
</div>
<!--Choose City Modal Window
====================
==================-->
