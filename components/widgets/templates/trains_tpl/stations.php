<?php
use \yii\web\HttpException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\components\helpers\StationHelper;


// echo '<pre>' . print_r($result, true) . '</pre>';

foreach ($result['List'] as $station) {
    echo Html::beginTag('tr', ['class' => $station['active'] === 1 ? 'stations-train-table_row' : 'stations-train-table_row inactive', 'data-name' => $station['Station'], 'data-code' => $station['Code'],'data-date' => $station['arvdate'] . ' ' . $station['ArvTime'], 'data-waiting' => StationHelper::formatStop($station['WaitingTime'])]);
    echo Html::beginTag('td', ['class' => 'stations-train-table_cell']);
        echo Html::tag('span', '', ['class' => 'table_circle']);
    echo Html::endTag('td');
    echo Html::beginTag('td', ['class' => 'stations-train-table_cell']);
        echo Html::tag('span', $station['Station'], ['class' => 'table_city']); //название станции
    echo Html::endTag('td');
    echo Html::tag('td',  StationHelper::formatTime($station['arvdate'], $station['ArvTime']) , ['class' => 'stations-train-table_cell']); //прибытие
    echo Html::tag('td', StationHelper::formatStop($station['WaitingTime']) , ['class' => 'stations-train-table_cell stop']); //стоянка
    echo Html::tag('td', StationHelper::formatTime($station['depdate'], $station['DepTime']) , ['class' => 'stations-train-table_cell']); //отправление
    echo Html::endTag('tr');
}

 Modal::begin([
   'id'     => 'train',
   'size'   => 'modal-lg',
   'footer' => '<button type="button" onclick="yaCounter44742985.reachGoal(\'backvagon\'); return true;" class="btn btn-default"
 data-dismiss="modal">Отмена</button>'
 ]);
 echo ' <form id="login-form" class="form-horizontal" action="/train/form" method="post" role="form">';
    echo  '<input type="hidden" name="_csrf" value='.  Yii::$app->request->getCsrfToken() .' >';
    echo Html::input('hidden', 'station', '', ['class' => 'station']);
    echo Html::input('hidden', 'city_id', '', ['class' => 'city_id']);
    echo Html::input('hidden', 'city', '', ['class' => 'city']);
  	echo Html::input('hidden', 'date', '', ['class' => 'date']);
    echo Html::input('hidden', 'waiting_time', '', ['class' => 'waiting_time']);
    echo Html::input('hidden', 'train_num', $result['Number'], ['class' => 'train_num']);
    echo Html::label('Укажите номер вагона', 'wagon_num');
    echo '</br>';
    echo Html::input('number', 'wagon_num', '1', ['max' => '999', 'min' => '1']);
    echo '</br>';
    echo '</br>';
    echo Html::submitButton('Выбрать', ['class' => 'btn btn-success', 'value'=>'', 'onclick' => 'yaCounter44742985.reachGoal("vagontrain"); return true;', 'name'=>'submit']);
echo '</form>';
 Modal::end();
