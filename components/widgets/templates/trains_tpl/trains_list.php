<?php
use yii\helpers\Html;
?>

<?php

if(!empty($result->Direction->Trains->Train)) {
    foreach ($result->Direction->Trains->Train as $train) {
        echo Html::beginTag('tr', ['class' => 'stations-train-table_row', 'id' => 'station_table',
        'data-key' => $train->attributes()->Number, 'data-from' => $train->Route->attributes()->CodeFrom, 'data-where' => $train->Route->attributes()->CodeTo]);
          echo Html::beginTag('td', ['class' => 'stations-train-table_cell']);
            echo Html::tag('span', $train->attributes()->Number, ['class' => 'table_city']); //название поезда
          echo Html::endTag('td');
          echo Html::tag('td', $train->Route->Station[0], ['class' => 'stations-train-table_cell']); //откуда
          echo Html::tag('td', $train->Route->Station[1], ['class' => 'stations-train-table_cell']); //куда
        echo Html::endTag('tr');
    }
}
?>
