﻿<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\helpers\TimeHelper;
use app\components\helpers\AgentHelper;
use app\components\helpers\StringHelper;

?>

<?php if ($where === 'train') {  ?>
    <?php
    $message = '
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center; margin-top: 10px; margin-bottom: -15px; padding-right: 0px">
                <p class="restourants-page-restaurant-additional_pay restourants-page-restaurant-additional_pay__not-working" style="border: 2px solid #eb0e0e">
                 Во время прибытия поезда, ресторан не работает
                </p>
            </div>';
    ?>
    <?php foreach ($agents_list as $agent): ?>
        <?php $mes = null ?>
        <?php if (TimeHelper::validateAgent($agent, $timezone, $_SESSION['time'])) {  ?>
            <a href=<?= Url::to(['/go-by-train/' . $agent->url . '/1'], 'https') ?> class="restourants-page-restaurants_restaurant">
        <?php } else { ?>
            <a class="restourants-page-restaurants_restaurant">
            <?php $mes = $message ?>
        <?php } ?>

        <?= AgentHelper::BasicForm($agent) ?>

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 no-padding">
            <?= AgentHelper::PayType($agent->agent_pay_types) ?>
        </div>

        <?= isset($mes) ? $mes : '' ?>

        </a>
    <?php endforeach; ?>

<?php } elseif ($where == 'home' || $where == 'all') { ?>
    <?php $message = '
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center; margin-top: 10px; margin-bottom: -15px; padding-right: 0px">
                        <p class="restourants-page-restaurant-additional_pay restourants-page-restaurant-additional_pay__not-working" style="border: 2px solid #eb0e0e">
                          Ресторан в данный момент не работает,
                          вы можете сделать предзаказ
                        </p>
                    </div>';
    ?>
    <?php foreach ($agents_list as $agent): ?>
        <?php $mes = null ?>
        <?php if (TimeHelper::validateAgent($agent, null, $now)) { ?>
            <a href=<?= Url::to(['/home/' . $agent->url . '/1'], 'https') ?> class="restourants-page-restaurants_restaurant">
        <?php } else { ?>
            <a href='javascript://0' class="restourants-page-restaurants_restaurant" name="<?= $agent->url ?>" onclick="return ShowPreOrd('<?= Url::to(['/home/' . $agent->url  . '/1'], 'https') ?>');">
            <?php $mes = $message ?>
        <?php } ?>

            <?= AgentHelper::BasicForm($agent) ?>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding">
                <?= AgentHelper::PayType($agent->agent_pay_types) ?>
            </div>

            <?= isset($mes) ? $mes : '' ?>

        </a>

    <?php endforeach; ?>
<?php } ?>
