<?php

use yii\db\Migration;

class m170704_095842_drop_column_password_table_user extends Migration
{
    public function safeUp()
    {
      $this->dropColumn('user', 'password');
    }

    public function safeDown()
    {
        $this->addColumn('user', 'password', $this->string(255));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170704_095842_drop_column_password_table_user cannot be reverted.\n";

        return false;
    }
    */
}
