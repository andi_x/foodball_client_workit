<?php

use yii\db\Migration;

class m170704_092927_add_column_table_user extends Migration
{
    public function safeUp()
    {
      $this->addColumn('user', 'created_at', $this->string(255));
      $this->addColumn('user', 'updated_at', $this->string(255));
      $this->addColumn('user', 'points', $this->string(255));
      $this->addColumn('user', 'accessToken', $this->string(255));
    }

    public function safeDown()
    {
      $this->dropColumn('user', 'created_at');
      $this->dropColumn('user', 'updated_at');
      $this->dropColumn('user', 'points');
      $this->dropColumn('user', 'accessToken');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170704_092927_add_column_table_user cannot be reverted.\n";

        return false;
    }
    */
}
