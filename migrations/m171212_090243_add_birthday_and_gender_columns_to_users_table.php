<?php

use yii\db\Migration;

/**
 * Handles adding birthday_and_gender to table `users`.
 */
class m171212_090243_add_birthday_and_gender_columns_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'gender', $this->boolean());
        $this->addColumn('user', 'birthday', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'gender');
        $this->dropColumn('user', 'birthday');
    }
}
