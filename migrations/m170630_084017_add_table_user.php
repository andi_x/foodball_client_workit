<?php

use yii\db\Migration;

class m170630_084017_add_table_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('user', [
          'id' => $this->primaryKey(),
          'username' => $this->string(255)->notNull(),
          'password' => $this->string(255)->notNull(),
          'email' => $this->string(255)->notNull(),
          'phone' => $this->string(12)->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170630_084017_add_table_user cannot be reverted.\n";

        return false;
    }
    */
}
