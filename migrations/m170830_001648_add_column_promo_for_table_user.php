<?php

use yii\db\Migration;

class m170830_001648_add_column_promo_for_table_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'promo', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'promo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170830_001648_add_column_promo_for_table_user cannot be reverted.\n";

        return false;
    }
    */
}
