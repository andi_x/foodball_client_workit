<?php

use yii\db\Migration;

class m170721_110626_add extends Migration
{
    public function safeUp()
    {
      $this->addColumn('user', 'is_call', $this->boolean());
    }

    public function safeDown()
    {
      $this->dropColumn('user', 'is_call');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170721_110626_add cannot be reverted.\n";

        return false;
    }
    */
}
