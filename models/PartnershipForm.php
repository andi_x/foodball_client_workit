<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PartnershipForm extends Model
{
    public $email;
    public $city;
    public $restaurant;
    public $name;
    public $phone;
    public $site;

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'city' => 'Город',
            'restaurant' => 'Название',
            'name' => 'Контактное лицо',
            'phone' => 'Телефон',
            'site' => 'Сайт',
        ];
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email', 'city', 'phone'], 'required'],
            [['restaurant', 'name', 'phone', 'site'], 'default', 'value' => 'не указано'],
            // email has to be a valid email address
            ['email', 'email'],
//            ['phone', 'string', 'length' => [11]],
            ['phone', 'number'],
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo(Yii::$app->params['postClientEmail'])
                ->setFrom(Yii::$app->params['sendClientEmail'])
                ->setReplyTo($this->email)
                ->setSubject('Заявка на партнёрство')
                ->setHtmlBody(
                    '<p>Город: ' . $this->city . '</p>' .
                    '<p>Название ресторана: ' . $this->restaurant . '</p>' .
                    '<p>Контактное лицо: ' . $this->name . '</p>' .
                    '<p>E-mail: ' . $this->email . '</p>' .
                    '<p>Телефон: ' . $this->phone . '</p>' .
                    '<p>Сайт: ' . $this->site . '</p>'
                )
                ->send();

            return true;
        }
        return false;
    }
}
