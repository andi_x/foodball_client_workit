<?php

namespace app\models;

use yii\base\Model;

class StationForm extends Model
{
  public $date;
  public $wherefrom;
  public $wherein;

  public function rules()
  {
      return [
          [['date', 'wherefrom', 'wherein'], 'required'],
      ];
  }

  public function attributeLabels() {
    return [
      'wherefrom' => 'Откуда',
      'wherein'   => 'Куда',
      'date'      => 'Дата отправления',
    ];
  }

}
