<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ResetForm extends Model
{
    public $phone;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            ['phone', 'string', 'length' => [11]],
            ['phone', 'number'],
            ['phone', 'match', 'pattern' => '/^7\d*$/i', 'message' => 'Номер должен начинаться с 7'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
        ];
    }

    /**
     * Reset user's password using the provided phone number.
     * @return bool whether the user is logged in successfully
     */
    public function reset()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $request =  array('phone' => $this->phone);
        $url = Yii::$app->params['api'] . "/users/reset";
        $header[] = "Accept:application/json";
        $header[] = "Content-Type:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        if (!curl_exec($ch)) {
            curl_close($ch);
            return 'Сброс пароля временно недоступен';
        }

        if (curl_errno($ch)) {
            curl_close($ch);
            throw new HttpException(500, 'Неизвестная ошибка '. curl_errno($ch));
        }

        switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            case $http_code == 200:
                $response = 'Подтвердите сброс пароля';
                break;
            case $http_code == 400:
                $response =  'Сервис отправки СМС временно недоступен';
                break;
            case $http_code == 500:
                $response =  'Возможно, в номере телефона ошибка.';
                break;
            default:
                $response = 'Неожиданный код HTTP: ' . $http_code . "\n";
        }

        curl_close($ch);
        return $response;
    }
}
