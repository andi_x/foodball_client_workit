<?php


namespace app\models;

use Yii;
use yii\base\Model;

class ProfileForm extends Model
{
    public $name;
    public $birthday;
    public $gender;
    public $email;

    const SCENARIO_USER     = 0;
    const SCENARIO_STEWARD  = 1;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_USER => ['name', 'birthday', 'gender', 'email'],
            self::SCENARIO_STEWARD => ['name', 'birthday',  'gender', 'email'],
        ];
    }

    public function rules()
    {
        return [
            [['name'],
                'required', 'on' => self::SCENARIO_USER],
            [['name', 'email'],
                'required', 'on' => self::SCENARIO_STEWARD],
            [['email'], 'email'],
            [['name'], 'string'],
            ['birthday', 'birthdayValidator'],
            ['gender', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'      => 'Имя',
            'birthday'  => 'Дата рождения',
            'gender'    => 'Пол',
            'email'     => 'Почта',
        ];
    }

    public function birthdayValidator() {
        if (\DateTime::createFromFormat('Y-m-d', $this->birthday) === false) {
            $this->addError("birthday", "Invalid format");
            return false;
        }

        return true;
    }

    /**
     * Изменят имя, почту пользователя
     * @return string ответа или false если не прошла валидация
     */
    public function update($access_token)
    {
        if (!$this->validate()) {
            return false;
        }

        $type = 'post';
        $request =  array('name' => $this->name, 'email' => $this->email, 'birthday' => $this->birthday, 'gender' => $this->gender);
        $url = Yii::$app->params['api'] . '/users/profile?access_token='.$access_token;

        return Api::request($type, $url, $request);
    }
}
