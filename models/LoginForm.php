<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $phone;
    public $password;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phone', 'password'], 'required'],
            ['password', 'string', 'min' => '6', 'max' => '255'],
            ['phone', 'string', 'length' => [11]],
            ['phone', 'number'],
            ['phone', 'match', 'pattern' => '/^7\d*$/i', 'message' => 'Номер должен начинаться с 7'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'password'  => 'Пароль',
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if (!$this->validate()) {
            return false;
        }
        $request =  array('phone' => $this->phone, 'password' => $this->password);
        $url = Yii::$app->params['api'] . "/users/login";
        $header[] = "Accept:application/json";
        $header[] = "Content-Type:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        if ($res = json_decode(curl_exec($ch))) {

            if (curl_errno($ch)) {
                curl_close($ch);
                throw new HttpException(500, 'Неизвестная ошибка '. curl_errno($ch));
            }

            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case $http_code == 200:
                    $temp = LoginForm::isAuth($res->access_token);
                    if ($temp === false) {
                        throw new HttpException(401, 'Ошибка авторизации');
                    }
                    $temp->email === null ? $temp->email = '' : '';
                    $user = null;
                    $user = $this->getUser($temp->id);
                    if (isset($user)) {
                        $user->accessToken = $res->access_token;
                        $user->points = $temp->points;
                        $user->promo = $temp->promo;
                        $user->email = $temp->email;
                        $user->username = $temp->username;
                        $user->birthday = $temp->birthday;
                        $user->gender = $temp->gender;
                        $user->save();
                        curl_close($ch);
                        return Yii::$app->user->login($user, 0);
                    } else {
                        $user = new User(['id' => $temp->id,
                                    'created_at' => $temp->created_at,
                                    'updated_at' => $temp->updated_at,
                                    'username'   => $temp->username,
                                    'email'      => $temp->email,
                                    'phone'      => $temp->phone,
                                    'points'     => $temp->points,
                                    'is_call'    => $temp->is_call,
                                    'promo'      => $temp->promo,
                                    'birthday'   => $temp->birthday,
                                    'gender'     => $temp->gender,
                                    'accessToken'=> $res->access_token,
                                  ]);
                        $user->save();
                        curl_close($ch);
                        return Yii::$app->user->login($user, 86400);
                    }
                    break;
                case $http_code == 422:
                    curl_close($ch);
                    return 'Неверный номер телефона или пароль';
                    break;
            }
        }

        curl_close($ch);
        return 'Авторизация временно недоступна';
    }



    public function isAuth($access_token = null)
    {
        if (!isset($access_token)) {
            return false;
        }

        $url = Yii::$app->params['api'] . '/users?access_token=' . $access_token . '&expand=promo';
        $header[] = "Accept:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        if ($res = curl_exec($ch)) {

            if (curl_errno($ch)) {
                curl_close($ch);
                throw new HttpException(500, 'Неизвестная ошибка '. curl_errno($ch));
            }

            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
                curl_close($ch);
                return json_decode($res);
            }
        }

        curl_close($ch);
        return false;
    }

    public function getUser($id)
    {
        if ($this->_user === false) {
            $this->_user = User::findIdentity($id);
        }

        return $this->_user;
    }
}
