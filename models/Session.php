<?php
namespace  app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Model;

/**
 *
 */
class Session extends Model
{
    public static function Clear()
    {
        $session = Yii::$app->session;
        $session->remove('cart');
        $session->remove('cart.count');
        $session->remove('cart.sum');
        $session->remove('station');
        $session->remove('train_num');
        $session->remove('wagon_num');
        $session->remove('train');
        $session->remove('stop');
        $session->remove('time');
        $session->remove('trtime');
        $session->remove('trdd');
        $session->remove('data');
        $session->remove('data_end');
        $session->remove('data_train');
        $session->remove('fiat');
        $session->remove('agent_id');
        $session->remove('preOrd');
        $session->remove('filter_types');
        $session->remove('filter_categories');
        $session->remove('filter_minprice');
        $session->remove('filter_name');
        $session->remove('wherefrom');
        $session->remove('wherein');
        $session->remove('date');
        $session->remove('CloudPay');
        $session->remove('trans_id');
        $session->remove('order_city');
        $session->remove('order_city_id');
        $session->remove('for_points');
    }

    public static function OrderTime($where=null, $timezone)
    {
        $session = Yii::$app->session;

        isset($where) ?: $where = 'home';

        if ($session->has('train')) {
            $session['train'] ? $where = 'go-by-train' : $where = 'home';
        }

        //Текущее время по москве
        $time = Yii::$app->formatter->format('now', 'timestamp') + ($timezone * 3600);
        $now = Yii::$app->formatter->asDate($time, 'H:m:s');

        // Возможность заплатить картой онлайн
        if ($session->get('data') >= $time + (3600 * 24 * 25)) {
            $session->set('fiat', false);
        }

        if ($where === 'go-by-train') {

            if (!$session->has('data_train')) {
                return false;
            }

            $session->remove('preOrd');
            $session->set('train', true);
            $session->set('data', Yii::$app->formatter->asDate($session['data_train'] + ($timezone * 3600), 'yyyy-MM-dd HH:mm'));
            $session->set('data_end', Yii::$app->formatter->asDate($session['data_train'] + ($timezone * 3600) + (3600 * 24 * 45), 'yyyy-MM-dd HH:mm'));
        } else {
            $session->remove('station');
            $session->remove('train_num');
            $session->remove('wagon_num');
            $session->remove('time');
            $session->remove('train');
            $session->remove('trtime');
            $session->remove('trdd');
            $session->set('train', false);
            $session->set('data', Yii::$app->formatter->asDate($time, 'yyyy-MM-dd HH:mm'));
            $session->set('data_end', Yii::$app->formatter->asDate($time + (3600 * 24 * 45), 'yyyy-MM-dd HH:mm'));
        }

        return true;
    }

    public static function getToken()
    {
        $token = Yii::$app->user->isGuest ? Yii::$app->session->get('token') : Yii::$app->user->identity->accessToken;
        return $token;
    }

}
