<?php


namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    public $username;
    public $password;
    public $email;
    public $phone;
    public $rzd_relation;

    const SCENARIO_USER    = 0;
    const SCENARIO_STEWARD = 1;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_USER => ['username', 'phone', 'password', 'email', 'phone'],
            self::SCENARIO_STEWARD => ['username', 'phone', 'password', 'email', 'phone', 'rzd_relation'],
        ];
    }

    public function rules()
    {
        return [
            [['username', 'password', 'phone'], 'required'],
            [['email', 'rzd_relation'], 'required', 'on' => self::SCENARIO_STEWARD],
            [['username', 'password', 'email', 'phone'], 'trim'],
            ['username', 'string', 'min' => '2', 'max' => '255'],
            ['password', 'string', 'min' => '6', 'max' => '255'],
            ['email', 'string', 'max' => '255'],
            ['email', 'email'],
            ['phone', 'string', 'length' => [11]],
            ['phone', 'number'],
            ['phone', 'match', 'pattern' => '/^7\d*$/i', 'message' => 'Номер должен начинаться с 7'],
        ];
    }


    public function attributeLabels() {
        return [
            'username'     => 'Имя',
            'password'     => 'Пароль',
            'email'        => 'Почта',
            'phone'        => 'Номер телефона',
            'rzd_relation' => 'Связь с РЖД',
        ];
    }


    /**
     * Проводит регистрацию пользователя
     * @return string ответа или false если не прошла валидация
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->scenario == self::SCENARIO_USER) {
            $request =  array('phone' => $this->phone, 'password' => $this->password, 'username' => $this->username, 'email' => $this->email);
            $url = Yii::$app->params['api'] . "/users";
        } elseif ($this->scenario == self::SCENARIO_STEWARD) {
            $request =  array('phone' => $this->phone, 'password' => $this->password, 'username' => $this->username, 'email' => $this->email,
                'rzd_relation' => $this->rzd_relation);
            $url = Yii::$app->params['api'] . "/users/steward";
        }

        $header[] = "Accept:application/json";
        $header[] = "Content-Type:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        if ($res = json_decode(curl_exec($ch))) {
            if(!curl_errno($ch)) {
                switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                    case $http_code == 200:
                        if ($res->message == 'Success') {
                            curl_close($ch);
                            return 'Поздравляем!';
                        } else {
                            curl_close($ch);
                            return 'Подтвердите регистрацию';
                        }
                        break;
                    case $http_code == 400:
                        curl_close($ch);
                        return 'Сервис отправки СМС временно недоступен';
                        break;
                    case $http_code == 421:
                        curl_close($ch);
                        return 'Неверный пароль или номер телефона';
                        break;
                    case $http_code == 422:
                        curl_close($ch);
                        return 'Этот телефон уже зарегистрирован';
                        break;
                    case $http_code == 500:
                        curl_close($ch);
                        return 'Внутренняя ошибка сервера';
                        break;
                    default:
                        curl_close($ch);
                        return 'Неожиданный код HTTP: ' . $http_code . "\n";
                }
            }
        } else {
            curl_close($ch);
            return 'Регистрация временно недоступна';
        }

    }
}
