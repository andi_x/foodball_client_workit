<?php


namespace app\models;

use yii\base\Model;
use Yii;

class PasswordForm extends Model
{
    public $phone;
    public $code;
    public $password;

    public function rules()
    {
        return [
            [['code', 'phone', 'password'], 'required'],
            ['phone', 'string', 'length' => [11]],
            ['phone', 'number'],
            ['phone', 'match', 'pattern' => '/^7\d*$/i', 'message' => 'Номер должен начинаться с 7'],
            ['code', 'string', 'length' => [4]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'code'  => 'Код подтверждения',
            'password'  => 'Новый пароль',
        ];
    }

    /**
     * Проводит регистрацию пользователя
     * @return string ответа или false если не прошла валидация
     */
    public function confirm()
    {
        if (!$this->validate()) {
            return false;
        }

        $request =  array('phone' => $this->phone, 'code' => $this->code, 'password' => $this->password);
        $url = Yii::$app->params['api'] . "/users/password";
        $header[] = "Accept:application/json";
        $header[] = "Content-Type:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        if (!curl_exec($ch)) {
            curl_close($ch);
            throw new HttpException(curl_getinfo($ch, CURLINFO_HTTP_CODE), 'Неизвестная ошибка');
        }

        if (curl_errno($ch)) {
            curl_close($ch);
            throw new HttpException(500, 'Неизвестная ошибка '. curl_errno($ch));
        }

        switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            case $http_code == 200:
                $response = 'Новый пароль сохранен';
                break;
            case $http_code == 404:
                $response = 'Номер телефона не найден';
                break;
            case $http_code == 422:
                $response = 'Неверный код подтверждения';
                break;
            default:
                $response = 'Неожиданный код HTTP: ' . $http_code . "\n";
        }

        curl_close($ch);
        return $response;
    }
}
