<?php


namespace app\models;

use Yii;
use yii\base\Model;
use \yii\web\HttpException;
use app\components\helpers\AgentHelper;

class Cart extends Model
{
    public function addToCart($product, $count=1)
    {
        /*
        * varlidate count product
        */
        if ($count > 50) {
            return ['errors' => 'Максимально количество товара 50 шт.'];
        }

        if ($product->for_points) {
            if ($_SESSION['for_points'] === true) {
                return ['errors' => 'Товар за баллы уже добавлен'];
            };
            $_SESSION['for_points'] = true;
            $count = 1;
        }

        if (isset($_SESSION['cart'][$product->id])) {
            $_SESSION['cart.count'] -= $_SESSION['cart'][$product->id]['count'];
            $_SESSION['cart.sum'] -= $_SESSION['cart'][$product->id]['price'] * $_SESSION['cart'][$product->id]['count'];
        }

        if ($product->for_points == 0) {
            $_SESSION['cart'][$product->id] = [
              'count' => $count,
              'name'  => $product->name,
              'price' => AgentHelper::Price($product->price),
              'description' => $product->description,
              'image' => $product->image,
            ];

            $_SESSION['cart.count'] = isset($_SESSION['cart.count']) ? $_SESSION['cart.count'] + $count : $count;
            $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] +  AgentHelper::Price($product->price) * $count :  AgentHelper::Price($product->price) * $count;
        } else {
            $_SESSION['cart'][$product->id] = [
              'count' => $count,
              'name'  => $product->name,
              'price' => AgentHelper::Price($product->price),
              'description' => $product->description,
              'image' => $product->image,
              'for_points' => 1,
            ];
            $_SESSION['cart.count'] = isset($_SESSION['cart.count']) ? $_SESSION['cart.count'] + $count : $count;
        }
    }

    public function recalc($id)
    {
        if (!isset($_SESSION['cart'][$id])) {
            return false;
        }
        if ($_SESSION['cart'][$id]['for_points'] == 1) {
            $_SESSION['cart.count'] -= 1;
            unset($_SESSION['for_points']);
            unset($_SESSION['cart'][$id]);
        } else {
            $countMinus = $_SESSION['cart'][$id]['count'];
            $sumMinus = $countMinus * $_SESSION['cart'][$id]['price'];
            $_SESSION['cart.count'] -= $countMinus;
            $_SESSION['cart.sum'] -= $sumMinus;
            unset($_SESSION['cart'][$id]);
        }
    }

    public static function clear()
    {
        $session = Yii::$app->session;
        $session->remove('cart');
        $session->remove('cart.count');
        $session->remove('cart.sum');
        $session->remove('for_points');
    }
}
