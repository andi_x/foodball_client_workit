<?php
namespace  app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Model;
/**
 *
 */
class ExpressApi extends  Model
{

    public static function Suggest($name)
    {
        $name = trim($name);
        $name = mb_strtoupper($name, 'UTF-8');
        $params  = '<?xml version="1.0" encoding="utf-8"?>';
        $params .= '<GtLocal_Request Type="StationList">';
        $params .= '<Name>'. $name .'</Name>';
        $params .= '</GtLocal_Request>';

        return self::request($params);
    }

    public function TrainsList($from, $to, $date)
    {
        $params = '<?xml version="1.0" encoding="utf-8"?>';
        $params .= '<GtExpress_Request Type="ScheduleRoute">';
        $params .= '<StationFrom>'. $from .'</StationFrom>';
        $params .= '<StationTo>'. $to .'</StationTo>';
        $params .= '<Direction Type="Forward">';
        $params .= '<DepDate>' . $date . '</DepDate>';
        $params .= '<FullDay/>';
        $params .= '</Direction>';
        $params .= '</GtExpress_Request>';

        return self::request($params);
    }

    public function StationsList($train_num, $date, $station_code)
    {
        $params = '<?xml version="1.0" encoding="utf-8"?>';
        $params .= '<GtExpress_Request Type="TrainRoute" Answer="Full">';
        $params .= '<Train Number="'. $train_num . '"/>';
        // !-- Дата в которую проходит поезд через выбранную станцию -->
        $params .= '<Date>' . $date . '</Date>';
    	// <!-- Код станции, через которую проходит поезд в выбранную дату -->
        $params .= '<Station>' . $station_code . '</Station>';
        $params .= '<Standard/>';
        $params .= '</GtExpress_Request>';

        return self::request($params);
    }

    private static function request($params)
    {
        $url = 'http://195.42.167.141:19080/fc';
        $username = 'falcode';
        $password = '!Rel47AzM!';
        $length =  strlen($params);
        $header = array("Content-Type: application/xml", "Content-Length: $length");

        if ($ch = curl_init()) {
            if (!curl_errno($ch)) {
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            } else {
                return "error: " . curl_errno($ch);
            }
        }

        // $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = curl_exec($ch);
        curl_close($ch);
        $xml=(substr($response, strpos($response, "\r\n\r\n")));
        $xml = str_replace('i8>', 'i4>', $xml);
        // $xml = trim($xml);
        $array = simplexml_load_string($xml);

        return $array;
    }
}
