<?php

namespace app\models;

use yii\base\Model;
use Yii;

class LogoutForm extends Model
{
    public $accessToken;
    private $_user = false;

    public function rules()
    {
        return [
            [['accessToken'], 'safe'],
        ];
    }

    public function Logout($accessToken = null)
    {
        if (!isset($accessToken)) {
            return false;
        }

        $request =  array('access_token' => $accessToken);
        $url = Yii::$app->params['api'] . '/users/logout?access_token=' . $accessToken;
        $header[] = "Accept:application/json";
        $header[] = "Content-Type:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        if (!curl_exec($ch)) {
            curl_close($ch);
            throw new HttpException(curl_getinfo($ch, CURLINFO_HTTP_CODE), 'Неизвестная ошибка');
        }

        if (curl_errno($ch)) {
            curl_close($ch);
            throw new HttpException(500, 'Неизвестная ошибка '. curl_errno($ch));
        }

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
            $user = $this->getUser($accessToken);

            if ($user) {
              $user->delete();
            }

            curl_close($ch);
            return true;
        }

        curl_close($ch);
        return false;

    }


    public function getUser($accessToken)
    {
        if ($this->_user === false) {
            $this->_user = User::findIdentityByAccessToken($accessToken);
        }

        return $this->_user;
    }
}
