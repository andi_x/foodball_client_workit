<?php

namespace app\models;

use yii\base\Model;

class TrainForm extends Model
{
  public $date;
  public $train_num;
  public $from;
  public $where;

  public function rules()
  {
      return [
          [['date', 'train_num', 'from', 'where'], 'required'],
      ];
  }

  public function attributeLabels() {
    return [
      'train_num' => 'Номер поезда',
      'date'      => 'Дата отправления',
      'from'      => 'Код станции отправления',
      'where'     => 'Код станции прибытия',
    ];
  }

}
