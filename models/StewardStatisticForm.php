<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class StewardStatisticForm extends Model
{
    public $order_from;
    public $order_to;
    public $total_orders;
    public $complete_orders;
    public $balance;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['order_from', 'order_to', 'total_orders', 'complete_orders', 'balance'], 'safe'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'order_from'     => 'Заказы с',
            'order_to'       => 'Заказы до',
            'total_order'    => 'Всего заказов',
            'complete_order' => 'Выполненных заказов',
            'balance'        => 'Баланс',
        ];
    }

}
