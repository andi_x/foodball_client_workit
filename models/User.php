<?php

namespace app\models;

use \yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use Yii;

class User extends ActiveRecord implements IdentityInterface
{

  public static function tableName()
    {
        return 'user';
    }

  public function rules()
  {
      return [
          [['username', 'phone'], 'required'],
          [['email', 'created_at', 'updated_at',
          'points', 'accessToken', 'is_call', 'promo', 'birthday', 'gender'], 'safe'],
      ];
  }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public static function getPoints($token)
    {
        return (self::find()->select('points')->where(['accessToken' => $token])->one())->points;
    }

    public static function getEmail($token)
    {
        return (self::find()->select('email')->where(['accessToken' => $token])->one())->email;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {

    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_key)
    {

    }

    public function is_steward($token)
    {
        return self::findIdentityByAccessToken($token)->promo;
    }

}
