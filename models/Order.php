<?php


namespace app\models;

use Yii;
use yii\base\Model;
use \yii\web\HttpException;

class Order extends Model
{
    public $payment_type;
    public $delivery_at;
    public $delivery_address;
    public $to_train;
    public $to_preorder;
    public $order_items;
    public $name;
    public $phone;
    public $note;
    public $promo;
    public $transaction_id;

    const SCENARIO_ORDER     = 0;
    const SCENARIO_PREORDER  = 1;

    const PAYMENT_CASH          = '1';
    const PAYMENT_CARD_CARRIER  = '2';
    const PAYMENT_CARD          = '3';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_ORDER => ['delivery_at', 'payment_type', 'to_train', 'to_preorder', 'delivery_address', 'promo',
             'order_items', 'name', 'phone', 'transaction_id', 'note'],
            self::SCENARIO_PREORDER => ['delivery_at', 'payment_type', 'to_train', 'to_preorder', 'delivery_address', 'promo',
             'order_items', 'name', 'phone', 'transaction_id', 'note'],
        ];
    }

    public function rules()
    {
        return [
            [['payment_type', 'delivery_at', 'delivery_address', 'to_train',
            'order_items', 'name', 'phone'], 'required'],
            [['payment_type'], 'integer'],
            [['to_train', 'to_preorder'], 'boolean'],
            [['note'], 'string'],
            [['name', 'delivery_address', 'delivery_at', 'transaction_id'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 30],
            [['promo'], 'string',  'min' => 6, 'max' => 6],
            ['delivery_at', 'DeliveryAtValidator'],
            ['payment_type', 'TransactionValidator'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'delivery_at'      => 'Дата доставки',
            'delivery_address' => 'Адрес доставки',
            'name'             => 'Имя',
            'phone'            => 'Телефон',
            'note'             => 'Примечание для курьера',
            'promo'            => 'Промокод',
            'payment_type'     => 'Способ оплаты'
        ];
    }

    /**
    * Validates delivery_at
    * @return bool
    */
    public function DeliveryAtValidator()
    {
        $session = Yii::$app->session;
        $session->open();
        $token = Yii::$app->user->isGuest ? $session['token'] : Yii::$app->user->identity->accessToken;
        $cookies = Yii::$app->request->cookies;
        $agent = Api::agent_info($token, $session['agent_id']);
        $city_id = isset($session['order_city_id']) ? $session['order_city_id'] : $cookies['city_id']->value;
        //Смещение по часовому поясу от Москвы
        $timezone = Api::cities_info($token, $city_id)->timezone;

        if ($session['train'] === true || $session['preOrd'] === 1) {
            $agent->close_time = date('H:i:s', strtotime($agent->close_time) + 1*3600);
            if (abs(strtotime($agent->close_time) - strtotime($agent->open_time)) <= 1*3600 ) {
                $agent->close_time = date('H:i:s', strtotime($agent->close_time) - 1*3600);
            }
        }

        $out = preg_split("~:~", $this->delivery_at);
        $orderm = $out[1];
        $orderh = preg_split("/\s/", $out[0])[1];
        $orderm = $orderm + $orderh*60;
        $closetime= explode(":", $agent->close_time);
        $closeh = $closetime[0];
        $closem = $closetime[1] + $closeh*60;
        $opentime= explode(":", $agent->open_time);
        $openh = $opentime[0];
        $openm = $opentime[1] + $openh*60;

        $now = Yii::$app->formatter->format('now', 'timestamp') + (3  * 3600) + $timezone;
        $delivery_end = $now + (45 * 24 * 3600);
        $delivery_fiat = $now + (25 * 24 * 3600);
        $delivery_at = Yii::$app->formatter->format($this->delivery_at, 'timestamp');

        $checkmin = $openm - $closem;

        if ($this->scenario === self::SCENARIO_PREORDER) {

            if ($delivery_at < $now + (3 * 3600)) {
                $this->addErrors(["Wrong delivery_at" => "Минимальное время для осуществления предзаказа 3 часа"]);
                return false;
            }

            if ($checkmin < 0) {
                $op = false;
                if ($openm+90 <= $orderm && $orderm < $closem) {
                    $op = true;
                }
            } elseif ($checkmin > 0) {
                $op = true;
                if ($openm+90 >= $orderm && $orderm >= $closem) {
                    $op = false;
                }
            } else {
                $op = true;
            }
        } else {
            if ($checkmin < 0) {
                $op = false;
                if ($openm <= $orderm && $orderm < $closem) {
                    $op = true;
                }
            } elseif ($checkmin > 0) {
                $op = true;
                if ($openm >= $orderm && $orderm >= $closem) {
                    $op = false;
                }
            } else {
                $op = true;
            }
        }

        if (!$op) {
            $this->addErrors(["Delivery_at error" => "В указанное время агент не работает, укажите более позднее время"]);
            return false;
        }

        // Check is delivery_at for end date validate
        if ($delivery_end < $delivery_at) {
            $this->addErrors(["Wrong delivery_at" => "Максимально время предзаказа 45 дней"]);
            return false;
        }

        // Check delivary time fo cachless payment_type
        if ($this->payment_type === self::PAYMENT_CARD) {

            if ($delivery_fiat < $delivery_at) {
                $this->addErrors(["Wrong delivery_at" => "Максимальное время предзаказа для оплаты онлайн 25 дней"]);
                return false;
            }

        }
    }

    public function TransactionValidator()
    {
        if ($this->payment_type === self::PAYMENT_CARD) {
            if (isset($this->transaction_id) && (!empty($this->transaction_id))) {
                return true;
            }
            $_SESSION['trans_id'] =
            Yii::$app->getSecurity()->generateRandomString(4) . '-' . Yii::$app->getSecurity()->generateRandomString(4) . '-'. Yii::$app->getSecurity()->generateRandomString(4) . '-' . Yii::$app->getSecurity()->generateRandomString(4);

            $this->addErrors(["Pay error" => "Заказ не оплачен"]);
            return false;
        }
    }

    public function beforeValidate()
    {
        //Смещение по часовому поясу от Москвы
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $cookies = Yii::$app->request->cookies;
        $city_id = City::getId($token);
        $timezone = Api::cities_info($token, $city_id)->timezone;
        //Текущее время по москве
        $time = Yii::$app->formatter->format('now', 'timestamp') + ($timezone * 3600);

        if ($this->scenario === self::SCENARIO_ORDER && $_SESSION['train'] === false) {
            $this->delivery_at = Yii::$app->formatter->asDate($time, 'yyyy-MM-dd HH:mm');
        }

        $this->scenario === self::SCENARIO_PREORDER ? $this->to_preorder = true : $this->to_preorder = false;
        $_SESSION['train'] ? $this->to_train = true : $this->to_train = false;
        return true;
    }

}
