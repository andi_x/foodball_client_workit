<?php


namespace app\models;

use yii\base\Model;
use \yii\web\HttpException;
use Yii;

class Api extends Model
{

  /**
  * Method: GET
  *
  * access_token=[string]` - User access token
  * page=[integer] - Page identificator
  * expand=[fields] - Agent type expand filed (allows to get extra info like agents)
  *
  * Filters:
  * name=[string] - City name filter
  * code=[string] - City code filter
  * available=[boolean] - City availability filter
  *
  * Returns json data with list of cities
  */
    public static function cities_list(
        $accessToken,
        $page = null,
        $name = null,
        $code = null
  ) {
        $type = 'get';
        $url = Yii::$app->params['api'] . '/cities?';
        $url .= 'access_token=' . $accessToken;

        if (isset($name)) {
            $url .= '&name='.$name;
        }
        if (isset($code)) {
            $url .= '&code='.$code;
        }
        if (isset($page)) {
            $url .= '&page='.$page;
        }
        $url .= '&available=1';

        return self::request($type, $url);
    }

    /**
    * Method: GET
    *
    * Required:
    * id=[integer] - City identificator
    * access_token=[string] - User access token
    *
    * Returns json data about a single city
    */
    public static function cities_info($accessToken = null, $id = null)
    {
        $type = 'get';

        if (isset($id)) {
            $url = Yii::$app->params['api'] . '/cities/' . $id;
        } else {
            throw new HttpException('Укажите город');
        }

        if (isset($accessToken)) {
            $url .= '?access_token=' . $accessToken;
        }

        return self::request($type, $url);
    }

    /**
     *
     */
    public static function agent_info($accessToken = null, $id=null)
    {
        $type = 'get';

        if (isset($id)) {
            $url = Yii::$app->params['api'] . '/agents/' . $id;
        } else {
            throw new HttpException(404, 'Агент не найден');
        }

        if (isset($accessToken)) {
            $url .= '?access_token=' . $accessToken;
            $url .= '&expand=agent_pay_types';
        } else {
            $url .= '?expand=agent_pay_types';
        }

        return self::request($type, $url);
    }


    /**
     *
     */
    public static function agents_list($accessToken = null, $data = [])
    {
        $type = 'get';
        $url = Yii::$app->params['api'] . '/agents?';

        isset($accessToken) ? $url .= '&access_token=' . $accessToken : '';
        isset($data['city_id']) ? $url .= '&city_id=' . $data['city_id'] : '';
        isset($data['min_order_price']) ? $url .= '&min_order_price=' . $data['min_order_price'] : '';
        isset($data['max_order_price']) ? $url .= '&max_order_price=' . $data['max_order_price'] : '';
        isset($data['name']) ? $url .= '&name=' .  urlencode($data['name']) : '';
        isset($data['url']) ? $url .= '&url=' .  $data['url'] : '';
        isset($data['agent_type_id']) ? $url .= '&agent_type_id=' . $data['agent_type_id'] : '';
        isset($data['product_category_id']) ? $url .= '&product_category_id=' . $data['product_category_id'] : '';
        isset($data['to_house']) ? $url .= '&to_house=' . $data['to_house'] : '&to_house=0';
        isset($data['to_train']) ? $url .= '&to_train=' . $data['to_train'] : '&to_train=0';
        $url .= '&expand=agent_pay_types';
        isset($data['city']) ? $url .= ',' . $data['city'] : '';
        $url .= '&available=1';
        isset($data['page']) ? $url .= '&page=' . $data['page'] : '';
        return self::request($type, $url);
    }

    /**
     *
     */
    public static function agent_types_list($accessToken = null)
    {
        $type = 'get';
        $url = Yii::$app->params['api'] . '/agent-types?';

        if (isset($accessToken)) {
            $url .= 'access_token=' . $accessToken;
        }

        return self::request($type, $url);
    }


    /**
     *
     */
    public static function product_categories_list($accessToken, $agent_id = null, $page = null)
    {
        $type = 'get';
        $url = Yii::$app->params['api'] . '/product-categories?access_token=' . $accessToken;
        isset($agent_id) ? $url .= '&agent_id=' . $agent_id : '';
        isset($page) ? $url .= '&page=' . $page : $url .= '&page=1';

        return self::request($type, $url);
    }

    /**
     *
     */
    public static function products_list(
        $accessToken = null,
        $agent_id = null,
        $page = null,
        $where = null,
        $product_category_id = null,
        $for_points = null
    ) {
        $type = 'get';
        $url = Yii::$app->params['api'] . '/products?';

        isset($accessToken) ? $url .= '&access_token=' . $accessToken : '';
        isset($agent_id) ? $url .= '&agent_id=' . $agent_id : true;
        isset($where) ? $url .= $where : true;
        isset($page) ? $url .= '&page=' . $page : $url .= '&page=1';
        isset($product_category_id) ? $url .= '&product_category_id=' . (int)$product_category_id : true;

        if (isset($for_points)) {
            if ($for_points === true) {
                $url .= '&for_points=1';
            } elseif ($for_points === false) {
                $url .= '&for_points=0';
            }
        }

        return self::request($type, $url);
    }

    /**
    *
    */
    public static function product_info($accessToken = null, $id = null)
    {
        $type = 'get';

        if (isset($id)) {
            $url = Yii::$app->params['api'] . '/products/' . $id;
        } else {
            throw new HttpException(404, 'Товар не указан');
        }

        if (isset($accessToken)) {
            $url .= '?access_token=' . $accessToken;
        }

        return self::request($type, $url);
    }

    /**
     *
     */
    public static function orders_create($access_token = null, $data = null)
    {
        $type = 'post';
        $url = Yii::$app->params['api'] . '/orders?';
        $cookies = Yii::$app->request->cookies;
        if (isset($data)) {
            $data->to_train ? $to_train = '1' : $to_train = '0';
            $data->to_preorder ? $to_preorder = '1' : $to_preorder = '0';
            $_SESSION['train'] ? '' : $delivery_address .=  $data->delivery_address . ' Город: ' . $cookies['city'];

            $request =  array(
                'payment_type' => $data->payment_type,
                'delivery_at' => $data->delivery_at . ':00',
                'delivery_address' => $data->delivery_address,
                'to_train' => $to_train,
                'to_preorder' => $to_preorder,
                'name' => $data->name,
                'phone' => $data->phone,
                'note' => $data->note,
                'promo' => $data->promo,
                'order_items' => $data->order_items
            );

            if(isset($data->transaction_id)) {
                $request['transaction_id'] = $data->transaction_id;
            }

            if (isset($access_token)) {
                $url .= '&access_token=' . $access_token;
            }

            return self::request($type, $url, $request);
        } else {
            throw new HttpException(404, 'Товар не указан');
        }
    }

    /**
    */
    public static function orders_list($accessToken = null, $page = null)
    {
        if (isset($accessToken)) {
            $url = Yii::$app->params['api'] . '/orders?access_token=' . $accessToken;
            $url .=  '&expand=order_agents';
            if (isset($page)) {
                $url .= '&page='.$page;
            }
            $type = 'get';

            return self::request($type, $url);
        } else {
            throw new HttpException(401, 'Вы не авторизовались');
        }
    }

    /**
    */
    public static function order_info($accessToken=null, $id=null)
    {
        if (isset($accessToken)) {
            if (isset($id)) {
                $url = Yii::$app->params['api'] . '/orders/' . $id . '?access_token=' . $accessToken;
                $url .=  '&expand=order_agents';
                $type = 'get';

                return self::request($type, $url);
            } else {
                throw new HttpException(404, 'Заказ не найден');
            }
        } else {
            throw new HttpException(401, 'Вы не авторизовались');
        }
    }

    /**
    */
    public static function stations_list(
        $accessToken,
        $code,
        $city = false,
        $city_id = null,
        $page = null
    ) {
        $type = 'get';
        $url = Yii::$app->params['api'] . '/stations?access_token=' . $accessToken;
        $url .= '&code=' . $code;

        if (isset($city_id)) {
            $url .= '&city_id=' . $city_id;
        }

        if ($city) {
            $url .= '&expand=city';
        }
        $page ? $url .= '&page=' . $page : '1';

        return self::request($type, $url);
    }

    /**
    *
    */
    public static function user_promo($accessToken=null, $code=null)
    {
        if (!isset($accessToken)) {
            throw new HttpException(401, 'Вы не авторизовались');
        }

        if (!isset($code)) {
            throw new HttpException(422, 'Неверный промокод');
        }

        $url = Yii::$app->params['api'] . '/users/promo?access_token=' . $accessToken;
        $url .= '&name=' . $code;
        $type = 'get';

        return self::request($type, $url)->message;
    }

    /**
     *
     */
    public static function steward_statistic($accessToken=null, $order_from=null, $order_to=null)
    {
        if (!isset($accessToken)) {
            throw new HttpException(401, 'Вы не авторизовались');
        }
        $url = Yii::$app->params['api'] . '/steward-statistics?access_token=' . $accessToken;

        isset($order_from) ? $url .= '&order_from=' . $order_from : '';
        isset($order_to) ? $url .= '&order_to=' . $order_to : '';
        $url .= '&expand=total_orders,complete_orders';
        $type = 'get';

        return self::request($type, $url);
    }

    /**
     *
     */
    public static function steward_message($accessToken=null)
    {
        if (!isset($accessToken)) {
            throw new HttpException(401, 'Вы не авторизовались');
        }
        $url =  Yii::$app->params['api'] . '/steward-messages?access_token=' . $accessToken;
        $type = 'get';

        return self::request($type, $url);
    }

    /**
     *
     */
    public static function steward_check_message($accessToken=null, $id)
    {
        if (!isset($accessToken)) {
            throw new HttpException(401, 'Вы не авторизовались');
        }

        if (!isset($id)) {
            throw new HttpException(421, 'Не выбрано сообщение');
        }

        $url =  Yii::$app->params['api'] . '/steward-messages/check?access_token=' . $accessToken . '&id=' . $id;
        $type = 'get';

        return self::request($type, $url);
    }

    /**
     *
     */
    public static function get_token()
    {
        $url = Yii::$app->params['api'] . '/users/token';
        $type = 'post';

        return self::request($type, $url)->access_token;
    }

    /**
     *
     */
    public static function request($type, $url, $request = null, $token=null)
    {

        $ch = curl_init();

        if (isset($token)) {
            curl_setopt($ch, CURLOPT_USERPWD, $token);
        }

        if ($type == 'get') {
            $header[] = "Accept:application/json";
        } elseif ($type == 'post') {
            $header[] = "Accept:application/json";
            $header[] = "Content-Type:application/json";
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        if ($res = curl_exec($ch)) {
            if (!curl_errno($ch)) {
                switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                    case $http_code == 200:
                        curl_close($ch);
                        return json_decode($res);
                        break;
                    case $http_code == 401:
                        curl_close($ch);
                        Yii::$app->user->logout();
                        break;
                    case $http_code == 422:
                        curl_close($ch);
                        return json_decode($res);
                        break;
                }
                // curl_close($ch);
                return json_decode($res);
            } else {
                curl_close($ch);
                throw new HttpException(500, 'Неизвестная ошибка '. curl_errno($ch));
            }
        }
        curl_close($ch);
    }

}
