<?php


namespace app\models;

use Yii;
use yii\base\Model;

class ConfirmForm extends Model
{
    public $phone;
    public $code;

    public function rules()
    {
        return [
        [['code', 'phone'], 'required'],
        ['phone', 'string', 'length' => [11]],
        ['code', 'string', 'length' => [4]],
        ['code', 'number'],
      ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Номер телефона',
            'code'  => 'Код подтверждения',
        ];
    }

    /**
     * Проводит регистрацию пользователя
     * @return string ответа или false если не прошла валидация
     */
    public function confirm()
    {
        if ($this->validate()) {
            $request =  array('phone' => $this->phone, 'code' => $this->code);
            $url = Yii::$app->params['api'] . "/users/confirm";
            $header[] = "Accept:application/json";
            $header[] = "Content-Type:application/json";

            if ($ch = curl_init()) {
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
            }

            if (curl_exec($ch)) {
                if (!curl_errno($ch)) {
                    switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                        case $http_code == 200:
                            return 'Вы зарегистрированы';
                            break;
                        case $http_code == 404:
                            return 'Номер телефона не найден';
                            break;
                        case $http_code == 422:
                            return 'Неверный код подтверждения';
                            break;
                        default:
                            return 'Неожиданный код HTTP: ' . $http_code . "\n";
                    }
                }
            } else {
                return 'Регистрация временно недоступна';
            }
        }
        return false;
    }


    /**
     * Проводит подтверждение заказа
     * @return string ответа или false если не прошла валидация
     */
    public function confirmOrder()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $session = Yii::$app->session;
        $session->open();
        $token = Yii::$app->user->isGuest ? $_SESSION['token'] : Yii::$app->user->identity->accessToken;
        $request =  array('phone' => $this->phone, 'code' => $this->code);
        $url = Yii::$app->params['api'] . "/orders/confirm?access_token=".$token;
        $header[] = "Accept:application/json";
        $header[] = "Content-Type:application/json";

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        }

        if (curl_exec($ch)) {
            if (!curl_errno($ch)) {
                switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                    case $http_code == 200:
                     $response = 'Ваш заказ подтвержден';
                     break;
                    case $http_code == 404:
                     $response =  'Номер телефона не найден';
                     break;
                    case $http_code == 422:
                     $response =  'Неверный код подтверждения';
                     break;
                    default:
                     $response = 'Неожиданный код HTTP: ' . $http_code . "\n";
                }
                curl_close($ch);
                return $response;
            }
        } else {
            curl_close($ch);
            return 'Сервис временно недоступен';
        }
    }
}
