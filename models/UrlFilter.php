<?php
namespace  app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Model;
use herroffizier\yii2tv\TranslitValidator;
use yii\helpers\Url;

/**
 *
 */
class UrlFilter extends  Model
{

    public $str;
    public $str_translit;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['str_translit'], 'safe'],
            [['str'], 'string', 'max' => 254],
            [
                ['str_translit'],
                TranslitValidator::classname(),
                'lowercase' => true,
                'sourceAttribute' => 'str'
            ],
        ];
    }


    public function edit()
    {
        Yii::$app->session->open();
        $this->validate();
        $cookies = Yii::$app->response->cookies;
        empty($this->str_translit) ? $this->str_translit = 'moskva' : '';

        if (!(strripos(Url::current(), $this->str_translit))) {

            if (!isset($cookies['city_url']->value)) {
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'city_url',
                    'value' => $this->str_translit,
                ]));
            }

            $url = "/" . $this->str_translit;

            if (isset($_SESSION['confirm_order'])) {
                $_SESSION['confirm_modal'] = true;
                $url .= '/cart/confirm-order';
            } else {
                $url .= Url::current(['cities' => null]);
            }

            $url = str_replace('/site/index', '' , $url);
            $url = str_replace('/index', '' , $url);
            // return var_dump(Url::current());
            return Url::isRelative($url) ? $url : false;
        }

        return false;
    }
}
