<?php
namespace  app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Model;

/**
 *
 */
class City extends  Model
{
    // /**
    //  * @inheritdoc
    //  */
    // public function behaviors()
    // {
    //     return [
    //     ];
    // }
    // /**
    //  * @inheritdoc
    //  */
    // public function rules()
    // {
    //     return [
    //     ];
    // }

    public static function get($token)
    {
        $session = Yii::$app->session;
        $cookies = Yii::$app->request->cookies;
        $city_id = self::getId($token);

        if (!empty($city_id)) {
            $city = Api::cities_info($token, $city_id);
        }

        return $city;
    }

    public static function getId($token)
    {
        $cookies = Yii::$app->request->cookies;
        $session = Yii::$app->session;

        if (!isset($cookies['city_id']->value) && !isset($session['order_city_id']) && !empty(Yii::$app->geoData->data['city']['name_ru'])) {
            $city = Api::cities_list($token, null, Yii::$app->geoData->data['city']['name_ru'])->items[0];
            if (!isset($city->id)) {
                return null;
            }
            $session->set('order_city_id', $city->id);
            $id = $city->id;
        } else {
            $id = Yii::$app->session->has('order_city_id') ? Yii::$app->session->get('order_city_id') : $cookies['city_id']->value;
        }

        return $id;
    }

}
