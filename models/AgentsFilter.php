<?php
namespace  app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Model;
use app\components\helpers\StringHelper;

/**
 *
 */
class AgentsFilter extends  Model
{
    public $min;
    public $max;
    public $types;
    public $name;
    public $categories;
    public $name_translit;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categories', 'name', 'min', 'max', 'types', 'name_translit'], 'safe'],
            [['categories', 'name', 'types'], 'string', 'max' => 254],
            [['max', 'min'], 'integer'],
        ];
    }

    public static function findByName($token, $name=null)
    {
        if (!isset($name)) {
            throw new HttpException(404, 'Не выбран ресторан');
        }

        $data['name'] = str_replace('-', ' ', $name)
            . ',' . $name
            . ',' . StringHelper::untranslit($name)
            . ',' . StringHelper::untranslit(str_replace('-', ' ', $name));
        $data['city_id'] = City::getId($token);
        $response = Api::agents_list($token, $data)->items[0];

        return $response;
    }

    public static function findByUrl($token, $url, $city_id)
    {
        $data['url'] = $url;
        $data['city_id'] = City::getId($token);
        $response = Api::agents_list($token, $data)->items[0];

        return $response;
    }
}
