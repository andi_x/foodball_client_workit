<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'geoData'
    ],
    'language' => 'ru',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '89r3q3535OlmVxHTbCRVMsnH_2fINi8x',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'authTimeout' => 3600 * 24,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // '<cities:[a-zA-Z_-]+>/<action:login|signup|reset|index|logout|confirm|about|press|politic|partnership|contacts|convention|shareterms|account|signup-agent|partnership-agent>' => 'site/<action>',
                '<action:login|signup|reset|index|logout|city|confirm|about|press|politic|partnership|contacts|convention|shareterms|account|signup-agent|partnership-agent>' => 'site/<action>',
                '<action:suggest|filters-clear|cancel-preord>' => 'preorder/<action>',
                '<where:home|go-by-train>/<name:[\w_-]+>/<page:[\d]+>' => 'product/index',
                '<where:home|go-by-train>' => 'preorder/index',
                '<where:preord>' => 'preorder/preord',
                '<controller:train|history|cart|product|preorder>/<action>' => '<controller>/<action>',
                '<controller:train|history|cart|product|preorder>' => '<controller>',
                '<cities:[a-zA-Z_-]+>/<controller:train|product|preorder>/<action>' => '<controller>/<action>',
                '<cities:[a-zA-Z_-]+>/<controller:train|product|preorder>' => '<controller>',
                '<cities:[a-zA-Z_-]+>/<where:home|go-by-train>/<name:[\w_-]+>/<page:[\d]+>' => 'product/index',
                '<cities:[a-zA-Z_-]+>/<where:home|go-by-train>' => 'preorder/index',
                '<cities:[a-zA-Z_-]+>/<where:(preord)>' => 'preorder/preord',
                // '<cities:[a-zA-Z_-]+>/<controller>/<action>' => '<controller>/<action>',
                // '<cities:[a-zA-Z_-]+>/<controller>' => '<controller>',
                '<cities:[a-zA-Z_-]+>' => 'site/',
            ],
        ],

        'geoData' => [
            'class'             => 'phpnt\geoData\GeoData',         // путь к классу
            'addToCookie'       => true,                            // сохранить в куки
            'cookieDuration'    => 86400,                          // время хранения в куки
            'setTimezoneApp'    => false,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['params']['api'] = 'http://foodballrfnew.ru/api/v2';
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
