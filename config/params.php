<?php

return [
    'adminEmail' => 'foodballrf@mail.ru',
    'sendClientEmail' => 'partnership@foodballrf.ru',
    'postClientEmail' => 'client@foodballrf.ru',
    'agentClientEmail'  => 'agent@foodballrf.ru',
    'api' => 'https://new.foodballrf.ru/api/v2',
];
